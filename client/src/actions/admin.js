import axios from 'axios';
import * as TYPES from './types';
import auth from "./auth";


export const GetAdminConfigResponse = (adminConfig) => {
    return {
        type: TYPES.ADMIN_CONFIG_RESPONSE_SUCCESS,
        adminConfig
    };
}

export const GetUserListResponse = (userLists) => {
    return {
        type: TYPES.USER_LISTS_RESPONSE_SUCCESS,
        userLists
    };
}

export const GetTenantResponse = (tenantLists) => {
    return {
        type: TYPES.TENANT_REQUEST_RESPONSE_SUCCESS,
        tenantLists
    };
}


export const GetAdminConfig = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/GetCompanySetting", payload, auth.headers);
        const { status_code, data } = res.data;
        if (status_code == 200) {
            dispatch(GetAdminConfigResponse(data));
        } else {
            dispatch(GetAdminConfigResponse(null));
        }
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }
}

export const EditSetting = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/EditSetting", payload, auth.headers);
        const { status_code} = res.data;
        if (status_code === 200) {
            GetAdminConfig();
        }
        return res.data;
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }

    // return dispatch => {
    //     return api.post("/EditSetting",data).then(res => {
    //         try {
    //             const { status_code} = res;
    //             if (status_code === 200) {
    //                 GetAdminConfig();
    //             }else{
    //                // dispatch(GetAdminConfigResponse(null));
    //             }
    //         } catch (err) {
    //             return { message: "Something is worng." }
    //         }

    //     })
    // }
}

export const EditUser = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/EditCompanyAdminUser", payload, auth.headers);
        const { status_code} = res.data;
        if (status_code === 200) GetAdminConfig();
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     return api.post("/EditCompanyAdminUser",data).then(res => {
    //         try {
    //             const { status_code} = res;
    //             if (status_code === 200) {
    //                 GetAdminConfig();
    //             }else{
    //                // dispatch(GetAdminConfigResponse(null));
    //             }
    //         } catch (err) {
    //             return { message: "Something is worng." }
    //         }

    //     })
    // }
}

export const GetUserList = () => async dispatch => {

    try {
        const res = await axios.post("/api/GetUserList", null, auth.headers);
        const { status_code, data } = res.data;
        if (status_code == 200) {
            dispatch(GetUserListResponse(data));
        } else {
            dispatch(GetUserListResponse([]));
        }
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     return api.post("/GetUserList").then(res => {
    //         try {
    //             const { status_code, data } = res;
    //             if (status_code == 200) {
    //                 dispatch(GetUserListResponse(data));
    //             }else{
    //                 dispatch(GetUserListResponse([]));
    //             }
    //         } catch (err) {
    //             return { message: "Something is worng." }
    //         }

    //     })
    // }
}

export const AddNewUser = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/AddUser", payload, auth.headers);
        return res.data
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     return api.post("/AddUser",data).then(res => {
    //         try {
    //             return res
    //         } catch (err) {
    //             return { message: "Something is worng." }
    //         }

    //     })
    // }
}

export const TenantRequest = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/AddTenantUser", payload, auth.headers);
        return res.data
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     return api.post("/AddUser",data).then(res => {
    //         try {
    //             return res
    //         } catch (err) {
    //             return { message: "Something is worng." }
    //         }

    //     })
    // }
}

export const GetTenantRequest = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/GetTenantRequest", payload, auth.headers);
        const { status_code, data } = res.data;
        if (status_code == 200) dispatch(GetTenantResponse(data));
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     var data = {id : id};
    //     return api.post("/GetTenantRequest",data).then(res => {
    //         try {
    //             if(res.status_code==200){
    //                 dispatch(GetTenantResponse(res.data));
    //             }
    //         } catch (err) {
    //             return { message: "Something is worng." }
    //         }

    //     })
    // }
}

export const AddCompany = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/AddCompany", payload, auth.headers);
        return res.data
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     return api.post("/AddCompany",data).then(res => {
    //         return res;
    //     });
    // }
}

export const GetPassword = (payload) => async dispatch => {

    try {
        const res = await axios.post("/api/GetPassword", payload, auth.headers);
        return res.data
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }


    // return dispatch => {
    //     return api.post("/GetPassword",data).then(res => {
    //         return res;
    //     });
    // }
}

