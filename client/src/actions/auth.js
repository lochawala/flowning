
var request_headers = { headers: { "Authorization": "Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0" } };

export default {
  headers : request_headers,
  setAuthToken() {
    if (localStorage.access_token) {
      request_headers.headers.Authorization = "Bearer " + localStorage.access_token;
    }
  },
  resetAuthToken() {
    request_headers.headers.Authorization = "Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0"
  }
  
}