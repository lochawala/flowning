import axios from 'axios';
import auth from "./auth";
import API from '../utils/api_url'
const apiUrl = API.API_URL;

export const saveServeyForm = (payload) => {	
	return (dispatch) => {
		return axios.put(`${apiUrl}/addservey`,payload,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}

export const ServeyList = () => {	
	return (dispatch) => {
	return axios.get(`${apiUrl}/serveyList`,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}

export const getQuestionsData = (data) => {	
	return (dispatch) => {
	return axios.post(`${apiUrl}/getQuestionsData`,data,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}

export const ServeyDelete = (data) => {	
	return (dispatch) => {
	return axios.post(`${apiUrl}/serveyDelete`,data,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}
export const addSurveyResponse = (data) => {	
	return (dispatch) => {
	return axios.post(`${apiUrl}/addSurveyResponse`,data,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}

export const GetSurveyResponse = (data) => {	
	return (dispatch) => {
	return axios.post(`${apiUrl}/getSurveyResponse`,data,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}
export const GetSurveyResponseanswer = (data) => {	
	return (dispatch) => {
	return axios.post(`${apiUrl}/getSurveyResponseanswer`,data,auth.headers)
			.then((res) => {
                return res;    
            })
			.catch(e => console.error(e))	
	}
}