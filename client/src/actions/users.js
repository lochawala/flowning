import axios from 'axios';
import * as TYPES from './types';
import auth from "./auth";


const setLoginError = (loginError) => {
    return {
        type: TYPES.SET_LOGIN_ERROR,
        loginError
    }
}

export const setUser_data = (User_data) => {
    return {
        type: TYPES.SET_LOGIN_SUCCESS,
        User_data
    };
}

export const setLogoutSuccess = (LogoutSuccess) => {
    return {
        type: TYPES.LOGOUT_SUCCESS,
        LogoutSuccess
    };
}
export const TenantConnectionSuccess = (TenantConnection) => {
    return {
        type: TYPES.TENANT_CONNECTION_SUCCESS,
        TenantConnection
    };
}

export const login = (payload) => async dispatch => {
    try {
        const res = await axios.post("/api/login", payload, auth.headers);
        const { message, status_code, data } = res.data;
        if (status_code === 200) {
            if (data.user.level == "6" && data.user.isView == "login") {
                window.location.href = "http://" + data.user.domain + "/login?email=" + data.user.email;
            } else {
                localStorage.access_token = data.access_token;
                localStorage.refresh_token = data.refresh_token;
                localStorage.user = JSON.stringify(data.user);
                auth.setAuthToken(data.access_token)
                dispatch(setUser_data(data.user))
            }
        } else {
            dispatch(setLoginError(message));
        }
        return res.data;
    } catch (err) {
        return { status: false, message: "Some thing wrong, Please try again." };
    }
}

export const loginwithGoogle = (payload) => async dispatch => {
    try {
        const res = await axios.post("/api/loginwithGoogle", payload, auth.headers);
        const { status_code, data } = res.data;
        if (status_code === 200) {
            localStorage.access_token = data.access_token;
            localStorage.refresh_token = data.refresh_token;
            localStorage.user = JSON.stringify(data.user);
            dispatch(setUser_data(data.user));
            return res.data;
        } else {
            return res.data;
        }
    } catch (err) {
        return { status: false, message: "Something is worng." }
    }
}

export const signup = (payload) => async dispatch => {
    try {
        const res = await axios.post("/api/signup", payload,auth.headers);
        return res.data;
    } catch (err) {
        return { status: false, message: "Something is worng." }
    }
}

export const verifyemails = (verifycode) => async dispatch => {
    try {
        const res = await axios.post("/api/tokenVerify", { token: verifycode });
        return res.data;
    } catch (err) {
        return { status: false, message: "Something is worng." }
    }
}

export const doforget = (payload) => async dispatch => {
    try {
        const res = await axios.post("/api/forgetpassword", payload, auth.headers);
        return res.data;
    } catch (err) {
        return { status: false, message: "Something is worng." }
    }
}

export const changepassword = (payload) => async dispatch => {
    try {
        const res = await axios.post("/api/changepassword", payload ,auth.headers);
        return res.data;
    } catch (err) {
        return { status: false, message: "Something is worng." }
    }
}

export const updateProfile = (payload) => async dispatch => {
    try {
        const res = await axios.post("/api/updateProfile", payload, auth.headers);
        return res.data;
    } catch (err) {
        return { status: false, message: "Something is worng." }
    }
}

export const logout = () => async dispatch => {
    try {
        localStorage.clear();
        auth.resetAuthToken()
        dispatch(setLogoutSuccess(false));
    } catch (err) {

    }
}

