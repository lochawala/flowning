import React, { Component } from 'react'
import './Settings.css'
import { connect } from 'react-redux'
import * as admin from '../../actions/admin'
import * as ACT from '../../actions'

class Settings extends Component {

	constructor() {
		super()
		this.state = { is_edit_user: false, userdata: null }
	}

	componentDidMount() {
		var {User_data} = this.props.user
		this.props.GetAdminConfig({user_id : User_data._id});
		this.props.setApp('admin')
		this.props.loadSidenavConfig('admin')
	}

	EditMode = (title,name, value) => {
		var data = {title : title, name: name, value: value }
		if(title=="smtp_auth_user"){
			data.value =value.user ? value.user  : this.state.userdata 
			data['temp_value'] = value.pass ? value.pass : null
		}

		if(title=="smtp_auth_password"){
			data.value =value.pass ? value.pass : this.state.userdata 
			data['temp_value'] = value.user ? value.user : null
		}
		this.setState({ is_edit_user: true, userdata: data })
	}

	onChange = (e) => {
		this.state.userdata.value = e.target.value
		this.setState({userdata : this.state.userdata})
	}

	editSubmit = (e) => {
		var is_error = false;
		var {name,value,title} = this.state.userdata;
		if(name==="user_name"){
			if(value.split(" ").length>=2){
				var firstname = value.split(" ")[0];
				var lastname = value.split(" ")[1];
				this.props.user.User_data.firstname = firstname;
				this.props.user.User_data.lastname = lastname
				var payload = { firstname : firstname,lastname : lastname, id :  this.props.user.User_data._id}
				this.props.EditUser(payload);
			}else{
				is_error = "Required full name"
			}
		}else{
			if(title=="smtp_auth_user"){
				var value =this.state.userdata.value
				this.state.userdata.value = {
					user : value,
					pass : this.state.userdata.temp_value ? this.state.userdata.temp_value : null
				}
			}

			if(title=="smtp_auth_password"){
				var value =this.state.userdata.value
				this.state.userdata.value = {
					user : this.state.userdata.temp_value ? this.state.userdata.temp_value : null,
					pass : value
				}
			}

			this.props.user.adminConfig[this.state.userdata.name]=this.state.userdata.value
			let payload = { [this.state.userdata.name]: this.state.userdata.value }
			this.props.EditSetting(payload);
		}


		if(is_error){
			window.M.toast({ html: is_error})
		}else{
			//this.props.GetAdminConfig();
			this.setState({ is_edit_user: false, userdata: null });
		}
		
	}

	editUser = (title,name, value) =>{
		var data = {title : title, name: name, value: value.firstname + " " + value.lastname }
		this.setState({ is_edit_user: true, userdata: data })
	}

	databaseconfig(url){
		var user = url.split("@")[0].split("//")[1].split(":")[0]
		var password = url.split("@")[0].split("//")[1].split(":")[1]
		var url_db = url.replace(user,'<user>');
		url_db = url_db.replace(password,'<password>');
		return {
			database_url : url_db,
			user : user,
			password : password
		}
	}

	render() {
		const { adminConfig, User_data } = this.props.user
		let { userdata } = this.state
		if (adminConfig === {} || !adminConfig) return false;
	 	return (
			<div className="settings-page">

				{this.state.is_edit_user &&
					<div>
						<div id="modal-login-form" className="modal open">
							<div className="close_btn" onClick={() => this.setState({ is_edit_user: false, userdata: null })} >
							<i className="material-icons">close</i></div> 
							<div className="editForm">
								<label htmlFor="edit">{userdata.title}</label> &nbsp;
								<input className=""
									id="edit"
									type={userdata.title=="smtp_auth_password" ? "password" : "text"}
									name={userdata.name}
									placeholder={userdata.name}
									value={userdata.value}
									maxLength="256"
									onChange={this.onChange} />
								<br />
								<div className="modal-trigger waves-effect waves-light btn" onClick={() => this.editSubmit(this.state)}>Submit</div>
							</div>

						</div>
						<div class="modal-overlay"></div>
					</div>
				}

				<div className="row">
					<div className="col s6">
						<h5><strong>Company details</strong></h5>
						<div className="card grey lighten-1 settings-card">
							<table className="settings-table">
								<thead>
									<tr><th>Item</th><th>Details</th></tr>
								</thead>
								<tbody>
									<tr><td>Logo</td><td>{adminConfig.logo}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('Logo','logo',adminConfig.logo)}>edit</i></td></tr>
									<tr><td>Company Name</td><td>{adminConfig.company_name}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('Company Name','company_name', adminConfig.company_name)}>edit</i></td></tr>
									<tr><td>Address</td><td>{adminConfig.address}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('Address','address', adminConfig.address)}>edit</i></td></tr>
									<tr><td>Country</td><td>{adminConfig.country}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('Country','country', adminConfig.country)}>edit</i></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div className="col s6">
						<h5><strong>Portal administrator</strong></h5>
						<div className="card grey lighten-1 settings-card portal-administrator">
							<table className="settings-table">
								<thead>
									<tr><th>Item</th><th>Details</th></tr>
								</thead>
								<tbody>
									<tr><td>Name</td><td>{adminConfig.user.firstname + " " + adminConfig.user.lastname}  &nbsp;<i className="material-icons edit" onClick={() => this.editUser('Name','user_name', User_data)}>edit</i></td></tr>
									<tr><td>Email</td><td>{User_data.email}</td></tr>
									<tr><td>User ID</td><td>{User_data._id}</td></tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col s6">
						<h5><strong>SMTP</strong></h5>
						<div className="card grey lighten-1 settings-card">
							<table className="settings-table">
								<thead>
									<tr><th>Item</th><th>Details</th></tr>
								</thead>
								<tbody>
									<tr><td>Server</td><td>{adminConfig.smtp_server}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('Server','smtp_server', adminConfig.smtp_server)}>edit</i></td></tr>
									<tr><td>Port</td><td>{adminConfig.smtp_port}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('Port','smtp_port', adminConfig.smtp_port)}>edit</i></td></tr>
									<tr><td>From Mail</td><td>{adminConfig.smtp_fromMail}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('fromMail','smtp_fromMail', adminConfig.smtp_auth)}>edit</i></td></tr>
									<tr><td>Authentication User</td><td>{adminConfig.smtp_auth.user}  &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('smtp_auth_user','smtp_auth', adminConfig.smtp_auth)}>edit</i></td></tr>
									<tr><td>Authentication Password</td><td>******* &nbsp;<i className="material-icons edit" onClick={() => this.EditMode('smtp_auth_password','smtp_auth', adminConfig.smtp_auth)}>edit</i></td></tr>
									
								</tbody>
							</table>
						</div>
					</div>
					<div className="col s6">
						<h5><strong>Database</strong></h5>
						<div className="card grey lighten-1 settings-card database">
							<table className="settings-table">
								<thead>
									<tr><th>Item</th><th>Details</th></tr>
								</thead>
								<tbody>
									<tr><td>MongoURI</td><td> {adminConfig.database_url ? this.databaseconfig(adminConfig.database_url).database_url : "flowngin:password1@ds153552.mlab.com:53552/flowngin-dev" }</td></tr>
									<tr><td>user</td><td> {adminConfig.database_url ? this.databaseconfig(adminConfig.database_url).user : "flowngin:password1@ds153552.mlab.com:53552/flowngin-dev" }</td></tr>
									<tr><td>password</td><td> *****</td></tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		);
	}
};

const mapStateToProps = ({ user }) => {
	return { user }
}

const mapDispatchToProps = (dispatch) => {
	return {
		GetAdminConfig: (data) => dispatch(admin.GetAdminConfig(data)),
		EditSetting: (data) => dispatch(admin.EditSetting(data)),
		EditUser: (data) => dispatch(admin.EditUser(data)),
		setApp: (appName) => dispatch(ACT.setApp(appName)),
		loadSidenavConfig: (appName) => dispatch(ACT.loadSidenavConfig(appName))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);