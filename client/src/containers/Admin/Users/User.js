import React, { Component } from 'react'
import './User.css'
import { connect } from 'react-redux'
import UserForm from './UserForm/UserForm';
import * as admin from '../../../actions/admin';
import constant from '../../../utils/constant';

class UserPage extends Component {

  constructor() {
    super()
    this.state = { is_open_adduser: false, userdata :"",levels : [
      {level_type:'Visitor',id:'1'},
      {level_type:'Member',id:'2'},
      {level_type:'Designer',id:'3'},
      {level_type:'Programmer',id:'4'},
      {level_type:'Reserved',id:'5'},
      {level_type:'Tenant',id:'6'},
      {level_type:'Portal Admin',id:'7'},
      {level_type:'super_admin',id:'8'},
    ]}
  }

  
	componentDidMount(){
    const that = this;
    this.props.GetUserList();
  }

  addUser = (e) => {
    if(e){
      this.props.AddNewUser(e).then(data=>{
        window.M.toast({ html: data.message });
        this.props.GetUserList();
        this.setState({is_open_adduser: false})
      }).catch(err=>{
        window.M.toast({ html: "Add Failed. " });
        this.props.GetUserList();
        this.setState({is_open_adduser: false})
      })
    }
  }

  render() {
    var {userLists} = this.props.user
    const that = this;
    userLists = userLists ? userLists : []
    const levels = constant.levels;
    return (
      <div className="user-page">

        <div className="modal-trigger waves-effect waves-light btn" onClick={()=>this.setState({ is_open_adduser: true,userdata : null })}>Add User</div>
        {this.state.is_open_adduser &&
          <div>
            <div id="modal-login-form" className="modal open">
              <div className="close_btn"onClick={()=>this.setState({ is_open_adduser: false, userdata : null })} ><i className="material-icons">close</i></div>
              <UserForm 
                userdata={this.state.userdata}
                levels = {levels}
                addUser = {this.addUser}
                ></UserForm>
            </div>
            <div class="modal-overlay"></div>
          </div>
        }

        <table className="striped mdl-data-table" id="example">
          <thead>
            <tr>
              <th>Full Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Leval</th>
              <th>credits</th>
              <th>status</th>
              <th>Details</th>
            </tr>
          </thead>

          <tbody>

            {userLists.map(function (item, i) {
              return (
                <tr key={i}>
                  <td>{item.firstname + " " + item.lastname}</td>
                  <td>{item.email}</td>
                  <td>{item.phone ? item.phone : "-----"}</td>
                  <td>{levels[item.level-1].name}</td>
                  <td>{item.credits}</td>
                  <td>  <label>
                    <input type="checkbox" className="filled-in" onChange={(e)=>console.log(e)} checked={item.is_verify ? "checked" : ""} />
                    <span>Filled in</span>
                  </label>
                  </td>
                  <td> <div className="modal-trigger waves-effect waves-light btn" onClick={()=>that.setState({ is_open_adduser: true, userdata : item })}>View</div></td>
                </tr>
              )
            })

            }

          </tbody>
        </table>
        {/* <div style={{ "display": "flex" }}>
          <ul className="pagination" style={{ "textAlign": "left" }}>
            <li className="disabled"><a href="#!"><i className="material-icons">chevron_left</i></a></li>
            <li className="active"><a href="#!">1</a></li>
            <li className="waves-effect"><a href="#!">2</a></li>
            <li className="waves-effect"><a href="#!">3</a></li>
            <li className="waves-effect"><a href="#!">4</a></li>
            <li className="waves-effect"><a href="#!">5</a></li>
            <li className="waves-effect"><a href="#!"><i className="material-icons">chevron_right</i></a></li>
          </ul>
          <ul className="pagination" style={{ "right": "25px", "position": "absolute" }}>
            <li className="active"><a href="#!">10</a></li>
          </ul>

        </div> */}

      </div>
    );
  }
};


const mapStateToProps = ({ user }) => {
  return { user }
}

const mapDispatchToProps = (dispatch) => {
  return {
    GetUserList: () => dispatch(admin.GetUserList()),
    AddNewUser : (data) => dispatch(admin.AddNewUser(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);