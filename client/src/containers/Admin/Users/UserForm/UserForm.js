import React, { Component } from 'react'
import './UserForm.css';
class User extends Component {

    state = {
        data: { email: "", firstname: "", lastname: "", level: 0 },
        loading: false,
        errors: ""
    }

    onChange = (e) => {
        const target = e.target;
        if (target instanceof HTMLInputElement) {
            const name = target.name;
            const value = target.value;
            this.setState({
                ...this.state,
                data: { ...this.state.data, [name]: value }
            });
        } else {
            this.setState({
                ...this.state,
                data: { ...this.state.data, [e.target.name]: e.target.value }
            });
        }
    };

    componentDidMount() {
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('select');
            var instances = window.M.FormSelect.init(elems);
        });

        if (this.props.userdata) {
            this.setState({ data: this.props.userdata });
        }
    }

    onSubmit = (e) => {
        e.preventDefault();
        var { email } = this.state.data
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            window.M.toast({ html: "Please enter vailid email address." })
        } else {
            if (this.state.data.level == 0) {
                window.M.toast({ html: "Please selected Level." })
            } else {
                this.props.addUser(this.state.data)
            }
        }
    }


    render() {
        const { data: { email, firstname, lastname } } = this.state;
        const isLoginButtonDisabled = !email || !firstname || !lastname;
        const { levels } = this.props
        const level = this.props.userdata ? this.props.userdata.level : 0;
        return (
            <div className="user-page">
                <div id="UserForm">
                    <h4 className="title center">Add New User</h4>
                    <form onSubmit={this.onSubmit}>
                        <div className="row center">
                            <div className="col s1"></div>
                            <div className="col s10">
                                <div className="row">
                                    <div className="input-field col s6">
                                        <label htmlFor="firstname" className="active">First Name</label>
                                        <input className=""
                                            id="firstname"
                                            type="text"
                                            name="firstname"
                                            value={firstname}
                                            maxLength="256"
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="input-field col s6">
                                        <label htmlFor="lastname" className="active">Last Name</label>
                                        <input className=""
                                            id="lastname"
                                            type="text"
                                            name="lastname"
                                            value={lastname}
                                            maxLength="256"
                                            onChange={this.onChange} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s12">
                                        <div className="row">
                                            <div className="input-field col s6">
                                                <label htmlFor="email" className="active">Email Address</label>
                                                <input className=""
                                                    id="email"
                                                    type="text"
                                                    name="email"
                                                    value={email}
                                                    maxLength="256"
                                                    onChange={this.onChange} />
                                            </div>
                                            <div className="col s6" >
                                                <select name="level" className="selected" defaultValue={level} onChange={this.onChange}>

                                                    {(level == 0) &&
                                                        <option value="0" disabled selected>Choose your option</option>
                                                    }
                                                    {
                                                        levels.map(function (item, i) {
                                                            return (
                                                                <option key={i} value={item.id}>{item.name}</option>
                                                            )
                                                        })
                                                    }
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <button type="submit center" className="waves-effect waves-light btn submit-btn" disabled={isLoginButtonDisabled}>Submit</button>
                            </div>
                            <div className="input-field col s1"></div>
                        </div>
                    </form>
                </div >
            </div>
        );
    }
};

export default User
