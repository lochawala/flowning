import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux';
import * as actions from '../actions';
import './App.css';

import Topnav from './Topnav';
import Dashboard from './Dashboard';
import SurveyList from './SurveyList';
import SurveyResponse from './SurveyResponse';
import SampleCollectionPage from '../components/SampleCollectionPage';

import CollectionPage from './Collection/CollectionPage';
import RecordPage from './Collection/RecordPage';
import CollectionList from './Collection/CollectionList';
import ExternalCollectionPage from './Collection/ExternalCollectionPage';
import DataInput from './Collection/DataInput';

import CreateApp from './Apps/CreateApp';
import AppSettings from './Apps/AppSettings';

import CreateForm from './Form/CreateForm';

import UploadForm from './UploadForm';

import SignupPage from './SignupPage';
import ChangePasswordPage from './ChangePasswordPage';
import PrivateRoute from './Routes/PrivateRoute';

// import Landing from './LandingPage'
import ProfilePage from './ProfilePage';
import LoginPage from './LoginPage';
import User from './Admin/Users/User';
import Settings from './Admin/Settings';
import SidenavSetup from './Sidenav/SidenavSetup';

// import NotFaund from './NotFaundPage'
import InitialLoadPage from './InitialLoadPage';
import TenantRequestPage from './TenantRequestPage';

import surveyPage from './SurveyPage';

const Routers = () => {
  return (
    <div>
      {/* Header */} 
      <Topnav />
     
     {/* Private */} 
     <Route path="/" component={InitialLoadPage} />
      <Route exact path="/" component={Dashboard} />
      <Route exact path="/dashboard" component={Dashboard} />
      <Route exact path="/Profile" component={ProfilePage} />
      <Route exact path="/user" component={User} />
      <Route exact path="/settings" component={Settings} />
      <Route exact path="/data-input" component={DataInput} />
      <Route exact path="/sample-collection" component={SampleCollectionPage} />
      <Route exact path="/collection" component={CollectionPage} />
      <Route exact path="/record" component={RecordPage} />
      <Route exact path="/collection-list" component={CollectionList} />
      <Route exact path="/create-form" component={CreateForm} />
      <Route exact path="/sidenav-setup" component={SidenavSetup} />
      <Route exact path="/external-collection" component={ExternalCollectionPage} />
      <Route exact path="/create-app" component={CreateApp} />
      <Route exact path="/app-settings" component={AppSettings} />
      <Route exact path="/upload-form" component={UploadForm} />
      <Route exact path="/tenantrequest" component={TenantRequestPage} />
      <Route exact path="/Survey-form" component={surveyPage} />
      <Route exact path="/survey" component={SurveyList} />
      <Route exact path="/apps/survey" component={SurveyList} />
      <Route exact path="/survey-response" component={SurveyResponse} />
      <Route exact path="/apps/survey-response" component={SurveyResponse} />
      
      {/* Public */} 
      <Route exact path="/login/:google_id" component={LoginPage} />
      <Route exact path="/login" component={LoginPage} />
      <Route exact path="/registration" component={SignupPage} />
      <Route exact path="/changepassword/:_id" component={ChangePasswordPage} />
    </div>
  )
}

const App = ({ location }) => (
  <Switch>
    <PrivateRoute location={location} path="/" component={Routers} />
  </Switch>
)
export default connect(null, actions)(App)
