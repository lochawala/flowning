import React, { Component } from 'react';
import './ChangePasswordPage.css';
import { changepassword } from "../../actions/users";
import { connect } from "react-redux";


class ChangePasswordPage extends Component {

    constructor(props) {
        super(props);
        this.state = { user_id: "", password: "", cpassword: "", };
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e){
        e.preventDefault();
        let { password, user_id,cpassword } = this.state;
        if(password != cpassword){
            window.M.toast({ html: "Confirm password does't match" })
        }else if(!password || !user_id){
            window.M.toast({ html: "Some thing is worng, Please try agian leter." })
        }else{
            var data = { password: password, user_id: user_id }
            this.props.changepassword(data).then(res=>{
                if(res.status==true){
                    window.M.toast({ html: "your password has been updated successfull." })
                    this.props.history.push("/login");
                }else{
                    window.M.toast({ html: res.message })
                }
            });
        }
       
    }

    componentWillMount(){
        this.setState({user_id :this.props.match.params._id })
    }


    render() {
        let { cpassword, password,} = this.state;
        let { User_data, loginError } = this.props;
        const isLoginButtonDisabled = !cpassword || !password;
        if (this.state.isCall && !User_data) {
            window.M.toast({ html: loginError })
        }
        return (
          <div id="LoginForm">
               <label className="title">Chnage my password</label>
                    <form id="LoginFormData" onSubmit={this.onSubmit}>
                        <div className="row t-left">
                            <div className="col s1"></div>
                            <div className="col s10">
                                <div className="row">
                                    <div className="col s12">
                                        <label htmlFor="email">Password</label>
                                        <input className=""
                                            id="password"
                                            type="password"
                                            name="password"
                                            placeholder="Password"
                                            value={password}
                                            maxLength="256"
                                            onChange={e => this.setState({ password: e.target.value, isCall: false })} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s12">
                                        <label htmlFor="cpassword">Confirm Password</label>
                                        <input className=""
                                            id="cpassword"
                                            type="password"
                                            name="cpassword"
                                            placeholder="Confirm Password"
                                            value={cpassword}
                                            maxLength="256"
                                            onChange={e => this.setState({ cpassword: e.target.value, isCall: false })} />
                                    </div>
                                </div>

                            </div>
                            <div className="col s1"></div>
                        </div>
                        <button type="submit" className="waves-effect waves-light btn submit-btn" disabled={isLoginButtonDisabled}>Submit</button>
                    </form>
                    
                </div >
        );
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        changepassword: (data) => dispatch(changepassword(data)),
    };
}

export default connect(null, mapDispatchToProps)(ChangePasswordPage);
