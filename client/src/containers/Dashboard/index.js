import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as ACT from '../../actions';
import * as user from '../../actions/users';
import './deshboard.css';
import moment from 'moment';
import { Link } from 'react-router-dom';
class Dashboard extends Component {

	constructor() {
		super();
	}

	componentDidMount() {
		if (this.props.user.isLoggedIn) {
			this.props.setApp('default')
			this.props.loadSidenavConfig('default');
			this.setState({ selectedItem: 'dashboard' })
		}
	}


	render() {
		let user = this.props.user.User_data
		return (
			<div style={{ "textAlign": "center", "width": "85%", "margin": "0 auto", "marginTop": "50px" }}>
				{(user.level == 8) &&
					<p>Welcome to Admin</p>
				}
				{(user.level == 2) &&
					<p>Welcome to dashboard</p>
				}
				{(user.level == 6) &&
					<p>Welcome to Tenant </p>
				}
			</div>
		)
	}
}


const mapStateToProps = ({ user }) => {
	return { user }
}

const mapDispatchToProps = (dispatch) => {
	return {
		setApp: (appName) => dispatch(ACT.setApp(appName)),
		loadSidenavConfig: (appName) => dispatch(ACT.loadSidenavConfig(appName)),
		logout: () => dispatch(user.logout())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
