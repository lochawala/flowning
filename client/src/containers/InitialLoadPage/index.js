import { Component } from 'react';
import { connect } from 'react-redux';
import * as ACT from '../../actions';
import * as user from '../../actions/users';
class InitialLoadPage extends Component {

  componentDidMount() {

    if (this.props.user.isLoggedIn) {
      if (!this.props.user.sidenavConfig) {
        this.props.setApp('default')
        this.props.loadSidenavConfig('default')
      }
      this.setState({ selectedItem: 'dashboard' })
    }
  }

  render() { return true }
}


const mapStateToProps = ({ user }) => {
  return { user }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setApp: (appName) => dispatch(ACT.setApp(appName)),
    loadSidenavConfig: (appName) => dispatch(ACT.loadSidenavConfig(appName))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialLoadPage);
