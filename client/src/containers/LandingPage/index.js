import React, { Component } from 'react';

export default class Landing extends Component {

	render() {
		return (
	    <div style={{ textAlign: 'center' }}>
	      <h1>Flowngin</h1>
	      Collaborate made simple
	    </div>
	  );
	}
}
