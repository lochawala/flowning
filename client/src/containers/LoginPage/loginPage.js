import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './loginPage.css';
import { verifyemails, doforget, login} from "../../actions/users";
import { loginwithGoogle, fetchUser } from '../../actions/index'
import { connect } from "react-redux";
import { checkTenatLogin, getQueryString } from "../../utils/helperFunctions";
import { check } from 'express-validator/check';
class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = { email: "", password: "", isCall: false, isforget: false, isTenantLogin: false };
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        this.state.isCall = true
        var payload = {
            email: this.state.email,
            password: this.state.password,
            isLogin: this.state.isTenantLogin ? 2 : 1,
            connection: this.props.connection
        }

        this.props.login(payload).then(response => {
            if (response.status === false) {
                window.M.toast({ html: response.message })
            }
        });
    }

    componentWillMount() {
        if (this.props.location.search) {
            var name = this.props.location.search.split("?")[1].split("=")[0];
            var value = this.props.location.search.split("?")[1].split("=")[1];
            if (name == "email") {
                this.setState({ email: getQueryString().email });
            } else {
                this.verifyemail(name, value);
            }

        }
    }

    verifyemail = async (name, value) => {
        var response = await this.props.verifyemails(value);
        if (name === "token") {
            if (response.status === true) {
                this.props.history.push("/changepassword/" + response.user_id);
            } else {
                window.M.toast({ html: response.message })
            }
        } else {
            window.M.toast({ html: response.message })
        }
    }

    componentDidMount() {
        if (checkTenatLogin()) {
            this.setState({ isTenantLogin: true });
        }
        if (this.props.match.params.google_id && this.props.match.params.google_id != 0) {
            this.props.loginwithGoogle(this.props.match.params.google_id);
        }
    }

    forgetPassword = () => {
        this.setState({ isforget: true })
    }

    isLogin = () => {
        this.setState({ isforget: false })
    }


    onSubmitforget = (e) => {
        e.preventDefault();
        let { email } = this.state;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            window.M.toast({ html: "Please enter vailid email address." })
        } else {
            this.props.doforget({ email: email }).then(res => {
                window.M.toast({ html: res.message });
                if (res.status == true) {
                    this.setState({ isforget: false, email: "", password: "" })
                }
            });
        }
    }

    render() {

        let { email, password, isforget, isTenantLogin } = this.state;
        const isLoginButtonDisabled = !email || !password;

        return (
            <div>
                {!isforget ?

                    (!isTenantLogin) ?
                        <div id="LoginForm">

                            <div className="loginwithGoogle">
                                <a className="div_google" href="/auth/google">
                                    <svg viewBox="0 0 18 18" role="presentation" aria-hidden="true" focusable="false">
                                        <g fill="none" fillRule="evenodd">
                                            <path d="M9 3.48c1.69 0 2.83.73 3.48 1.34l2.54-2.48C13.46.89 11.43 0 9 0 5.48 0 2.44 2.02.96 4.96l2.91 2.26C4.6 5.05 6.62 3.48 9 3.48z"
                                                fill="#EA4335"></path>
                                            <path d="M17.64 9.2c0-.74-.06-1.28-.19-1.84H9v3.34h4.96c-.1.83-.64 2.08-1.84 2.92l2.84 2.2c1.7-1.57 2.68-3.88 2.68-6.62z"
                                                fill="#4285F4"></path>
                                            <path d="M3.88 10.78A5.54 5.54 0 0 1 3.58 9c0-.62.11-1.22.29-1.78L.96 4.96A9.008 9.008 0 0 0 0 9c0 1.45.35 2.82.96 4.04l2.92-2.26z"
                                                fill="#FBBC05"></path>
                                            <path d="M9 18c2.43 0 4.47-.8 5.96-2.18l-2.84-2.2c-.76.53-1.78.9-3.12.9-2.38 0-4.4-1.57-5.12-3.74L.97 13.04C2.45 15.98 5.48 18 9 18z"
                                                fill="#34A853"></path>
                                            <path d="M0 0h18v18H0V0z"></path>
                                        </g>
                                    </svg>
                                    <label className="fb_signup_txt">Sign in with Google</label>
                                </a>
                            </div>

                            <div className="row center">
                                <div className="input-field col s1"></div>
                                <div className="input-field col s11">
                                    <div className="row" style={{ "padding": "10px 15px 10px 16px" }}>
                                        <div className="col s5" style={{ "padding": "0" }}>
                                            <hr />
                                        </div>
                                        <div className="col or_txt_signup" style={{ "marginTop": "-5px !important" }}>
                                            or
                                </div>
                                        <div className="col s5" style={{ "padding": "0" }}>
                                            <hr />
                                        </div>
                                    </div>
                                </div>
                                {/* <div className="input-field col s1"></div> */}
                            </div>
                            <form id="LoginFormData" onSubmit={this.onSubmit}>
                                <div className="row t-left">
                                    <div className="col s1"></div>
                                    <div className="col s10">
                                        <div className="row">
                                            <div className="col s12">
                                                <label className="validate">Sign in with your e-mail address</label>
                                                <input className=""
                                                    id="email"
                                                    type="text"
                                                    name="email"
                                                    placeholder="Email"
                                                    value={email}
                                                    maxLength="256"
                                                    onChange={e => this.setState({ email: e.target.value.toLowerCase(), isCall: false })} />


                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <label htmlFor="email">Password</label>
                                                <input className=""
                                                    id="password"
                                                    type="password"
                                                    name="password"
                                                    placeholder="Password"
                                                    value={password}
                                                    maxLength="256"
                                                    onChange={e => this.setState({ password: e.target.value, isCall: false })} />

                                            </div>
                                        </div>

                                    </div>
                                    <div className="col s1"></div>
                                </div>
                                <button type="submit" className="waves-effect waves-light btn submit-btn" disabled={isLoginButtonDisabled}>Sign In</button>
                            </form>
                            <p style={{ "textAlign": "right", "marginRight": "41px" }}>
                                <a href="#" onClick={this.forgetPassword}>Forget Password</a>
                            </p>
                            <p className="footer_login"><b>Not account yet? &nbsp;
          <Link
                                    to="/registration"
                                    onClick={this.handleClickDashboard}>
                                    Sign Up&nbsp;
          </Link>
                                with your email</b></p>
                        </div >
                        :
                        <div id="LoginForm" >
                            <h6 style={{ "marginTop": "-35px", "marginBottom": "30px" }}>Sign-in with company email</h6>
                            <form id="LoginFormData" onSubmit={this.onSubmit}>
                                <div className="row t-left">
                                    <div className="col s1"></div>
                                    <div className="col s10">
                                        <div className="row">
                                            <div className="col s12">
                                                <label className="validate">Sign in with your e-mail address</label>
                                                <input className=""
                                                    id="email"
                                                    type="text"
                                                    name="email"
                                                    placeholder="Email"
                                                    value={email}
                                                    maxLength="256"
                                                    onChange={e => this.setState({ email: e.target.value.toLowerCase(), isCall: false })} />


                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <label htmlFor="email">Password</label>
                                                <input className=""
                                                    id="password"
                                                    type="password"
                                                    name="password"
                                                    placeholder="Password"
                                                    value={password}
                                                    maxLength="256"
                                                    onChange={e => this.setState({ password: e.target.value, isCall: false })} />

                                            </div>
                                        </div>

                                    </div>
                                    <div className="col s1"></div>
                                </div>
                                <button type="submit" className="waves-effect waves-light btn submit-btn" disabled={isLoginButtonDisabled}>Sign In</button>
                            </form>
                            <p style={{ "textAlign": "right", "marginRight": "41px" }}>
                                <a href="#" onClick={this.forgetPassword}>Forget Password</a>
                            </p>
                        </div >

                    :
                    <div id="LoginForm">
                        <label className="title">Forget my password</label>
                        <br /><br />
                        <form id="LoginFormData" onSubmit={this.onSubmitforget}>
                            <div className="row t-left">
                                <div className="col s1"></div>
                                <div className="col s10">
                                    <div className="row">
                                        <div className="col s12">
                                            <label className="validate">Sign in with your e-mail address</label>
                                            <input className=""
                                                id="email"
                                                type="text"
                                                name="email"
                                                placeholder="Email"
                                                value={email}
                                                maxLength="256"
                                                onChange={e => this.setState({ email: e.target.value.toLowerCase(), isCall: false })} />


                                        </div>
                                    </div>
                                </div>
                                <div className="col s1"></div>
                            </div>
                            <button type="submit" className="waves-effect waves-light btn submit-btn" disabled={!email}>Submit</button>
                        </form>
                        <p style={{ "textAlign": "center", "display": "inline-flex" }}>
                            Back to <a href="#" onClick={this.isLogin}>&nbsp; Login</a>
                        </p>

                    </div >
                }
            </div>
        );
    }
}





const mapStateToProps = (state) => {
    return {
        User_data: state.user.User_data,
        connection: state.user.TenantConnection,
        loginError: state.user.loginError
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (payload) => dispatch(login(payload)),
        verifyemails: (code) => dispatch(verifyemails(code)),
        doforget: (email) => dispatch(doforget(email)),
        loginwithGoogle: (data) => dispatch(loginwithGoogle(data)),
        fetchUser: () => dispatch(fetchUser())

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
