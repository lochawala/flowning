import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as user from '../../actions/users';
import './ProfilePage.css'

class ProfilePage extends Component {

  constructor() {
		super()
		this.state = { firstname:"",lastname:"",email:"",phone:"",address:"",mobile:"",id : "" }
  }

  componentDidMount(){
    if(this.props.user.User_data){
      const {firstname,lastname,email,phone,address,mobile,_id} = this.props.user.User_data
      
      this.setState({
        firstname:firstname,
        lastname : lastname,
        email :email,
        mobile : mobile,
        address : address,
        id : _id
      })
    }
  }

  onChange = (e) =>{
    const target = e.target;
    if (target instanceof HTMLInputElement) {
      const name = target.name;
      const value = target.value;
      this.setState({
          ...this.state,
          [name]: value
      });
  }
}

onSubmit = (e) =>{
  e.preventDefault();
    this.props.updateProfile(this.state).then(res=>{
      window.M.toast({ html: res.message })
    });
    const {firstname,lastname,address,mobile} = this.state
    this.props.user.User_data.firstname = firstname
    this.props.user.User_data.lastname = lastname
    this.props.user.User_data.mobile = mobile
    this.props.user.User_data.address = address
    localStorage.user = JSON.stringify(this.props.user.User_data);
 }

  

  render() {
    const {firstname,lastname,email,phone,address,mobile} = this.state
    console.log(this.props.user);
    let levels = [], level = 0;
    return (
      <div className="user-page">
        <div id="UserForm">
          <h4 className="title center">Profile</h4>
          <form onSubmit={this.onSubmit}>
            <div className="row center">
              <div className="col s1"></div>
              <div className="col s10">
                <div className="row">
                  <div className="input-field col s6">
                    <label htmlFor="firstname" className="active">First Name</label>
                    <input className=""
                      id="firstname"
                      type="text"
                      name="firstname"
                      value={firstname}
                      maxLength="256"
                      onChange={this.onChange} />
                  </div>
                  <div className="input-field col s6">
                    <label htmlFor="lastname" className="active">Last Name</label>
                    <input className=""
                      id="lastname"
                      type="text"
                      name="lastname"
                      value={lastname}
                      maxLength="256"
                      onChange={this.onChange} />
                  </div>
                </div>
                <div className="row">
                  <div className="col s12">
                    <div className="row">
                      <div className="input-field col s6">
                        <label htmlFor="email" className="active">Email Address</label>
                        <input className=""
                          id="email"
                          readOnly="true"
                          type="text"
                          name="email"
                          value={email}
                          maxLength="256"
                          onChange={this.onChange} />
                      </div>
                      <div className="col input-field s6" >
                        <label htmlFor="phone" className="active">Phone Number</label>
                        <input className=""
                          id="phone"
                          type="text"
                          name="phone"
                          value={phone}
                          maxLength="256"
                          onChange={this.onChange} />
                      </div>
                    </div>

                  </div>
                </div>

                <div className="row">
                  <div className="col s12">
                    <div className="row">
                      <div className="input-field col s6">
                        <label htmlFor="address" className="active">Address</label>
                        <input className=""
                          id="address"
                          type="text"
                          name="address"
                          value={address}
                          maxLength="256"
                          onChange={this.onChange} />
                      </div>
                      <div className="col input-field s6" >
                        <label htmlFor="mobile" className="active">Mobile Number</label>
                        <input className=""
                          id="mobile"
                          type="text"
                          name="mobile"
                          value={mobile}
                          maxLength="256"
                          onChange={this.onChange} />
                      </div>
                    </div>

                  </div>
                </div>
                <button type="submit center" className="waves-effect waves-light btn submit-btn">Update Profile</button>
              </div>
              <div className="input-field col s1"></div>
            </div>
          </form>
        </div >
      </div>

    );
  }
}

const mapStateToProps = ({ user }) => {
	return { user }
}

const mapDispatchToProps = (dispatch) => {
	return {
    updateProfile : (data) => dispatch(user.updateProfile(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);