import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

var pulbicRouter = ['login','registration','changepassword','auth'];
		var accessRouter = ['Survey-form'];

const PrivateRoute = ({ isAuthenticated, level ,component: Component, ...rest }) =>{
  var obj = {...rest},routes, router = obj.location.pathname.split('/')[1];
  if(accessRouter.indexOf(router) == 0){
    routes = <Route {...rest} render={props => <Component {...props} />} />
  }else if( pulbicRouter.indexOf(router) < 0 ){  //-1 is true
    routes = <Route {...rest} render={props => isAuthenticated ? (router==="") ? <Redirect to="/dashboard" /> : <Component {...props} /> : <Redirect to="/login" />} />
  }else{
    routes = <Route {...rest} render={props => isAuthenticated ? <Redirect to="/dashboard"/> : <Component {...props} />} />
  }
  

 return routes
} 

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.isLoggedIn,
    level : state.user.isLoggedIn ? state.user.User_data.level : null 
  }
}

export default connect(mapStateToProps)(PrivateRoute);
