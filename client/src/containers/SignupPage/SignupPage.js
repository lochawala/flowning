import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './SignupPage.css';
import { signup } from "../../actions/users";
import { connect } from "react-redux";

class SignupPage extends Component {

    state = {
        data: { email: "", password: "", fname: "", lname: "", cpassword: "" },
        loading: false,
        errors: ""
    }

    onChange = (e) => {
        const target = e.target;
        if (target instanceof HTMLInputElement) {
            const name = target.name;
            const value = target.value;
            this.setState({
                ...this.state,
                data: { ...this.state.data, [name]: value }
            });
        }
    };

    onSubmit = (e) => {
        e.preventDefault();
        var { email, password, cpassword } = this.state.data
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            window.M.toast({ html: "Please enter vailid email address." })
        } else if (password !== cpassword) {
            window.M.toast({ html: "Confirm password does't match" })
        } else {
            this.props.signup(this.state.data).then((res) => {
                window.M.toast({ html: res.message });
                this.setState({data : { email: "", password: "", fname: "", lname: "", cpassword: "" }});
            });
        }
    }
    
    render() {
        const { data: { email, password, fname, lname, cpassword } } = this.state;
        const isLoginButtonDisabled = !email || !password || !fname || !lname || !cpassword;

        return (
            <div id="SignupForm">
                <label className="title">Sign in with your e-mail address</label>
                <form onSubmit={this.onSubmit}>
                    <div className="row center">
                        <div className="input-field col s1"></div>
                        <div className="input-field col s10">
                            <div className="row">
                                <div className="input-field col s6">
                                    <input className=""
                                        id="fname"
                                        type="text"
                                        name="fname"
                                        placeholder="First Name"
                                        value={fname}
                                        maxLength="256"
                                        onChange={this.onChange} />
                                </div>
                                <div className="input-field col s6">
                                    <input className=""
                                        id="lname"
                                        type="text"
                                        name="lname"
                                        placeholder="Last Name"
                                        value={lname}
                                        maxLength="256"
                                        onChange={this.onChange} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input className=""
                                        id="email"
                                        type="text"
                                        name="email"
                                        placeholder="Email"
                                        value={email}
                                        maxLength="256"
                                        onChange={this.onChange} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input className=""
                                        id="password"
                                        type="password"
                                        name="password"
                                        placeholder="Password"
                                        value={password}
                                        maxLength="256"
                                        onChange={this.onChange} />

                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input className=""
                                        id="cpassword"
                                        type="password"
                                        name="cpassword"
                                        placeholder="Confirm Password"
                                        value={cpassword}
                                        maxLength="256"
                                        onChange={this.onChange} />

                                </div>
                            </div>
                        </div>
                        <div className="input-field col s1"></div>
                    </div>
                    <button type="submit" className="waves-effect waves-light btn submit-btn" disabled={isLoginButtonDisabled}>Sign UP</button>
                </form>
                <p className="footer_login"><b>Already Sign-up? &nbsp;
                 <Link
                        to="/login"
                        onClick={this.handleClickDashboard}>
                        Sign In&nbsp;
                 </Link>
                    to your account</b></p>

            </div >
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signup: (data) => dispatch(signup(data))
    };
}

export default connect(null, mapDispatchToProps)(SignupPage);
