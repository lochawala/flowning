import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as ACT from '../../actions';
import * as user from '../../actions/users';
import { getUrl } from '../../../src/utils/helperFunctions'
import { ServeyList, ServeyDelete } from '../../actions/survey';
import './SurveyList.css';
import moment from 'moment';
import { Link } from 'react-router-dom';
class SurveyList extends Component {

	constructor() {
		super()
		this.state = {
			myurveryList: [],
			UsersurveyList: [],
			link: "",
			is_create_survey : false,
			survey_category : "default",
			survey_title : ""
		}
	}

	componentDidMount() {
		if (this.props.user.isLoggedIn) {
			this.props.setApp('default')
			this.props.loadSidenavConfig('default');
			this.setState({ selectedItem: 'dashboard' })
		}
	}

	componentWillMount() {
		this.GetSurveyList()
	}

	GetSurveyList = () => {
		this.props.ServeyList().then(data => {
			if (data) {
				const that = this;
				var surveyList = data.data.data
				if (surveyList) {
					if (that.props.user.User_data.level == 8) {
						surveyList.map(function (item) {

							if (item.is_owner_suervey)
								that.state.myurveryList.push(item)
							else
								that.state.UsersurveyList.push(item)

						})
					} else {
						surveyList.map(function (item) {
							if (item.shared_users) {
								var resdata = item.shared_users.find((x) => x == that.props.user.User_data._id)
								if (resdata) {
									that.state.myurveryList.push(item)
								}
							}
						})
					}

					this.setState(this.state);
				}
			}
		});
	}

	GetWelcome(level) {
	}

	EditSurvey = (item) => {
		window.location.reload();
		this.props.history.push('/Survey-form?id=' + item.external_link + '&view=true');
	}

	setLink = (url) => {
		this.setState({ link: getUrl() + "/Survey-form?id=" + url })
	}

	ServeyDelete = (item, i) => {
		const that = this;
		this.props.ServeyDelete({ id: item._id }).then(() => {
			that.state.myurveryList.splice(i, 1);
			that.setState(that.state)
			window.M.toast({ html: "Deleted successfully." })
		})
	}

	GoSurveyResponse = (id) => {
		this.props.history.push("survey-response?id=" + id)
	}

	CreateSurvey = () => {
		var { survey_category,survey_title } = this.state;
		this.props.history.push({ 
		pathname: '/Survey-form',
		state: { survey_title: survey_title, survey_category : survey_category}})
	}	

	render() {
		let user = this.props.user.User_data
		var { myurveryList, UsersurveyList, link,is_create_survey,survey_category,survey_title } = this.state;
		const that = this;
		return (
			<div style={{ "textAlign": "center", "width": "85%", "margin": "0 auto", "marginTop": "50px" }}>

				{is_create_survey &&
					<div id="modal3" className="modal open questions_model create-survey-model"  >
						<div className="model-header">
							<h6 style={{ "padding": "10px" }}>Create Survey</h6>
						</div>
						<div className="modal-content">
							<div style={{ width: "100%" }} className="input-field" >
								<input value={survey_title}    onChange={(e) => this.setState({survey_title : e.target.value}) } id="survey_name" type="text" class="validate" />
								<label for="survey_name">Survey Name </label>
							</div>
							<select class="browser-default" name="survey_category" defaultValue={survey_category} onChange={(e) => this.setState({survey_category : e.target.value})}>
								<option value="default" disabled selected>Select a category</option>
								<option value="Feedback" >Feedback</option>
								<option value="Customer_Satisfaction" >Customer Satisfaction</option>
								<option value="Community" >Community</option>
								<option value="Events" >Events</option>
								<option value="Employees" >Employees</option>
								<option value="Market_Research" >Market Research</option>
							</select>

						</div>
						<div className="modal-footer">
							<button href="#!" className="btn-flat" onClick={(e) => this.CreateSurvey()} >Add</button>
							<button href="#!" className="btn-flat" onClick={(e)=> this.setState({is_create_survey : false})} >Cancel</button>
						</div>
					</div>
				}


				<div id="modal1" class="modal">
					<div class="modal-content">
						<input value={link}></input>
					</div>
					<div class="modal-footer">
						<a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
					</div>
				</div>

				{(user.level == 8 || myurveryList.length) &&
					<div style={{ "marginBottom": "40px" }}>
						<div id="survey">
							<div className="row center" style={{ "marginBottom": "25px" }}>
								<div className="input-field col s2 paddingLeft-0">
									<h6 style={{ "textAlign": "left" }}><b>My survey</b></h6>
								</div>
								<div className="input-field col s2">
								</div>
								<div className="input-field col s4"></div>
								<div className="input-field col s2">
									<div style={{ "margin-top": "-7px" }}>
									</div>
								</div>
								<div className="input-field col s2">
									{/* <Link to={'/Survey-form'}> */}
										<button onClick={(e)=> this.setState({is_create_survey : true})} type="button" className="waves-effect waves-light btn submit-btn" >Create Survey</button>
									{/* </Link> */}
								</div>
							</div>

							<table className="striped mdl-data-table" id="surveylist" style={{ "TextAlign": "center", "marginTop": "15px" }}>
								<thead style={{ "border": "2px solid " }}>
									<tr>
										<th className="title-th">TITILE</th>
										<th >CREATED</th>
										<th >MODIFED</th>
										<th >PUBLISHED</th>
										<th >VAILID TIL</th>
										<th className="action-th">ACTIONS</th>
									</tr>
								</thead>

								{myurveryList.map((item, key) => {
									return (<tr className={key % 2 == 1 ? 'color-gray' : ''}>
										<td>{item.survey_title}</td>
										<td>{moment(item.createdAt).format("DD/MM/YYYY")}</td>
										<td>{item.updatedAt == "" ? "---" : moment().format("DD/MM/YYYY")}</td>
										<td>{moment(item.datePublished).format("DD/MM/YYYY")}</td>
										<td>{moment(item.validTill).format("DD/MM/YYYY")}</td>
										<td>
											{(new Date() <= new Date(item.datePublished) || new Date() == new Date(item.datePublished)) &&
												<button style={{ "marginRight": "5px" }} onClick={(e) => this.EditSurvey(item)} className="btn"><i className="material-icons " center>edit</i></button>
											}

											{(new Date() < new Date(item.validTill)) &&
												<button style={{ "marginRight": "5px" }} className="btn"><i className="material-icons " center onClick={(e) => that.ServeyDelete(item, key)}>delete_forever</i></button>
											}

											<button style={{ "marginRight": "5px" }} className="btn modal-trigger" onClick={(e) => that.setLink(item.external_link)} href="#modal1"><i className="material-icons " center>more_vert</i></button>

											{/* { (new Date(item.datePublished) >= new Date()) &&  */}
											<button onClick={(e) => that.GoSurveyResponse(item._id)} style={{ "marginRight": "5px" }} className="btn"><i className="material-icons " center>remove_red_eye</i></button>
											{/* } */}
										</td>
									</tr>)
								})}

							</table>
						</div>
						{(user.level == 8) &&
							<div id="survey">
								<div className="row center" style={{ "marginBottom": "40px" }}>
									<div className="input-field col s2 paddingLeft-0">
										<h6 className="textalingLeft"><b>Teams survey</b></h6>
									</div>
									<div className="input-field col s2">
										{/* <select name="level" className="selected">
									<option value="0" selected>option</option>
									<option value="1">option1</option>
									<option value="2">option2</option>
									<option value="3">option3</option>
								</select> */}
									</div>
									<div className="input-field col s4"></div>
									<div className="input-field col s2">
										<div style={{ "margin-top": "-7px" }}>
											{/* <i class="material-icons prefix">search</i>
									<input type="text" placeholder="search" id="autocomplete-input" class="autocomplete red-text" /> */}
										</div>
									</div>
								</div>

								<table className="striped mdl-data-table" id="surveylist" style={{ "TextAlign": "center", "marginTop": "15px" }}>
									<thead style={{ "border": "2px solid " }}>
										<tr>
											<th className="title-th">TITILE</th>
											<th >CREATED</th>
											<th >MODIFED</th>
											<th >PUBLISHED</th>
											<th >VAILID TIL</th>
											<th className="action-th">ACTIONS</th>
										</tr>
									</thead>

									{UsersurveyList.map((item, key) => {
										return (<tr className={key % 2 == 1 ? 'color-gray' : ''}>
											<td>{item.survey_title}</td>
											<td>{moment(item.createdAt).format("DD/MM/YYYY")}</td>
											<td>{item.updatedAt == "" ? "---" : moment().format("DD/MM/YYYY")}</td>
											<td>{moment(item.datePublished).format("DD/MM/YYYY")}</td>
											<td>{moment(item.validTill).format("DD/MM/YYYY")}</td>
											<td>
												{(new Date() <= new Date(item.datePublished) || new Date() == new Date(item.datePublished)) &&
													<button onClick={(e) => this.EditSurvey(item)} className="btn"><i className="material-icons " center>edit</i></button>
												}
												&nbsp;&nbsp;
											{(new Date() < new Date(item.validTill)) &&
													<button className="btn"><i className="material-icons " center onClick={(e) => that.ServeyDelete(item, key)}>delete_forever</i></button>
												}
												&nbsp;&nbsp;
											{(new Date(item.validTill) < new Date()) &&
													<button className="btn"><i className="material-icons " center>remove_red_eye</i></button>
												}
												<button className="btn"><i className="material-icons " center>more_vert</i></button>
											</td>
										</tr>)
									})}

								</table>
							</div>
						}

					</div>
				}
				{(user.level == 2) &&
					<p>Welcome to dashboard</p>
				}
				{(user.level == 6) &&
					<p>Welcome to Tenant </p>
				}
			</div>
		)
	}
}


const mapStateToProps = ({ user }) => {
	return { user }
}

const mapDispatchToProps = (dispatch) => {
	return {
		setApp: (appName) => dispatch(ACT.setApp(appName)),
		loadSidenavConfig: (appName) => dispatch(ACT.loadSidenavConfig(appName)),
		ServeyList: () => dispatch(ServeyList()),
		ServeyDelete: (id) => dispatch(ServeyDelete(id)),
		logout: () => dispatch(user.logout())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SurveyList);
