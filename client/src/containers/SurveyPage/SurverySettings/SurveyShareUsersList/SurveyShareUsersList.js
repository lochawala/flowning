import React, { Component } from 'react';
import M from 'materialize-css/dist/js/materialize.min.js';
import { connect } from 'react-redux';
import {GetUserList} from '../../../../actions/admin';
class SurveyShareUsersList extends Component {

    constructor() {
        super();
        this.state = {
            userLists : []
        }
    }

    componentDidMount() {
        this.props.GetUserList();        
    }

    componentWillReceiveProps(props){
        if(!props.userLists){
            return false
        }else{
          this.setState({userLists : props.userLists})
        }
    }

    selectedusers(i){
        this.state.userLists[i].is_selected = this.state.userLists[i].is_selected ? false : true;
        this.setState(this.state);
    }

    submit(){
        var users = [];
        this.state.userLists.forEach(element=>{
            if(element.is_selected){
                users.push(element._id)
            }
        });
        this.props.seletedusers(users);

    }
    
    render() {
        var {userLists} = this.state
        const that = this;
        return (
            <div className="survey_userslist">
                <div id="modal1" className="modal" >
                <div>
                <div className="model-header">
                        <h4 style={{ "borderBottom": "1px solid", "padding" : "10px" }}>User Lists </h4>
                </div>
                    <div className="modal-content" style={{"height": "300px","overflow": "auto"}}>
                        <div>
                            <table>
                                <th>First name</th>
                                <th>Lirst name</th>
                                <th>Email address</th>
                                <th>Select</th>
                                {
                                    userLists.map(function (item,i) {
                                        return (
                                            <tr>
                                                <td>{item.firstname}</td>
                                                <td>{item.lastname}</td>
                                                <td>{item.email}</td>
                                                <th onClick={(e)=>that.selectedusers(i)}>  <input type="checkbox" className="filled-in"  checked={item.is_selected ? "checked" : ""} /><span></span></th>
                                            </tr>
                                        )
                                    })
                                }
                            </table>

                        </div>
                    </div>
                    <div className="modal-footer">
                        <button href="#!" onClick={(e) => this.submit()} className="modal-close waves-effect waves-green btn-flat" >Agree</button>
                    </div>
                </div>
                 </div>   
            </div>
        )

    }
}

const mapStateToProps = ({ user }) => {
    var userLists = user.userLists ? user.userLists : []
    return { userLists }
  }

const mapDispatchToProps = (dispatch) => ({
    GetUserList: (data) => dispatch(GetUserList())
})

export default connect(mapStateToProps, mapDispatchToProps)(SurveyShareUsersList);