
import React, { Component } from 'react';
import { connect } from 'react-redux';
import M from 'materialize-css/dist/js/materialize.min.js';
import moment from 'moment';

class QuestionsTypes extends Component {


    render() {
        const selected_questions = this.props.selected_questions
        return (
            <div>
                <div style={{ "fontWeight": "bold", "borderBottom": "1px solid #e5e4e4", "height": "40px", "marginTop": "14px" }}>Question Type</div>
                <table>
                    {/* <tr className={(selected_questions == 1) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.props.selectedQuestiontype(1)}>
                        <td style={{ "width": "95px" }}><input type="range" id="test5" min="0" readOnly max="100" /></td>
                        <td>Slidder</td>
                    </tr> */}
                    {/* <tr className={(selected_questions == 2) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) =>  this.props.selectedQuestiontype(2)}>
                        <td style={{ "width": "95px" }}>
                            <div class="switch"><label><input disabled checked="checked" type="checkbox" /><span class="lever"></span></label></div>
                        </td>
                        <td>Yes/No Question</td>
                    </tr> */}
                    {/* <tr className={(selected_questions == 3) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.props.selectedQuestiontype(3)}>
                        <td style={{ "width": "95px", "textAlign": "center", "color": "#26a69a" }}>
                            <label><input type="checkbox" class="filled-in" checked="checked" /><span /></label>
                        </td>
                        <td>Checkboxs</td>
                    </tr> */}
                    <tr className="questions_list rating_scaling" >
                        <td style={{ "width": "60px", "textAlign": "center", "color": "#26a69a" }}><i className="material-icons " center>linear_scale</i></td>
                        <td>Rating Scale</td>
                        <td><button className="btn question-button" onClick={(e) => this.props.selectedQuestiontype("rating_scaling")}>Add</button></td>
                    </tr>
                    {/* <tr className={(selected_questions == 5) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.props.selectedQuestiontype(5)}>
                        <td style={{ "width": "95px" }}></td>
                        <td>Ranking</td>
                    </tr> */}
                    {/* <tr className={(selected_questions == 6) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.props.selectedQuestiontype(6)}>
                        <td style={{ "width": "95px", "textAlign": "center", "color": "#26a69a" }}> <label><input name="group1" type="radio" /><span></span></label></td>
                        <td>RadioButton</td>
                    </tr> */}
                    {/* <tr className={(selected_questions == 7) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.props.selectedQuestiontype(7)}>
                        <td style={{ "width": "95px", "textAlign": "center", "color": "#26a69a" }}><i className="material-icons " center>star_half</i></td>
                        <td>Start Rating</td>
                    </tr> */}
                    {/* <tr className={(selected_questions == 8) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.props.selectedQuestiontype(8)}>
                        <td style={{ "width": "95px", "textAlign": "center", "color": "#26a69a" }}><i className="material-icons " center>input</i></td>
                        <td>Textbox</td>
                    </tr> */}
                    <tr className="questions_list commentbox" >
                        <td style={{ "width": "60px", "textAlign": "center", "color": "#26a69a" }}><i className="material-icons " center>insert_comment</i></td>
                        <td>Comment Box</td>
                        <td><button className="btn question-button" onClick={(e) => this.props.selectedQuestiontype("commentbox")}>Add</button></td>
                    </tr>
                    {/* <tr className={(this.getSelectedQuestionType() == 10) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.selectedQuestiontype(10)}>
                 <td style={{ "width": "95px", "textAlign": "center", "color": "#26a69a" }}><i className="material-icons " center>ac_unit</i></td>
                 <td>NPS</td>
             </tr>
             <tr className={(this.getSelectedQuestionType() == 11) ? "questions_list seleted_question_type" : "questions_list"} onClick={(e) => this.selectedQuestiontype(11)}>
                 <td style={{ "width": "95px", "textAlign": "center", "color": "#26a69a" }}><i className="material-icons " center>keyboard_arrow_down</i></td>
                 <td>Dropdown</td>
             </tr> */}
                </table>
            </div>
        )
    }

}

export default QuestionsTypes