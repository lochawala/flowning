import React, { Component } from 'react';
class SurveyAddQuestionModel extends Component {

    constructor() {
        super();
        this.state = {
            question_date: {
                question: "",
                rows: [],
                columns: []
            },
            rowValue: "",
            columnsValue: ""
        }
    }

    componentDidMount(){
        this.questionInput.focus()
    }

    render() {
      
        var { rowValue, columnsValue ,selected_questions_type } = this.props;
        var { question, rows, columns } = this.props.question_date;
        const that = this;

    
        return (
            <div id="modal2" className="modal open questions_model" style={{ "width": "50%","height":"100%" }} >
            <div>
                <div className="model-header">

                    {(selected_questions_type == "rating_scaling") &&
                        <h6 style={{ "padding": "10px" }}>Rating scale </h6>
                    }

                    {(selected_questions_type == "commentbox") &&
                        <h6 style={{ "padding": "10px" }}>Comment Box</h6>
                    }

                </div>
                <div className="modal-content" style={{ "height": "100%", "overflow": "auto" }}>
                    <div>
                        <div style={{ width: "70%" }} className="input-field" >
                            <input ref={(input) => { this.questionInput = input; }} onChange={(e) => that.props.changeQuestionsPage(e)} value={question}  type="text" class="validate" />
                            <label for="last_name">Enter your question </label>
                        </div>
                        {(selected_questions_type == "rating_scaling") &&
                            <div>
                                <div>
                                    <p><label style={{ "lineHeight": "2", "fontSize": "15px", "marginRight": "20px" }}><b>Rows</b></label></p>

                                    <div className="row">
                                        <div className="col s10"><input onKeyPress={(e) => this.props.RowandColumnsValue(e, 'rows')} style={{ "border": "2px solid", "height": "30px", "paddingLeft": "2px" }} onChange={(e => this.props.RowandColumnsValue(e, 'rows'))} value={rowValue} id="last_name" type="text" class="validate" /></div>
                                        <div className="col s1" onClick={(e) => this.props.addRowsColumns('rows')}><i className="material-icons " center>add_circle</i></div>
                                        <div className="col s1"></div>
                                    </div>
                                    {
                                        rows.map(function (item, i) {
                                            return (
                                                <div className="row">
                                                    <div className="col s10"><input style={{ "border": "2px solid", "height": "30px", "paddingLeft": "2px" }} onChange={(e => that.props.editRowandColumnsValue(e, 'rows', i))} value={item.label} id="last_name" type="text" class="validate" /></div>
                                                    <div className="col s1" onClick={(e) => that.props.removeRowsColumns('rows', i)}><i className="material-icons " center>remove_circle_outline</i></div>
                                                    <div className="col s1"></div>
                                                </div>
                                            )
                                        })

                                    }



                                </div>
                                <div>
                                    <p><label style={{ "lineHeight": "2", "fontSize": "15px", "marginRight": "20px" }}><b>Columns</b></label></p>
                                    <div className="row">
                                        <div className="col s10"><input onKeyPress={(e) => this.props.RowandColumnsValue(e, 'columns')} style={{ "border": "2px solid", "height": "30px", "paddingLeft": "2px" }} onChange={(e => this.props.RowandColumnsValue(e, 'columns'))} value={columnsValue} id="last_name" type="text" class="validate" /></div>
                                        <div className="col s1" onClick={(e) => this.props.addRowsColumns('columns')}><i className="material-icons " center>add_circle</i></div>
                                        <div className="col s1"></div>
                                    </div>
                                    {
                                        columns.map(function (item, i) {
                                            return (
                                                <div className="row">
                                                    <div className="col s10"><input style={{ "border": "2px solid", "height": "30px", "paddingLeft": "2px" }} onChange={(e => that.props.editRowandColumnsValue(e, 'columns', i))} value={item.label} id="last_name" type="text" class="validate" /></div>
                                                    <div className="col s1" onClick={(e) => that.props.removeRowsColumns('columns', i)}><i className="material-icons " center>remove_circle_outline</i></div>
                                                    <div className="col s1"></div>
                                                </div>
                                            )
                                        })

                                    }
                                </div>
                            </div>
                        }

                        {(selected_questions_type == "commentbox") &&

                            <div>
                                <div style={{ width: "55%", display: "flex" }}>
                                    <label style={{ "lineHeight": "2", "fontSize": "15px", "marginRight": "20px" }}>Default rows of the comment textarea </label>
                                    <input style={{ "width": "60px", "border": "2px solid", "height": "30px", "paddingLeft": "2px" }} id="last_name" type="number" class="validate" />
                                </div>
                            </div>

                        }
                    </div>
                </div>
                <div className="modal-footer">
                    <button href="#!" className="btn-flat" onClick={(e) => this.props.submit()} >Submit</button>
                    <button href="#!" className="btn-flat" onClick={(e) => this.props.closequestionoptionmodel()} >Cancel</button>
                </div>
            </div>
        </div>
 
        )

    }
}


export default SurveyAddQuestionModel