
import React, { Component } from 'react';
import { connect } from 'react-redux';
import M from 'materialize-css/dist/js/materialize.min.js';
import moment from 'moment';
import QuestionsTypes from './QuestionsTypes';
import SurveyAddQuestionModel from "./SurveyAddQuestionModel";

class SurveyDesign extends Component {

    constructor() {
        super()
        this.state = {
            titlemodel: false,
            title: "",
            is_edit: false,
            questions_index: 0,
            question_date: {
                question: "",
                rows: [],
                columns: []
            },
            rowValue: "",
            columnsValue: ""
        }
    }

    componentDidMount() {
    }

    componentWillReceiveProps(props) {
        var { selected_pages, selected_questions } = props;
        var pages = props.survey.pages;
    }

    changeTitlePage(e) {
        this.setState({ title: e.target.value });
    }

    editQuestions = (questions_data, key, i) => {
        this.state.question_date = questions_data;
        this.state.is_edit = true;
        this.state.questions_index = i
        this.props.selectedQuestiontype(key,true)
        this.setState(this.state);
    }

    editRowandColumnsValue = (e, type, i) => {
        if (type == "rows") {
            this.state.question_date.rows[i].label = e.target.value
        } else {
            this.state.question_date.columns[i].label = e.target.value
        }
        this.setState(this.state);
    }

    submit = (type) => {
        if (type == 'editTitle') {
            this.props.addQuestionsOptions(this.state.title, 'editTitle');
            this.state.title = "";
            this.state.titlemodel = false;
        } else {
            var payload = {
                question: this.state.question_date.question,
                rows: this.state.question_date.rows,
                columns: this.state.question_date.columns,
                is_edit: this.state.is_edit,
                questions_index: this.state.questions_index
            }
            this.props.addQuestionsOptions(payload)
            this.state.question_date = {
                question: "",
                rows: [],
                columns: []
            }
            this.state.is_edit = false;
            this.state.questions_index = 0;
        }
        this.setState(this.state)
    }

    addRowsColumns = (type) => {
        if (type == "rows") {
            this.state.question_date.rows.push({ label: this.state.rowValue });
            this.state.rowValue = "";
        } else {
            this.state.question_date.columns.push({ label: this.state.columnsValue });
            this.state.columnsValue = "";
        }
        this.setState(this.state)
    }

    changeQuestionsPage(e) {
        this.state.question_date.question = e.target.value
        this.setState(this.state);
    }

    removeRowsColumns = (type, index) => {
        if (type == "rows") {
            this.state.question_date.rows.splice(index, 1);
        } else {
            this.state.question_date.columns.splice(index, 1);
        }
        this.setState(this.state)
    }

    addRowsColumns = (type) => {
        if (type == "rows") {
            this.state.question_date.rows.push({ label: this.state.rowValue });
            this.state.rowValue = "";
        } else {
            this.state.question_date.columns.push({ label: this.state.columnsValue });
            this.state.columnsValue = "";
        }
        this.setState(this.state)
    }

    RowandColumnsValue = (e, type) => {

        if (type == "rows") {
            if (e.key == "Enter") {
                this.addRowsColumns("rows")
            } else {
                this.setState({ rowValue: e.target.value })
            }

        } else {
            if (e.key == "Enter") {
                this.addRowsColumns("columns")
            } else {
                this.setState({ columnsValue: e.target.value })
            }

        }
    }





    render() {


        var { selected_pages, selected_questions, selected_questions_type, is_questionoptionmodel } = this.props;
        var { layout, survey_title, pages, title, titlemodel } = this.props.survey;
        var { question_date, rowValue, columnsValue } = this.state
        const that = this;


        return (
            <div className="section_design" style={{ "border": "2px solid #e5e4e4" }}>
                <div className="row "  style={{"borderBottom" : "1px solid rgb(229, 228, 228)"}}>

                    <div className="col s2 question_types">
                        <QuestionsTypes
                            selectedQuestiontype={(key) => this.props.selectedQuestiontype(key)} />
                    </div>

                    <div className="col s10" style={{"borderLeft": "1px solid rgb(229, 228, 228)"}}>
                        <div className="row ">
                            <div className="col s10">
                                <div style={{ "marginTop": "10px", "marginBottom": "10px", "textAlign": "left" }}>
                                    {pages.map(function (item, i) {
                                        return (<button
                                            style={{ marginRight: "5px" }}
                                            type="button"
                                            onClick={(e) => that.props.selectQuestion(i)}
                                            className="btn" >{item.page_number}</button>)
                                    })
                                    }
                                    <button style={{ marginRight: "5px" }} type="button" className="btn" onClick={(e) => this.props.addPages(0)} >+</button>
                                </div>
                            </div>
                            <div className="col s2">
                                <div style={{ "marginTop": "10px", "marginBottom": "10px", "textAlign": "left" }}>
                                    {selected_pages !== 0 &&
                                        <button className="btn" onClick={(e) => this.props.deletePages(selected_pages)}><i className="material-icons " center>delete_forever</i></button>
                                    }

                                </div>
                            </div>
                        </div>

                        <div style={{ ...layout.header }} className={"section_settings_line center"}>
                            <div className="row ">
                                <div className="col s10">
                                    <span>{survey_title}</span>
                                </div>
                                <div className="col s2 survey_title_cols">
                                    <input type="radio" class="filled-in" checked={pages[selected_pages].Survey_title_visible ? 'checkbox' : ""} />
                                    <span onClick={(e) => this.props.TitleVisible('survey_title')}> Show on this page</span>
                                </div>
                            </div>
                        </div>

                        <div>
                            {(pages[selected_pages].questions.length != 0 && !this.props.is_questionoptionmodel) &&
                                <div>

                                    <div style={{ ...layout.subheader }} className={"section_settings_line center page_title"} >
                                        <div className="row ">
                                            <div className="col s8">
                                                {(pages[selected_pages].page_title != "") &&
                                                    <span>{pages[selected_pages].page_title}</span>
                                                }
                                                {(pages[selected_pages].page_title == "") &&
                                                    <span>Page &nbsp; {selected_questions + 1} &nbsp; title</span>
                                                }

                                            </div>
                                            <div className="col s2 page_title_cols_btn" >
                                                <button onClick={(e) => this.setState({ titlemodel: true })} className="btn question-edit-button material-icons">Edit</button>
                                            </div>
                                            <div className="col s2 page_title_cols">
                                                <input type="checkbox" class="filled-in" checked={pages[selected_pages].Page_title_visible ? 'checkbox' : ""} />
                                                <span onClick={(e) => this.props.TitleVisible('page_title')}>Show on this page</span>
                                            </div>
                                        </div>
                                    </div>

                                    {pages[selected_pages].questions.map(function (questions_data, questions_index) {
                                        return (
                                            <div style={{ ...layout.questions }} className="question question-list-border">
                                                <div className="row ">
                                                    <div className="col s9"></div>
                                                    <div className="col s1" style={{ "marginLeft": "5%", "height": "25px" }}>
                                                        <button onClick={(e) => that.editQuestions(questions_data, questions_data.question_type, questions_index)} className="btn question-edit-button material-icons">Edit</button>
                                                    </div>
                                                    <div className="col s1" style={{ "height": "25px" }}>
                                                        <button onClick={(e) => that.props.deleteQuestions(questions_index)} className="btn question-edit-button material-icons">Delete</button>
                                                    </div>
                                                </div>

                                                <span><b>{(questions_index + 1)} :</b> &nbsp;&nbsp; {questions_data.question}</span>

                                                {(questions_data.question_type == "rating_scaling") &&
                                                    <div>
                                                        <table>
                                                            <th style={{ "width": "30%" }} className="padding-5"></th>
                                                            {questions_data.columns.map(function (item, i) {
                                                                return (<th style={{ "width": "5%" }} className="padding-5">{item.label}</th>)
                                                            })
                                                            }
                                                            {questions_data.rows.map(function (item, i) {
                                                                return (
                                                                    <tr className={i % 2 == 1 ? '' : 'color-gray'}>
                                                                        <td className="padding-5">
                                                                            <span>{item.label}</span>
                                                                        </td>
                                                                        {questions_data.columns.map(function (items, i) {
                                                                            return (<td style={{ "width": "5%" }} className="padding-5"> <label>
                                                                                <input type="radio" class="filled-in" checked="" />
                                                                                <span></span>
                                                                            </label></td>)
                                                                        })
                                                                        }
                                                                    </tr>
                                                                )
                                                            })
                                                            }
                                                        </table>

                                                    </div>
                                                }

                                                {(questions_data.question_type == "commentbox") &&
                                                    <div>
                                                        <br />
                                                        <textarea type="text"></textarea>
                                                    </div>
                                                }
                                            </div>
                                        )
                                    })

                                    }
                                </div>
                            }

                            <div className='Footer-question-add'>
                                <h6 style={{ textAlign: "center", color: "green" }}><b>Select a question type to add a new question here.</b></h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"question-footer"} style={{marginBottom:"25px"}}>
                    <button style={{"position" : "absolute", "bottom": "5px", "left" : "25px","display": "flex",}} onClick={(e) => this.props.onChangeTabs(1)} type="button" className="btn" >
                    <i className="material-icons arrow"> keyboard_arrow_left</i>&nbsp;Setting Survey
                    </button>
                    <button style={{"position" : "absolute", "bottom": "5px", "right" : "25px","display": "flex",}} onClick={(e) => this.props.addPages(1)} type="button" className="btn" >
                    Preview Survey&nbsp;<i className="material-icons arrow"> keyboard_arrow_right</i>
                    </button>
                </div>

                {is_questionoptionmodel &&
                    <SurveyAddQuestionModel
                        closequestionoptionmodel={(e) => this.props.closequestionoptionmodel()}
                        editRowandColumnsValue={(e, type, i) => this.editRowandColumnsValue(e, type, i)}
                        addRowsColumns={(type) => this.pro.addRowsColumns(type)}
                        changeQuestionsPage={(e) => this.changeQuestionsPage(e)}
                        removeRowsColumns={(type, index) => this.removeRowsColumns(type, index)}
                        addRowsColumns={(type) => this.addRowsColumns(type)}
                        RowandColumnsValue={(e, type) => this.RowandColumnsValue(e, type)}
                        submit={(e) => this.submit()}
                        selected_questions_type={selected_questions_type}
                        question_date={question_date}
                        rowValue={rowValue}
                        columnsValue={columnsValue}
                    />
                }


                {this.state.titlemodel &&
                    <div id="modal3" className="modal open questions_model" style={{ "width": "50%", "height": "300px" }} >
                        <div>
                            <div className="model-header">
                                <h6 style={{ "padding": "10px" }}>Edit page title</h6>
                            </div>
                            <div className="modal-content">
                                <div style={{ width: "70%" }} className="input-field" >
                                    <input onChange={(e) => that.changeTitlePage(e)} value={title} id="last_name" type="text" class="validate" />
                                    <label for="last_name">Enter your title page </label>
                                </div>
                                <input type="checkbox" class="filled-in" checked={pages[selected_pages].Page_title_visible ? 'checkbox' : ""} />
                                <span onClick={(e) => this.props.TitleVisible('page_title')}>Allow Skip</span>
                            </div>
                            <div className="modal-footer">
                                <button href="#!" className="btn-flat" onClick={(e) => this.submit('editTitle')} >Submit</button>
                                <button href="#!" className="btn-flat" onClick={(e) => this.setState({ titlemodel: false })} >Cancel</button>
                            </div>
                        </div>
                    </div>
                }
            </div>

        )
    }

}

export default SurveyDesign