import React, { Component } from 'react';
class ServeyHeaderSections extends Component {

    constructor() {
        super();
        this.state = {
            selected_tabs: 1,
            survey_tabs: [
                {
                    id: 1,
                    survey_tabs_name: "Survey settings",
                    status: 0, // 0 edit, 1 pending, 2 done
                },

                {
                    id: 2,
                    survey_tabs_name: "Design survey",
                    status: 1,// 0 edit, 1 pending, 2 done
                },

                {
                    id: 3,
                    survey_tabs_name: "Perview survey",
                    status: 1,// 0 edit, 1 pending, 2 done
                },

                {
                    id: 4,
                    survey_tabs_name: "Publish survey",
                    status: 1,// 0 edit, 1 pending, 2 done
                },

            ]
        }
    }


    componentWillReceiveProps(props) {
        this.setState({selected_tabs : props.selected_tabs, survey_tabs :props.survey_tabs})
    }

    render() {
        var { survey_tabs, selected_tabs } = this.state;
        const that = this;

        return (
            <div className="row center Serveytabs " style={{ "marginTop": '20px'}}>
                {survey_tabs.map(function (item, i) {
                    return (
                        <div>
                            <div className="col s2 paddingLeft-0">
                                <table onClick={() => that.props.onChangeTabs(item.id)}>
                                    <tr className={(selected_tabs == item.id) ? "selected-tabs " : ""} >
                                        <td className="padding-0">
                                            {item.status==0 &&
                                                <i className="material-icons center to_survey_tr">edit</i>
                                            }
                                            {item.status==1 &&
                                                <div class="numberCircle to_survey_tr">{item.id}</div>
                                            }
                                            {item.status==2 &&
                                                <i className="material-icons center to_survey_tr">check_circle</i>
                                            }

                                            </td>
                                        <td className="padding-0"><b>{item.survey_tabs_name}</b></td>
                                    </tr>
                                </table>
                            </div>
                            {(i != 3) &&
                                <div className="col s1" style={{ "padding-top": "8px" }}>
                                    <hr />
                                </div>
                            }
                        </div>
                    )
                })
                }
            </div>
        )

    }
}


export default ServeyHeaderSections