import React, { Component } from 'react';
import { connect } from 'react-redux';
import './SurveyPage.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import moment from 'moment';
import { saveServeyForm, getQuestionsData, addSurveyResponse } from '../../actions/survey';
import * as ACT from '../../actions';
import { GenerateKey, getUrl, getQueryString } from '../../utils/helperFunctions';

//Components
import ServeyHeaderSections from './SurveyHeaderSections';
import SurveySettings from './SurveySettings';
import SurveyDesign from './SurveyDesign';
import { relative } from 'path';
import { log } from 'util';
class ServeyPage extends Component {

    constructor() {
        super();
        this.state = {
            selected_tabs: 2,
            survey_tabs: [
                {
                    id: 1,
                    survey_tabs_name: "Survey settings",
                    status: 0, // 0 edit, 1 pending, 2 done
                },

                {
                    id: 2,
                    survey_tabs_name: "Design survey",
                    status: 1,// 0 edit, 1 pending, 2 done
                },

                {
                    id: 3,
                    survey_tabs_name: "Perview survey",
                    status: 1,// 0 edit, 1 pending, 2 done
                },

                {
                    id: 4,
                    survey_tabs_name: "Publish survey",
                    status: 1,// 0 edit, 1 pending, 2 done
                },
            ],
            selected_pages_index: 0,
            selected_questions_index: 0,
            selected_questions_type: "",
            is_questionoptionmodel: false,
            color_code: "",
            is_set_color_type: null,
            is_selected: "header",
            inputoptions: "",
            otheroptions: "",
            is_view_question: null,
            getQuestionsData: null,
            is_answare_submit: false,
            // survey: {
            //     _id: "",
            //     survey_title: "",
            //     survey_description: "",
            //     survey_category: 0,
            //     survey_type: "New",
            //     is_owner_suervey: true,
            //     is_user_survery: false,
            //     is_Publish_external: false,
            //     shared_users: [],
            //     pages: [
            //         {
            //             page_number: 1,
            //             page_title: "",
            //             Page_title_visible: true,
            //             Survey_title_visible: true,
            //             questions: []
            //         },
            //     ],
            //     layout: {
            //         header: {
            //             backgroundColor: "#5f9ea0a8",
            //             border: "center",
            //             color: "#000",
            //             fontSize: "14px",
            //             textAlign: "inherit",
            //             fontWeight: "inherit",
            //             fontStyle: "inherit"
            //         },
            //         subheader: {
            //             backgroundColor: "#fff",
            //             border: "center",
            //             color: "#000",
            //             fontSize: "14px",
            //             textAlign: "inherit",
            //             fontWeight: "inherit",
            //             fontStyle: "inherit",
            //             paddingLeft: "10px",
            //         },
            //         questions: {
            //             backgroundColor: "#FFF",
            //             border: "center",
            //             color: "#000",
            //             fontSize: "14px",
            //             textAlign: "inherit",
            //             fontWeight: "inherit",
            //             fontStyle: "inherit",
            //         },
            //         footer: {
            //             border: "center",
            //             color: "#000",
            //             fontSize: "14px",
            //             textAlign: "inherit",
            //             fontWeight: "inherit",
            //             fontStyle: "inherit",
            //             position: "relative"
            //         }
            //     },
            //     datePublished: moment(new Date()),
            //     validTill: moment(new Date().setDate(new Date().getDate() + 7)),
            //     external_link: GenerateKey()
            // }
            survey :{"survey_title":"Survey Summary","survey_description":"dsdasdasdasdasdasdasd","survey_category":0,"survey_type":"New","is_owner_suervey":true,"is_user_survery":false,"is_Publish_external":false,"pages":[{"page_number":1,"page_title":"","Page_title_visible":true,"Survey_title_visible":true,"questions":[{"number":1,"question":"Survey Summary","question_type":"rating_scaling","rows":[{"label":"Survey Summary"},{"label":"as"},{"label":"asdasda"},{"label":"asdasdasd"},{"label":"asdasd"}],"columns":[{"label":"asdasdasdasd"},{"label":"asdadasdasd"},{"label":"asdasd"},{"label":"asdasdasd"},{"label":"asdasdasdasd"}],"required":""}]}],"shared_users":[{"is_verify":false,"is_deleted":false,"is_active":true,"_id":"5c2ecf798314b02a54d0518b","credits":"15","googleId":"111853897380276307313_test","firstname":"Kk","lastname":"Gan","email":"kkgan0930@gmail.com_test","level":"2","__v":0,"createdAt":"2019-03-24T12:09:27.366Z","is_selected":true},{"is_verify":false,"is_deleted":false,"is_active":true,"_id":"5c34dcf56b03382c2c2f9440","googleId":"104050335341147483043","credits":"10","fullname":"Iqbal Novramadani","firstname":"Iqbal","lastname":"Novramadani","email":"iqbal.slf@gmail.com","role_id":6,"level":"2","updatedAt":"2019-02-06T17:32:14.432Z","createdAt":"2019-03-24T12:09:27.366Z","is_selected":true},{"is_verify":true,"is_deleted":false,"is_active":true,"_id":"5c4e77287c80620338c6d4f2","firstname":"Kk","lastname":"Gan","fullname":"Gejun Yan","department":"Sales","manager":"Jane Doe","email":"kkgan0930@gmail.com","password":"2b79771ffb4bc09214b82e","role_id":7,"login_type":"2","googleId":"111853897380276307313","credits":"5","updatedAt":"2019-02-07T02:51:44.726Z","level":"2","createdAt":"2019-03-24T12:09:27.366Z","is_selected":true},{"is_verify":true,"is_deleted":false,"is_active":true,"_id":"5c4efe1bd9f81905bc973693","firstname":"Richard","lastname":"Roe","email":"richardr@ntxte07.com","password":"3f6a705fe510de9a40","verify_code":"996402","login_type":"1","role_id":2,"credits":"0","createdAt":"2019-01-28T13:05:31.763Z","updatedAt":"2019-02-08T01:07:02.582Z","level":"2","__v":0,"mobile":"12345678","is_selected":true}],"layout":{"header":{"backgroundColor":"#5f9ea0a8","border":"center","color":"#000","fontSize":"14px","textAlign":"inherit","fontWeight":"inherit","fontStyle":"inherit"},"subheader":{"backgroundColor":"#fff","border":"center","color":"#000","fontSize":"14px","textAlign":"inherit","fontWeight":"inherit","fontStyle":"inherit","paddingLeft":"10px"},"questions":{"backgroundColor":"#FFF","border":"center","color":"#000","fontSize":"14px","textAlign":"inherit","fontWeight":"inherit","fontStyle":"inherit"},"footer":{"border":"center","color":"#000","fontSize":"14px","textAlign":"inherit","fontWeight":"inherit","fontStyle":"inherit","position":"relative"}},"datePublished":"2019-03-17T18:30:00.000Z","validTill":"2019-03-21T18:30:00.000Z","external_link":"4DiZSnsXvDUhBKsWWL5h"}
        }
    }

    componentDidMount() {
        const that = this;
        if (this.props.history.location.state) {
            var survey_title = this.props.history.location.state.survey_title;
            var survey_category = this.props.history.location.state.survey_category;
            this.state.survey.survey_title = survey_title ? survey_title : "";
            this.state.survey.survey_category = survey_category ? survey_category : "";
            this.setState(this.state);
        }
        if (getQueryString().id) {
            this.props.getQuestionsData(getQueryString()).then(data => {
                if (data) {
                    if (!data.data) return false
                    if ((!data.data.data.is_Publish_external && that.props.user.isLoggedIn) || data.data.data.is_Publish_external) {
                        var surveyData = data.data.data;
                        if (getQueryString().view) {
                            this.state.survey = surveyData
                            that.setState(that.state);
                        } else {
                            this.state.survey = surveyData
                            this.state.is_view_question = true;
                            that.setState(that.state);
                        }

                    } else {
                        this.setState({ is_view_question: false });
                    }
                }
            });
        } else {
            this.setState({ is_view_question: false });
        }
    }

    onChangeTabsToDesign = (data) => {
        this.state.survey_tabs[0].status = 2;
        this.state.selected_tabs = 2;
        this.state.survey = data;
        this.setState(this.state)
    }

    onChangeTabsStatus = (selected_tabs) => {

        if (selected_tabs !== 1) {
            if (this.state.survey_tabs[0].status == 2) {
                if (selected_tabs == 3) {
                    this.addQuestion(3);
                } else {
                    this.state.selected_tabs = selected_tabs
                    this.setState(this.state);
                }
            } else {
                window.M.toast({ html: "Please do complate Setting." })
            }
        } else {
            this.state.selected_tabs = selected_tabs
            this.setState(this.state);
        }
    }

    onChangeTabs = (selected_tabs) => {
        this.state.selected_tabs = selected_tabs
        this.setState(this.state);
    }

    addPages = (actionTypes) => {
        var letgth = this.state.survey.pages.length, is_check = false, message = "Please seletecd questions type.";

        this.state.survey.pages.forEach((element, i) => {

            if (element.questions.length == 0) {
                is_check = true;
            } else {
                element.questions.forEach(elements => {
                    if (elements.question == "") {
                        is_check = true;
                    } else {
                        message = "Please seletecd questions type."
                        is_check = (elements.question_type == 0) ? true : false;
                        if (elements.question_type == 3 || elements.question_type == 4 || elements.question_type == 6) {
                            is_check = (elements.question_options == "") ? true : false;
                            message = "Please add atleast one questions options."
                        }
                    }
                });
            }
        });


        if (is_check) {
            window.M.toast({ html: message })
        } else {

            if (actionTypes == 0) {

                this.state.survey.pages.push({
                    page_number: letgth + 1,
                    page_title: "",
                    Page_title_visible: true,
                    Survey_title_visible: true,
                    questions: []
                });

                this.state.selected_pages_index = this.state.selected_pages_index + 1;
                this.setState(this.state);

            } else {
                this.state.selected_tabs = 3;
                this.state.selected_pages_index = 0
                this.state.survey_tabs[1].status = 2;
                this.setState(this.state);
            }
        }
    }

    addQuestionsOptions = (data, type) => {
        var page_index = this.state.selected_pages_index, questions_index = this.state.selected_questions_index;

        if (type == "editTitle") {
            this.state.survey.pages[page_index].page_title = data;
        } else {
            this.state.is_questionoptionmodel = false;
            if (this.state.selected_questions_type == "rating_scaling") {
                if (data.is_edit) {
                    this.state.survey.pages[page_index].questions[data.questions_index].question = data.question
                    this.state.survey.pages[page_index].questions[data.questions_index].rows = data.rows
                    this.state.survey.pages[page_index].questions[data.questions_index].columns = data.columns
                } else {
                    this.state.survey.pages[page_index].questions.push({
                        number: this.state.survey.pages[page_index].questions.length == 0 ? 1 : this.state.survey.pages[page_index].questions.length + 1,
                        question: data.question,
                        question_type: "rating_scaling",
                        rows: data.rows,
                        columns: data.columns,
                        required: ""
                    });
                }

            } else if (this.state.selected_questions_type == "commentbox") {
                if (data.is_edit) {
                    this.state.survey.pages[page_index].questions[data.questions_index].question = data.question
                } else {
                    this.state.survey.pages[page_index].questions.push({
                        number: this.state.survey.pages[page_index].questions.length == 0 ? 1 : this.state.survey.pages[page_index].questions.length + 1,
                        question: data.question,
                        question_type: "commentbox",
                        required: ""
                    });
                }

            }
        }

        this.setState(this.state)
    }

    selectedQuestiontype = (type, is_edit) => {
        const { selected_pages_index } = this.state;
        if (type == "rating_scaling") {
            if (is_edit) {
                this.state.is_questionoptionmodel = true;
            } else {
                this.state.survey.pages[selected_pages_index].questions.push({
                    number: this.state.survey.pages[selected_pages_index].questions.length == 0 ? 1 : this.state.survey.pages[selected_pages_index].questions.length + 1,
                    question: "Please rate the following",
                    question_type: "rating_scaling",
                    rows: [
                        { "label": "Product" },
                        { "label": "Services" },
                        { "label": "Support" },
                    ],
                    columns: [
                        { "label": "Very Poor" },
                        { "label": "Poor" },
                        { "label": "neutral" },
                        { "label": "Good" },
                        { "label": "Excellent" }
                    ],
                    required: true
                });
            }

        } else {
            this.state.is_questionoptionmodel = true;
        }
        this.state.selected_questions_type = type;
        this.setState(this.state);
    }

    closequestionoptionmodel = () => {
        this.setState({ is_questionoptionmodel: false })
    }

    questionswrite = (e, index) => {
        this.state.questionList[index].question = e.target.value;
        this.setState(this.state);
    }

    moveTonext = (type) => {
        if (type == "pages") {

            var index = this.state.selected_pages_index
            this.setState({ selected_pages_index: (index + 1), selected_questions_index: 0 })

        } else {

            var index = this.state.selected_questions_index
            this.setState({ selected_questions_index: (index + 1) })

        }
    }

    moveToPer = () => {
        var index = this.state.selected_questions_index
        this.setState({ selected_questions_index: (index - 1) })
    }

    deletePages = (e) => {
        var page_index = this.state.selected_pages_index, questions_index = this.state.selected_questions_index;
        this.state.survey.pages.splice(e, 1);
        this.state.survey.pages.forEach((element, i) => {
            element.page_number = i + 1;
        });
        this.state.selected_pages_index = this.state.selected_pages_index - 1;
        this.setState(this.state)
    }

    deleteQuestions = (e) => {
        var selected_pages_index = this.state.selected_pages_index, questions_index = this.state.selected_questions_index;
        this.state.survey.pages[selected_pages_index].questions.splice(e, 1);
        this.state.survey.pages[selected_pages_index].questions.forEach((element, i) => {
            element.page_number = i + 1;
        });
        this.setState(this.state)
    }

    TitleVisible = (type) => {
        var selected_pages_index = this.state.selected_pages_index;

        if (type == "page_title") {
            this.state.survey.pages[selected_pages_index].Page_title_visible = !this.state.survey.pages[selected_pages_index].Page_title_visible
        } else if (type == "survey_title") {
            this.state.survey.pages[selected_pages_index].Survey_title_visible = !this.state.survey.pages[selected_pages_index].Survey_title_visible
        }

        this.setState(this.state);


    }

    publish = () => {
        this.props.saveServeyForm(this.state.survey).then(data => {
            this.state.selected_tabs = 4;
            window.M.toast({ html: "You question page publish successfully." })
            this.props.history.push("/apps/survey")
        });
    }

    SeletedAwsQuestion = (data, row_index, type, col_index) => {
        var index = this.state.selected_questions_index, page_index = this.state.selected_pages_index;
        var that = this;
        if (type == "rating_scaling") {
            that.state.survey.pages[page_index].questions[index].rows[row_index].selected_index = col_index;
        } else if (type == "commentbox") {
            this.state.survey.pages[page_index].questions[index].answare = data.target.value
        }
        this.setState({ getQuestionsData: this.state.getQuestionsData })
    }

    awsSubmit = () => {
        const { survey } = this.state
        var payload = {
            survey_id: survey._id,
            user_id: this.props.user.isLoggedIn ? this.props.user.User_data._id : "",
            email_address: "",
            phone_number: "",
            pages: []
        }

        survey.pages.forEach(pageElement => {
            var page = {
                page_number: "",
                questions: []
            }

            page.page_number = pageElement.page_number
            pageElement.questions.forEach(questionElement => {
                var answare;

                if (questionElement.question_type == "rating_scaling") {
                    answare = questionElement.rows
                } else if (questionElement.question_type == "commentbox") {
                    answare = questionElement.answare
                }

                page.questions.push({
                    number: questionElement.number,
                    question_type: questionElement.question_type,
                    answare: answare
                })
            });

            payload.pages.push(page);

        });

        this.props.addSurveyResponse(payload).then((resData) => {
            this.setState({ is_answare_submit: true })
        })

    }

    totalQuestion = () => {
        var totalQuestion = 0
        this.state.survey.pages.forEach(pageElement => {
            pageElement.questions.forEach(questionElement => {
                totalQuestion = totalQuestion + 1
            });
        });
        return totalQuestion
    }

    render() {
        const {
            survey_tabs,
            selected_tabs,
            selected_pages_index,
            selected_questions_index,
            selected_questions_type,
            is_view_question,
            is_answare_submit,
            survey
        } = this.state;
        var shareUser = "";
        if (survey.shared_users.length != 0) {
            survey.shared_users.map((i) => shareUser = shareUser + i.firstname + ", ")
        }

        const that = this, index = selected_questions_index;
        if (!is_view_question && this.props.user.isLoggedIn) {
            return (
                <div>

                    <div style={{ width: "90%", "margin": "0 auto" }}>
                        <ServeyHeaderSections
                            selected_tabs={selected_tabs}
                            survey_tabs={survey_tabs}
                            onChangeTabs={(e) => this.onChangeTabs(e)} />
                    </div>

                    {selected_tabs == 1 &&
                        <div>
                            <div className="line"></div>
                            <div style={{ width: "90%", "margin": "0 auto" }}>

                                <SurveySettings
                                    SettingData={survey}
                                    Username={this.props.user.User_data.firstname + " " + this.props.user.User_data.firstname}
                                    onChangeTabs={(data) => this.onChangeTabsToDesign(data)}
                                />
                            </div>
                        </div>
                    }

                    {selected_tabs == 2 &&
                        <SurveyDesign
                            selectedQuestiontype={(key, is_edit) => this.selectedQuestiontype(key, is_edit)}
                            selectQuestion={(number) => this.setState({ selected_pages_index: (number) })}
                            addPages={(e) => this.addPages(e)}
                            addQuestionsOptions={(data, type) => this.addQuestionsOptions(data, type)}
                            deletePages={(e, index) => this.deletePages(e)}
                            deleteQuestions={(e, index) => this.deleteQuestions(e)}
                            closequestionoptionmodel={(e) => this.closequestionoptionmodel()}
                            TitleVisible={(e) => this.TitleVisible(e)}
                            onChangeTabs={(e) => this.onChangeTabsStatus(e)}
                            selected_questions_type={selected_questions_type}
                            is_questionoptionmodel={this.state.is_questionoptionmodel}
                            selected_pages={selected_pages_index}
                            selected_questions={selected_questions_index}
                            survey={survey} />
                    }

                    {selected_tabs == 3 &&
                        <div>

                            <div className="line"></div>
                            <div style={{ "width": "99%", "margin": "0 auto" }}>
                                {survey.pages[selected_pages_index].Survey_title_visible &&
                                    < div style={{ ...survey.layout.header }} className={"section_settings_line center"}>
                                        <span>{survey.survey_title}</span>
                                    </div>
                                }
                                <div>
                                    <div style={{ ...survey.layout.subheader }} className={"section_settings_line center"} >
                                        <div className="row ">
                                            <div className="col s12">
                                                {(survey.pages[selected_pages_index].page_title != "") &&
                                                    <span>{survey.pages[selected_pages_index].page_title}</span>
                                                }
                                                {(survey.pages[selected_pages_index].page_title == "") &&
                                                    <span>Page &nbsp; {selected_pages_index + 1} &nbsp; title</span>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ "height": "350px", "overflow": "auto", "borderBottom": "2px solid grey" }}>
                                        {survey.pages[selected_pages_index].questions.map(function (questions_data, questions_index) {
                                            return (
                                                <div style={{ ...survey.layout.questions }} className="question">
                                                    <span><b>{(questions_index + 1)} :</b> &nbsp;&nbsp; {questions_data.question}</span>

                                                    {(questions_data.question_type == "rating_scaling") &&
                                                        <div>
                                                            <table>
                                                                <th style={{ "width": "30%" }} className="padding-5"></th>
                                                                {questions_data.columns.map(function (item, i) {
                                                                    return (<th style={{ "width": "5%" }} className="padding-5">{item.label}</th>)
                                                                })
                                                                }
                                                                {questions_data.rows.map(function (item, i) {
                                                                    return (
                                                                        <tr className={i % 2 == 1 ? '' : 'color-gray'}>
                                                                            <td className="padding-5">
                                                                                <span>{item.label}</span>
                                                                            </td>
                                                                            {questions_data.columns.map(function (items, i) {
                                                                                return (<td style={{ "width": "5%" }} className="padding-5"> <label>
                                                                                    <input type="radio" class="filled-in" checked="" />
                                                                                    <span></span>
                                                                                </label></td>)
                                                                            })
                                                                            }
                                                                        </tr>
                                                                    )
                                                                })
                                                                }
                                                            </table>

                                                        </div>
                                                    }

                                                    {(questions_data.question_type == "commentbox") &&
                                                        <div>
                                                            <br />
                                                            <textarea type="text"></textarea>
                                                        </div>
                                                    }
                                                </div>
                                            )
                                        })
                                        }
                                    </div>
                                </div>
                                <div className="row " style={{ "marginTop": "15px" }}>
                                    <div className="col s5">
                                        <div className="row ">
                                            <div className="col" style={{ "paddingLeft": "28px" }}>
                                                <button style={{ "width": "100px" }}
                                                    disabled={(selected_pages_index == 0) ? true : false}
                                                    onClick={(e) => this.setState({ selected_pages_index: (selected_pages_index - 1) })} type="button" className="btn" >Previous</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s6" style={{ "paddingLeft": "0px" }}>
                                        Pages {selected_pages_index + 1} of {survey.pages.length}
                                    </div>
                                    <div className="col s1" style={{ "paddingLeft": "0px" }}>
                                        <div className="row ">

                                            <div className="col" style={{ "paddingLeft": "0px" }}>
                                                {(selected_pages_index + 1) == survey.pages.length &&
                                                    <button style={{ "width": "100px" }}
                                                        onClick={(e) => this.onChangeTabs(4)} type="button" className="btn" >Complate</button>
                                                }
                                                {(selected_pages_index + 1) != survey.pages.length &&

                                                    <button style={{ "width": "100px" }}
                                                        disabled={((selected_pages_index + 1) == survey.pages.length) ? true : false}
                                                        onClick={(e) => this.setState({ selected_pages_index: (selected_pages_index + 1) })} type="button" className="btn" >Continew</button>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={"question-footer"} style={{ marginBottom: "25px" }}>
                                    <button style={{ "position": "absolute", "bottom": "5px", "left": "25px", "display": "flex", }} onClick={(e) => this.onChangeTabs(2)} type="button" className="btn" >
                                        <i className="material-icons arrow"> keyboard_arrow_left</i>&nbsp;Setting Survey
                                </button>
                                    <button style={{ "position": "absolute", "bottom": "5px", "right": "25px", "display": "flex", }} onClick={(e) => this.onChangeTabs(4)} type="button" className="btn" >
                                        Preview Survey&nbsp;<i className="material-icons arrow"> keyboard_arrow_right</i>
                                    </button>
                                </div>
                            </div>

                        </div>
                    }

                    {selected_tabs == 4 &&
                        <div>

                            <div className="line"></div>
                            <div className="preview-survey">
                                <div className="row">
                                    <div className="col s6">
                                        <label className="validate">Survey Title</label><br />
                                        <input
                                            style={{ "width": "70%" }}
                                            type="text"
                                            value={survey.survey_title}
                                            maxLength="256" />

                                    </div>
                                    <div className="col s6">
                                        <label className="validate">Created By<span className="redText">*</span></label><br />

                                        <input
                                            style={{ "width": "70%" }}
                                            type="text"
                                            value={this.props.user.User_data.firstname + " " + this.props.user.User_data.firstname}
                                            maxLength="256" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s6">
                                        <label className="validate">Date Created</label><br />
                                        <input
                                            style={{ "width": "70%" }}
                                            type="text"
                                            value={moment(new Date(survey.datePublished)).format("ll")}
                                            maxLength="256" />

                                    </div>
                                    <div className="col s6">
                                        <label className="validate">Date Publish<span className="redText">*</span></label><br />

                                        <input
                                            style={{ "width": "70%" }}
                                            type="text"
                                            value={moment(new Date(survey.validTill)).format("ll")}
                                            maxLength="256" />

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s12">
                                        <label className="validate">Share to users</label><br />
                                        <input
                                            style={{ "width": "100%" }}
                                            type="text"
                                            value={shareUser}
                                            maxLength="256" />

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s6">
                                        <label className="validate">Number of page(s)</label><br />
                                        <input
                                            style={{ "width": "70%" }}
                                            type="text"
                                            value={survey.pages.length}
                                            maxLength="256" />

                                    </div>
                                    <div className="col s6">
                                        <label className="validate">Number of question(s)</label><br />
                                        <input
                                            style={{ "width": "70%" }}
                                            type="text"
                                            value={this.totalQuestion()}
                                            maxLength="256" />

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s12">
                                        <label className="validate">Survey Url</label><br />
                                        <input
                                            style={{ "width": "100%" }}
                                            type="text"
                                            value={getUrl() + "?id=" + survey.external_link}
                                            maxLength="256" />

                                    </div>
                                </div>
                                <div className="preview-survey-footer" style={{ "marginBottom": "25px" }}>
                                    <div className="preview-survey-footer-button">
                                        <button style={{ "marginRight": "10px" }}
                                            type="button" className="btn" >
                                            Save
                               </button>
                                        <button style={{ "marginRight": "10px" }}
                                            type="button" className="btn" >
                                            Cancel
                                 </button>
                                        <button style={{ "marginRight": "10px" }}
                                            onClick={(e) => this.publish()} type="button" className="btn" >
                                            Publish
                                    </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }



                </div>
            )

        } else {
            if (is_answare_submit) {
                return (<h1 style={{ "textAlign": "center" }}>Thank you.</h1>)
            } else if (is_answare_submit == false && is_view_question) {
                return (
                    <div>

                        <div className="line"></div>
                        <div style={{ "width": "99%", "margin": "0 auto" }}>
                            {survey.pages[selected_pages_index].Survey_title_visible &&
                                < div style={{ ...survey.layout.header }} className={"section_settings_line center"}>
                                    <span>{survey.survey_title}</span>
                                </div>
                            }
                            <div>
                                <div style={{ ...survey.layout.subheader }} className={"section_settings_line center"} >
                                    <div className="row ">
                                        <div className="col s12">
                                            {(survey.pages[selected_pages_index].page_title != "") &&
                                                <span>{survey.pages[selected_pages_index].page_title}</span>
                                            }
                                            {(survey.pages[selected_pages_index].page_title == "") &&
                                                <span>Page &nbsp; {selected_pages_index + 1} &nbsp; title</span>
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div style={{ "height": "350px", "overflow": "auto", "borderBottom": "2px solid grey" }}>
                                    {survey.pages[selected_pages_index].questions.map(function (questions_data, questions_index) {
                                        return (
                                            <div style={{ ...survey.layout.questions }} className="question">
                                                <span><b>{(questions_index + 1)} :</b> &nbsp;&nbsp; {questions_data.question}</span>

                                                {(questions_data.question_type == "rating_scaling") &&
                                                    <div>
                                                        <table>
                                                            <th style={{ "width": "30%" }} className="padding-5"></th>
                                                            {questions_data.columns.map(function (item, i) {
                                                                return (<th style={{ "width": "5%" }} className="padding-5">{item.label}</th>)
                                                            })
                                                            }
                                                            {questions_data.rows.map(function (rowitem, row_index) {
                                                                return (
                                                                    <tr className={row_index % 2 == 1 ? '' : 'color-gray'}>
                                                                        <td className="padding-5">
                                                                            <span>{rowitem.label}</span>
                                                                        </td>
                                                                        {questions_data.columns.map(function (items, col_index) {
                                                                            var is_selected = rowitem.selected_index === col_index ? "true" : "false"
                                                                            return (<td style={{ "width": "5%" }} className="padding-5"> <label>
                                                                               <input type="radio" id={col_index} name={col_index} class="filled-in" onClick={(e) => that.SeletedAwsQuestion(items, row_index, 'rating_scaling', col_index)} checked={(is_selected == "true") ? "checked" : ""} />
                                                                                <span></span>
                                                                            </label></td>)
                                                                        })
                                                                        }
                                                                    </tr>
                                                                )
                                                            })
                                                            }
                                                        </table>

                                                    </div>
                                                }

                                                {(questions_data.question_type == "commentbox") &&
                                                    <div>
                                                        <br />
                                                        <textarea type="text" value={survey.pages[selected_pages_index].questions[selected_questions_index].answare} onChange={(e) => that.SeletedAwsQuestion(e, 0, 'commentbox')}></textarea>
                                                    </div>
                                                }
                                            </div>
                                        )
                                    })
                                    }
                                </div>
                            </div>
                            <div className="row " style={{ "marginTop": "15px" }}>
                                <div className="col s5">
                                    <div className="row ">
                                        <div className="col" style={{ "paddingLeft": "28px" }}>
                                            <button style={{ "width": "100px" }}
                                                disabled={(selected_pages_index == 0) ? true : false}
                                                onClick={(e) => this.setState({ selected_pages_index: (selected_pages_index - 1) })} type="button" className="btn" >Previous</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col s6" style={{ "paddingLeft": "0px" }}>
                                    Pages {selected_pages_index + 1} of {survey.pages.length}
                                </div>
                                <div className="col s1" style={{ "paddingLeft": "0px" }}>
                                    <div className="row ">

                                        <div className="col" style={{ "paddingLeft": "0px" }}>
                                            {(selected_pages_index + 1) == survey.pages.length &&
                                                <button style={{ "width": "100px" }}
                                                    onClick={(e) => this.onChangeTabs(4)} type="button" className="btn" >Complate</button>
                                            }
                                            {(selected_pages_index + 1) != survey.pages.length &&

                                                <button style={{ "width": "100px" }}
                                                    disabled={((selected_pages_index + 1) == survey.pages.length) ? true : false}
                                                    onClick={(e) => this.setState({ selected_pages_index: (selected_pages_index + 1) })} type="button" className="btn" >Continew</button>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={"question-footer"} style={{ marginBottom: "25px" }}>
                                <button style={{ "position": "absolute", "bottom": "5px", "left": "25px", "display": "flex", }} onClick={(e) => this.onChangeTabs(2)} type="button" className="btn" >
                                    <i className="material-icons arrow"> keyboard_arrow_left</i>&nbsp;Setting Survey
                        </button>
                                <button style={{ "position": "absolute", "bottom": "5px", "right": "25px", "display": "flex", }} onClick={(e) => this.onChangeTabs(4)} type="button" className="btn" >
                                    Preview Survey&nbsp;<i className="material-icons arrow"> keyboard_arrow_right</i>
                                </button>
                            </div>
                        </div>

                    </div>
                )
            } else {
                return (<h1 style={{ "textAlign": "center" }}> Not Page availble</h1>)
            }
        }
    }
}

const mapDispatchToProps = (dispatch) => ({
    saveServeyForm: (data) => dispatch(saveServeyForm(data)),
    getQuestionsData: (data) => dispatch(getQuestionsData(data)),
    addSurveyResponse: (data) => dispatch(addSurveyResponse(data))
})

const mapStateToProps = ({ user }) => {
    return { user }
}

export default connect(mapStateToProps, mapDispatchToProps)(ServeyPage);
