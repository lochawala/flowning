import React, { Component } from 'react';
import moment from 'moment';
import M from 'materialize-css/dist/js/materialize.min.js';
import SurveyShareUsersList from './SurveyShareUsersList';
import { getUrl } from '../../../utils/helperFunctions';
class SurveySettings extends Component {

    constructor() {
        super();
        this.state = {
            is_one_section_open: true,
            is_two_section_open: true,
            is_three_section_open: true,
            //edit settings
            is_seleted_header: false,
            is_seleted_header_titile: false,
            is_seleted_subtilte: false,
            color_code: "",
            is_set_color_type: null,
            is_selected: "header",
            survey: {
                survey_title: "",
                survey_description: "",
                survey_category: 0,
                survey_type: "New",
                is_owner_suervey: true,
                is_user_survery: false,
                is_Publish_external: false,
                shared_users: [],
                layout: {
                    header: {
                        backgroundColor: "#5f9ea0a8",
                        border: "center",
                        color: "#000",
                        fontSize: "14px",
                        textAlign: "inherit",
                        fontWeight: "inherit",
                        fontStyle: "inherit"
                    },
                    subheader: {
                        backgroundColor: "#fff",
                        border: "center",
                        color: "#000",
                        fontSize: "14px",
                        textAlign: "inherit",
                        fontWeight: "inherit",
                        fontStyle: "inherit",
                        paddingLeft: "10px",
                    },
                    questions: {
                        backgroundColor: "#FFF",
                        border: "center",
                        color: "#000",
                        fontSize: "14px",
                        textAlign: "inherit",
                        fontWeight: "inherit",
                        fontStyle: "inherit",
                    },
                    footer: {
                        border: "center",
                        color: "#000",
                        fontSize: "14px",
                        textAlign: "inherit",
                        fontWeight: "inherit",
                        fontStyle: "inherit",
                        position: "relative"
                    }
                },
                pages: [
                    {
                        page_number: "1",
                        page_title: "Page 1 Title",
                        Page_title_visible: true,
                        Survey_title_visible: true,
                        questions: [
                            {
                                number: "1",
                                question: "Please tell us how do you find the session in general",
                                question_type: "NPS",
                                required: ""
                            }
                        ]
                    },
                ],
                datePublished: "",
                validTill: "",
                external_link: ""
            }
        }
    }


    componentDidMount() {
        var context = this, new_date = new Date();
        var elems = document.querySelectorAll('.published_date');

        M.Datepicker.init(elems, {
            defaultDate: new_date,
            container: 'body',
            onSelect: function (date) {
                context.state.survey.datePublished = moment(new Date(date));
                context.setState(context.state);
            },
            autoClose: true
        });
        var elems = document.querySelectorAll('.due_date');
        M.Datepicker.init(elems, {
            defaultDate: new_date,
            container: 'body',
            onSelect: function (date) {
                context.state.survey.validTill = moment(new Date(date));
                context.setState(context.state);
            },
            autoClose: true
        });


        this.survey_title.focus();

    }

    componentWillReceiveProps(props) {
        const {
            survey_title,
            survey_description,
            survey_category,
            survey_type,
            is_owner_suervey,
            is_user_survery,
            is_Publish_external,
            pages,
            shared_users,
            layout,
            datePublished,
            validTill,
            external_link,
        } = props.SettingData;

        var survey = {
            survey_title: survey_title,
            survey_description: survey_description,
            survey_category: survey_category,
            survey_type: survey_type,
            is_owner_suervey: is_owner_suervey,
            is_user_survery: is_user_survery,
            is_Publish_external: is_Publish_external,
            pages: pages,
            shared_users: shared_users,
            layout: layout,
            datePublished: datePublished,
            validTill: validTill,
            external_link: external_link
        }
        this.setState({ survey: survey });
    }

    SelectedDesign = (e) => {
        this.state.is_selected = e;
        this.setState(this.state);
    }

    onChangesetting = (e) => {
        if (e == "is_owner_suervey" || e == "is_user_survery") {
            this.state.survey["is_owner_suervey"] = !this.state.survey["is_owner_suervey"];
            this.state.survey["is_user_survery"] = !this.state.survey["is_user_survery"];
        } else {
            this.state.survey[e] = !this.state.survey[e];
        }
        this.setState(this.state);
    }

    onChangecolor = (e) => {
        if (e.target) {
            var is_selected_header = this.state.is_selected;
            var is_set_color_type = this.state.is_set_color_type;
            this.state.color_code = e.target.value;
            if (is_set_color_type == "borderColor") {
                this.state.survey.layout[is_selected_header]["border"] = "2px solid" + e.target.value;
            } else {
                this.state.survey.layout[is_selected_header][is_set_color_type] = e.target.value;
            }
            this.setState(this.state);
        } else {
            this.state.is_set_color_type = e;
            this.setState(this.state);
        }
    }

    reset() {
        this.state.survey.layout = {
            header: {
                backgroundColor: "#5f9ea0a8",
                border: "center",
                color: "#000",
                fontSize: "14px",
                textAlign: "inherit",
                fontWeight: "inherit",
                fontStyle: "inherit"
            },
            subheader: {
                backgroundColor: "#fff",
                border: "center",
                color: "#000",
                fontSize: "14px",
                textAlign: "inherit",
                fontWeight: "inherit",
                fontStyle: "inherit",
                paddingLeft: "10px"
            },
            questions: {
                backgroundColor: "#FFF",
                border: "center",
                color: "#000",
                fontSize: "14px",
                textAlign: "inherit",
                fontWeight: "inherit",
                fontStyle: "inherit",
            },
            footer: {
                backgroundColor: "#5f9ea0a8",
                border: "center",
                color: "#000",
                fontSize: "14px",
                textAlign: "inherit",
                fontWeight: "inherit",
                fontStyle: "inherit",
                position: "relative"
            }
        }
        this.setState(this.state);
    }

    submitSetting() {
        const {
            survey_title,
            survey_description,
            survey_type,
            datePublished,
            validTill,
        } = this.state.survey;

        if (!survey_title || !survey_description || !survey_type || !datePublished || !validTill) {
            window.M.toast({ html: "Please do complate Setting." });
        } else {
            this.props.onChangeTabs(this.state.survey);
        }
    }

    editorTools = (e) => {
        var is_selected_header = this.state.is_selected;
        this.state.survey.layout[is_selected_header][e.name] = e.value;
        this.setState(this.state);
    }


    onChangesummery = (e) => {
        var value = e.target.value;
        var name = e.target.name;
        this.state.survey[name] = value;
        this.setState(this.state)
    }

    onOpenSections = (type) => {
        if (this.state.Summary.title == "" || this.state.Summary.description == "" || this.state.Summary.status == 0) {
            window.M.toast({ html: "Please Do complated form for Summary." })
        } else {
            this.state[type] = !this.state[type]
            this.setState(this.state);
        }
    }

    selectuser = (item) => {
        this.state.survey.shared_users = item;
        this.setState(this.state);
    }

    render() {

        var {
            is_one_section_open,
            is_two_section_open,
            is_three_section_open,
            is_selected,
            is_set_color_type,
            color_code
        } = this.state;

        var {
            survey_title,
            survey_description,
            survey_category,
            survey_type,
            is_owner_suervey,
            is_user_survery,
            is_Publish_external,
            layout,
            datePublished,
            validTill,
            external_link,
            shared_users
        } = this.state.survey;

        const that = this;

        var is_selected_header = "",
            is_selected_subtitiles = "",
            is_selected_question = "",
            is_selected_footer = "";

        if (is_selected == "header") {
            is_selected_header = "select_border"
        } else if (is_selected == "subheader") {
            is_selected_subtitiles = "select_border"
        } else if (is_selected == "questions") {
            is_selected_question = "select_border"
        } else if (is_selected == "footer") {
            is_selected_footer = "select_border"
        }
        return (
            <div className="section_settings" >
                {/* <button className="waves-effect waves-light btn " href="#modal1">Modal</button> */}

                {
                    <SurveyShareUsersList seletedusers={(item) => this.selectuser(item)}>

                    </SurveyShareUsersList>}

                <div className="section_settings_line">
                    <h6>Survey Summary</h6>
                    <i className="material-icons arrow"
                        onClick={(e) => this.setState({ is_one_section_open: !is_one_section_open })}>
                        {is_one_section_open ? "expand_less" : "expand_more"}
                    </i>
                </div>

                <div style={{ "height": "300px", "margin-top": "20px" }} className={!is_one_section_open ? "display-none" : ""}>
                    <div className="row">
                        <div className="col s3">
                            <label className="validate">Survey Title <span className="redText">*</span></label>
                            <input className=""
                                ref={(input) => { this.survey_title = input; }}
                                id="survey_title"
                                type="text"
                                name="survey_title"
                                value={survey_title}
                                placeholder="Survey Title"
                                onChange={this.onChangesummery}
                                maxLength="256" />
                        </div>

                        <div className="col s2 input-field" style={{ "marginTop": "40px" }} >
                            <select class="browser-default" name="status" defaultValue={survey_type} onChange={this.onChangesummery}>
                                <option value="New" >New</option>
                                <option value="Published" >Published</option>
                                <option value="Inactive" >Inactive</option>
                                <option value="Archived" >Archived</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <label className="validate">Description <span className="redText">*</span></label>
                            <textarea className=""
                                style={{ "height": "150px" }}
                                id="survey_description"
                                type="text"
                                value={survey_description}
                                onChange={this.onChangesummery}
                                name="survey_description"
                                placeholder="description"
                                maxLength="1000" />
                        </div>
                    </div>
                </div>

                <br />

                <div className="section_settings_line">
                    <h6>Sharing Setting</h6>
                    <i className="material-icons arrow" onClick={(e) => this.setState({ is_two_section_open: !is_two_section_open })}> {is_two_section_open ? "expand_less" : "expand_more"}</i>
                </div>

                <div style={{ "margin-top": "20px", "marginBottom": "10px" }} className={!is_two_section_open ? "display-none" : ""}>
                    <div className="row">
                        <div className="col s7" onClick={(e) => this.onChangesetting('is_owner_suervey')} >
                            <span><label className="validate">Owner of survey</label></span><br />
                            <input style={{ "width": "70%" }} type="text" name="group1" readOnly="true" value={this.props.Username} className="filled-in" checked={is_owner_suervey ? "checked" : ""} />
                        </div>

                        <div className="col s5">
                            <span><label className="validate">Shared To</label></span>
                            <div style={{ "display": "flex" }}>
                                <div style={{ border: "dotted", "height": "100px", "padding": "5px", width: "80%" }}>
                                    {shared_users.map(function (item) {
                                        return (
                                            <span style={{ "marginRight": "5px", color: "grey" }}>{item.firstname}    ,&nbsp;</span>
                                        )
                                    })

                                    }
                                </div>
                                <button className={"modal-trigger search-button"} href="#modal1" style={{ "marginLeft": "5px", "marginRight": "5px", "width": "35px", "height": "30px" }}>
                                    <i className="material-icons center">search</i>
                                </button>
                            </div>

                        </div>
                    </div>

                    <div className="row">
                        <div className="col s2">
                            <input type="checkbox" className="filled-in" checked={is_Publish_external ? "checked" : ""} />
                            <span><label className="validate">Anonymous Access</label></span>
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col s6">
                            <label className="validate">Date Published <span className="redText">*</span></label><br />

                            {datePublished == "" &&
                                <input className="published_date"
                                    style={{ "width": "70%" }}
                                    id="published_date"
                                    type="text"
                                    name="published_date"
                                    value={""}
                                    placeholder="Date Published"
                                    maxLength="256" />
                            }

                            {datePublished != "" &&
                                <input className="datepicker published_date"
                                    style={{ "width": "70%" }}
                                    id="published_date"
                                    type="text"
                                    name="published_date"
                                    value={moment(new Date(datePublished)).format("ll")}
                                    placeholder="Date Published"
                                    maxLength="256" />
                            }

                        </div>
                        <div className="col s6">
                            <label className="validate">Date Due <span className="redText">*</span></label><br />

                            {validTill == "" &&
                                 <input className="due_date"
                                style={{ "width": "70%" }}
                                id="due_date"
                                type="text"
                                name="due_date"
                                value=""
                                placeholder="Datepicker due_date"
                                maxLength="256" />
                            }

                            {validTill != "" &&
                                <input className="datepicker due_date"
                                style={{ "width": "70%" }}
                                id="due_date"
                                type="text"
                                name="due_date"
                                value={moment(new Date(validTill)).format("ll")}
                                placeholder="Datepicker due_date"
                                maxLength="256" />
                            }
                            
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <label className="validate">Published to external</label><br />
                            <input className=""
                                id="title"
                                type="text"
                                readOnly="true"
                                value={getUrl() + "/Survey-form?id=" + external_link}
                                name="title"
                                placeholder="Published to external"
                                maxLength="256" />
                        </div>

                    </div>

                    {/*

                    {/* <div className="row">
                        <div className="col s2" onClick={(e) => this.onChangesetting('is_owner_suervey')} >
                            <input type="radio" name="group1" className="filled-in" checked={is_owner_suervey ? "checked" : ""} />
                            <span><label className="validate">Owner of survey</label></span>
                        </div>
                        <div onClick={(e) => this.onChangesetting("is_user_survery")} className={!is_user_survery ? "modal-trigger col s2" : "col s2"} href="#modal1">
                            <input type="radio" name="group1" className="filled-in" checked={is_user_survery ? "checked" : ""} />
                            <span><label className="validate">Shared To Users</label></span>
                        </div>
                        <div className="col s2" onClick={(e) => this.onChangesetting("is_Publish_external")}>
                            <input type="checkbox" className="filled-in" checked={is_Publish_external ? "checked" : ""} />
                            <span><label className="validate">Publish to external</label></span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s2">
                            <label className="validate">Date Published</label>
                            <input className="datepicker published_date"
                                id="published_date"
                                type="text"
                                name="published_date"
                                value={moment(new Date(datePublished)).format("ll")}
                                placeholder="Date Published"
                                maxLength="256" />
                        </div>
                        <div className="col s2">
                            <label className="validate">Date Due</label>
                            <input className="datepicker due_date"
                                id="due_date"
                                type="text"
                                name="due_date"
                                value={moment(new Date(validTill)).format("ll")}
                                placeholder="Datepicker due_date"
                                maxLength="256" />
                        </div>
                        <div className="col s6">
                            <label className="validate">Published to external</label>
                            <input className=""
                                id="title"
                                type="text"
                                readOnly="true"
                                value={getUrl() + "/Survey-form?id=" + external_link}
                                name="title"
                                placeholder="Published to external"
                                maxLength="256" />
                        </div>
                    </div>
                 */}
                </div>

                <br />

                <div className="section_settings_line">
                    <h6>Survey layout</h6>
                    <i className="material-icons arrow" onClick={(e) => this.onOpenSections('is_three_section_open')}> {is_three_section_open ? "expand_less" : "expand_more"}</i>
                </div>

                <div style={{ "height": "300px", "margin-top": "20px", "marginBottom": "20px"}} className={!is_three_section_open ? "display-none" : ""}>
                    <div className="row">
                        <div className="col s2 editor-tools" style={{ "height": "320px", "padding": "5px" }} >
                            <div className="row">
                                <div className="col s12">
                                    <button onClick={(e) => this.editorTools({ name: "fontWeight", value: 'bold' })}><i className="material-icons">format_bold</i></button>
                                    <button onClick={(e) => this.editorTools({ name: "fontStyle", value: 'italic' })}><i className="material-icons ">format_italic</i></button>
                                    <button onClick={(e) => this.editorTools({ name: "fontStyle", value: 'underlined' })}><i className="material-icons">format_underlined</i></button>
                                    <input type="number" className="inputnumber" onChange={(e) => this.editorTools({ name: "fontSize", value: e.target.value + "px" })} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s12">
                                    <button onClick={(e) => this.editorTools({ name: "textAlign", value: 'left' })}><i className="material-icons">format_align_left</i></button>
                                    <button onClick={(e) => this.editorTools({ name: "textAlign", value: 'center' })}><i className="material-icons ">format_align_center</i></button>
                                    <button onClick={(e) => this.editorTools({ name: "textAlign", value: 'right' })}><i className="material-icons">format_align_right</i></button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s8">
                                    <input type="checkbox"
                                        className="filled-in checkbox"
                                        checked={(is_set_color_type == "color") ? "checked" : ""} />

                                    <span onClick={(e) => this.onChangecolor("color")} ><label className="validate">Font Color</label></span>
                                    <br />

                                    <input type="checkbox"
                                        className="filled-in checkbox"
                                        checked="checked"
                                        checked={(is_set_color_type == "backgroundColor") ? "checked" : ""}
                                    />

                                    <span onClick={(e) => this.onChangecolor("backgroundColor")}><label className="validate">Background Color</label></span>
                                    <br />

                                    <input type="checkbox"
                                        className="filled-in checkbox"
                                        checked="checked"
                                        checked={(is_set_color_type == "borderColor") ? "checked" : ""} />

                                    <span onClick={(e) => this.onChangecolor("borderColor")}><label className="validate">Border Color</label></span>
                                </div>
                                <div className="col s4">
                                    <input type="color" id="head" name="head" value={color_code} onChange={this.onChangecolor} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col 12">
                                    <button onClick={(e) => this.reset()} className="btn" >reset</button>
                                </div>
                            </div>
                        </div>
                        <div className="col s10">
                            <div style={{ ...layout.header }} className={"section_settings_line center " + is_selected_header} onClick={(e) => this.SelectedDesign('header')}>
                                <span>{survey_title}</span>
                            </div>
                            <div style={{ ...layout.subheader }} className={"subtitles " + is_selected_subtitiles} onClick={(e) => this.SelectedDesign('subheader')}>
                                <span>Page Title</span>
                            </div>
                            <div style={{ ...layout.questions }} className={"question " + is_selected_question} onClick={(e) => this.SelectedDesign('questions')} >
                                <span>Simple question formate</span>
                            </div>

                        </div>

                    </div>

                </div>
                <button type="button" onClick={(e) => this.submitSetting()} className="btn" style={{ "display": "flex","marginBottom": "20px", "textAlign": "center", "position": "absolute", "right": "5%", "marginTop": "10px", "color": "#fff" }}>
                Design Survey &nbsp; 
                <i className="material-icons arrow"> keyboard_arrow_right</i>
                </button>
            </div>
        )

    }
}


export default SurveySettings