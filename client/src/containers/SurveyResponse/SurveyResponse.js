import React, { Component } from 'react';
import { connect } from 'react-redux';
import M from 'materialize-css/dist/js/materialize.min.js';
import moment from 'moment';
import { GetSurveyResponse, GetSurveyResponseanswer, getQuestionsData } from '../../actions/survey';
import { GenerateKey, getUrl, getQueryString } from '../../utils/helperFunctions';
import SurveyResponseLists from './SurveyResponseLists'

class SurveyResponse extends Component {

    constructor() {
        super();
        this.state = {
            ResponseList: [],
            answer: null,
            is_view_answer: false,
            survey: "",
            selected_pages_index: 0,
            selected_questions_index: 0
        };
    }

    componentDidMount() {
        if (getQueryString().id) {
            this.props.GetSurveyResponse(getQueryString()).then(data => {
                if (data) {
                    var ResponseList = data.data ? data.data.data : [];
                    this.setState({ ResponseList: ResponseList })
                }
            });
        } else {
            this.props.history.push("/apps/survey")
        }
    }


    moveTonext = (type) => {
        if (type == "pages") {

            var index = this.state.selected_pages_index
            this.setState({ selected_pages_index: (index + 1), selected_questions_index: 0 })

        } else {

            var index = this.state.selected_questions_index
            this.setState({ selected_questions_index: (index + 1) })

        }
    }

    moveToPer = () => {
        var index = this.state.selected_questions_index
        this.setState({ selected_questions_index: (index - 1) })
    }

    viewAnsware = (id) => {
        const that = this;
        this.props.GetSurveyResponseanswer({ id: id }).then(data => {
            if (data) {
                var answer = data.data ? data.data.data[0] : [];
                var is_view_answer = true;
                if (answer.length != 0) {
                    that.props.getQuestionsData({ id: answer.survey_id, key: true }).then(data => {
                        if (data) {
                            var surveyData = data.data ? data.data.data : [];
                            if (surveyData.length != 0) {
                                surveyData.pages.forEach(elementQuestionPage => {
                                    answer.pages.forEach(elemntAnswerPage => {
                                        if (elementQuestionPage.page_number == elemntAnswerPage.page_number) {
                                            elementQuestionPage.questions.forEach(elementQuestion => {
                                                elemntAnswerPage.questions.forEach(elemntAnswerQuestion => {
                                                    if (elementQuestion.number == elemntAnswerQuestion.number) {
                                                        if (elementQuestion.question_type == "rating_scaling") {
                                                            elementQuestion.rows = elemntAnswerQuestion.answare;
                                                        } else if (elementQuestion.question_type == "commentbox") {
                                                            elementQuestion.answare = elemntAnswerQuestion.answare;
                                                        }
                                                    }
                                                });
                                            });
                                        }
                                    });

                                });
                            }
                            debugger
                            this.setState({ answer: answer, is_view_answer: is_view_answer, survey: surveyData })
                        }
                    });
                }
            }
        });
    }

    render() {
        const { ResponseList, answer, is_view_answer, survey, selected_pages_index, selected_questions_index } = this.state;
        const that = this;
        return (
            <div>
                {!is_view_answer &&
                    <SurveyResponseLists viewAnsware={this.viewAnsware} items={ResponseList} />
                }

                {is_view_answer &&

                    <div>
                        <button style={{ "width": "100px", "marginTop" : "10px" , "marginLeft" : "20px" }} 
                        onClick={(e) => this.setState({ 
                            answer: null,
                            is_view_answer: false,
                            selected_pages_index: 0,
                            selected_questions_index: 0,
                            survey: ""})}
                         type="button" className="btn" >Back</button>
                        <div style={{ "width": "99%", "margin": "0 auto" }}>
                            <div style={{ "margin": "0 auto", "padding": "10px", "borderBottom": "1px solid" }}>
                                {survey.pages[selected_pages_index].Survey_title_visible &&
                                    < div style={{ ...survey.layout.header }} className={"section_settings_line center"}>
                                        <span>{survey.survey_title}</span>
                                    </div>
                                }
                                {survey.pages[selected_pages_index].Page_title_visible &&
                                    <div style={{ ...survey.layout.subheader }} className={"subtitles"} >
                                        <div className="row ">
                                            <div className="col s12">
                                                {(survey.pages[selected_pages_index].page_title != "") &&
                                                    <span>{survey.pages[selected_pages_index].page_title}</span>
                                                }
                                                {(survey.pages[selected_pages_index].page_title == "") &&
                                                    <span>Page &nbsp; {selected_pages_index + 1} &nbsp; title</span>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                }


                                <div style={{ ...survey.layout.questions }} className={"question"}>

                                    <p className='color-gray' style={{ "padding": "20px" }}><span><b>{survey.pages[selected_pages_index].questions[selected_questions_index].number}.</b>
                                        &nbsp;&nbsp; {survey.pages[selected_pages_index].questions[selected_questions_index].question}</span></p>
                                    {(survey.pages[selected_pages_index].questions[selected_questions_index].question_type == "rating_scaling") &&
                                        <div>
                                            <table>
                                                <th style={{ "width": "30%" }} ></th>
                                                {survey.pages[selected_pages_index].questions[selected_questions_index].columns.map(function (item, i) {
                                                    return (<th style={{ "width": "5%" }}>{item.label}</th>)
                                                })
                                                }
                                                {survey.pages[selected_pages_index].questions[selected_questions_index].rows.map(function (rowitem, row_index) {
                                                    return (
                                                        <tr className={row_index % 2 == 1 ? '' : 'color-gray'}>
                                                            <td>
                                                                <span>{rowitem.label}</span>
                                                            </td>
                                                            {survey.pages[selected_pages_index].questions[selected_questions_index].columns.map(function (items, col_index) {
                                                                var is_selected = rowitem.selected_index === col_index ? "true" : "false"
                                                                return (<td style={{ "width": "5%" }}>
                                                                    <label>
                                                                        <input type="checkbox" id={col_index} name={col_index} class="filled-in" onClick={(e) => that.SeletedAwsQuestion(items, row_index, 'rating_scaling', col_index)} checked={(is_selected == "true") ? "checked" : ""} />
                                                                        <span></span>
                                                                    </label></td>)
                                                            })
                                                            }
                                                        </tr>
                                                    )
                                                })
                                                }
                                            </table>

                                        </div>
                                    }

                                    {(survey.pages[selected_pages_index].questions[selected_questions_index].question_type == "commentbox") &&
                                        <div>
                                            <br />
                                            <textarea type="text" value={survey.pages[selected_pages_index].questions[selected_questions_index].answare} onChange={(e) => that.SeletedAwsQuestion(e, 0, 'commentbox')}></textarea>
                                        </div>
                                    }
                                </div>

                                <div className="row " style={{ "marginTop": "15px" }}>
                                    <div className="col s5">
                                        <div className="row ">
                                            <div className="col" style={{ "paddingLeft": "10px" }}>
                                                <button style={{ "width": "100px" }} disabled={(selected_questions_index == 0) ? true : false} onClick={(e) => this.moveToPer('questions')} type="button" className="btn" >Previous</button>
                                            </div>
                                            <div className="col" style={{ "paddingLeft": "0px" }}>
                                                <button style={{ "width": "100px" }} disabled={((selected_questions_index + 1) == survey.pages[selected_pages_index].questions.length) ? true : false} onClick={(e) => this.moveTonext('questions')} type="button" className="btn" >Continew</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s4" style={{ "paddingLeft": "0px" }}>
                                        <div className="row ">
                                            <div className="col" style={{ "paddingLeft": "0px" }}>
                                                <h6> <b>{(selected_questions_index + 1)}</b>  of &nbsp;<b>{survey.pages[selected_pages_index].questions.length}</b> &nbsp;&nbsp;question in &nbsp;<b>{selected_pages_index + 1}</b> &nbsp; pages</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col s3" style={{ "paddingLeft": "0px" }}>
                                        <div className="row ">
                                            <div className="col s4" style={{ "paddingLeft": "0px" }}>
                                                <button style={{ "width": "100px" }} disabled={((selected_pages_index + 1) == survey.pages.length) ? true : false} onClick={(e) => this.moveTonext('pages')} type="button" className="btn" >Skip</button>
                                            </div>
                                            {/* <div className="col s4" style={{ "paddingLeft": "0px" }}>
                                                <button style={{ "width": "100px" }} onClick={(e) => this.awsSubmit()} type="button" className="btn" >Complate</button>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div >
                    </div>
                }

            </div>
        )

    }
}

const mapDispatchToProps = (dispatch) => ({
    GetSurveyResponse: (data) => dispatch(GetSurveyResponse(data)),
    GetSurveyResponseanswer: (data) => dispatch(GetSurveyResponseanswer(data)),
    getQuestionsData: (data) => dispatch(getQuestionsData(data)),
})

const mapStateToProps = ({ user }) => {
    return { user }
}

export default connect(mapStateToProps, mapDispatchToProps)(SurveyResponse);
