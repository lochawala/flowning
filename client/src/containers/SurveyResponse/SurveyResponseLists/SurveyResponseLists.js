import React, { Component } from 'react';
import M from 'materialize-css/dist/js/materialize.min.js';
import moment from 'moment';

class SurveyResponseLists extends Component {

    constructor() {
        super();
    }


    render() {
        const ResponseList = this.props.items, that = this;
        return (
            <div id="survey" style={{"width" : "98%", "margin" : "auto"}}>
                <table className="striped mdl-data-table" id="surveylist" style={{ "TextAlign": "center", "marginTop": "15px" }}>
                    <thead style={{ "border": "2px solid " }}>
                        <tr>
                            <th className="title-th">No.</th>
                            <th >CREATED</th>
                            <th >Email Adress</th>
                            <th >Survey Status</th>
                            <th >Ip Address</th>
                            <th >Answare</th>
                        </tr>
                    </thead>

                    {ResponseList.map((item, key) => {
                        return (<tr className={key % 2 == 1 ? 'color-gray' : ''}>
                            <td>{key+1}</td>
                            <td>{moment(item.createdAt).format("DD/MM/YYYY")}</td>
                            <td>{item.email_address ? item.email_address : "----"} </td>
                            <td>{item.survey_status ? item.survey_status : "----"}</td>
                            <td>{item.ip_address}</td>
                            <td><button className="btn"><i className="material-icons " onClick={(e)=> that.props.viewAnsware(item._id)} center>remove_red_eye</i></button></td>
                        </tr>)
                    })}

                </table>
            </div>
        )


    }
}


export default SurveyResponseLists;
