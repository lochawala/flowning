import React, { Component } from 'react';

class TenantApprovedForms extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            _id : "",
            user_id : "",
            company_name: "", 
            company_email: "", 
            address : "",
            country : "",
            hostname : "",
            emailDomain : "",
            db_url : "",
            firstname  : "",
            lastname : "",
            mobile : "",
            user_info : "",
            is_active : "",
            domain: "",
        }
    }

    componentDidMount(){
        const {userdata} = this.props;
        this.setState({
            _id : userdata._id,
            user_id : userdata.user._id,
            company_name: userdata.name, 
            company_email: userdata.email, 
            address : userdata.user.address,
            country :   userdata.is_active ? userdata.company.country : userdata.country,
            hostname : userdata.name,
            emailDomain : userdata.domain,
            db_url :  userdata.is_active ? userdata.company.database_url : userdata.db_connection,
            firstname  : userdata.user.firstname,
            lastname : userdata.user.lastname,
            mobile : userdata.user.mobile,
            user_info : userdata.user,
            is_active : userdata.is_active,
            domain: userdata.domain,
        })
    }

    onSubmit = (e)=>{
        e.preventDefault()
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(this.state.company_email)) {
            window.M.toast({ html: "Please enter vailid email address." })
        }else {
          this.props.addCompany(this.state)
        }
        
    }


    render() { 
       const { 
            company_name,
            company_email,
            domain,
            country,
            firstname,
            lastname,
            mobile,
            address,
            db_url,
            is_active
        } = this.state;
        const isLoginButtonDisabled = !firstname || !lastname || !company_email || !company_name || !domain || !domain || !db_url ;
        
        return (
            <div className="user-page">
                <div id="UserForm">
                    <h4 className="title center">Add New Company</h4>
                    <form onSubmit={this.onSubmit}>
                        <div className="row center">
                            <div className="col s1"></div>
                            <div className="col s10">
                                <div className="row">
                                    <div className="input-field col s6">
                                        <label htmlFor="company_name" className="active">Company Name*</label>
                                        <input 
                                            id="company_name"
                                            type="text"
                                            value={company_name}
                                            name="company_name"
                                            onChange={(e) => this.setState({company_name : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                    <div className="input-field col s6">
                                        <label htmlFor="company_email" className="active">Company email*</label>
                                        <input 
                                            id="company_email"
                                            type="text"
                                            value={company_email}
                                            name="company_email"
                                            onChange={(e) => this.setState({company_email : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s6">
                                        <label htmlFor="domain" className="active">Domain*</label>
                                        <input 
                                            id="domain"
                                            type="text"
                                            value={domain}
                                            name="domain"
                                            maxLength="256" 
                                            onChange={(e) => this.setState({company_email : e.target.value})}
                                            />
                                    </div>
                                    <div className="input-field col s6">
                                        <label htmlFor="country" className="active">country</label>
                                        <input 
                                            id="country"
                                            type="text"
                                            name="country"
                                            value={country}
                                            onChange={(e) => this.setState({country : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s6">
                                        <label htmlFor="firstname" className="active">First Name*</label>
                                        <input 
                                            id="firstname"
                                            type="text"
                                            value={firstname}
                                            name="firstname"
                                            onChange={(e) => this.setState({firstname : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                    <div className="input-field col s6">
                                        <label htmlFor="lastname" className="active">Last Name*</label>
                                        <input 
                                            id="lastname"
                                            type="text"
                                            value={lastname}
                                            name="lastname"
                                            onChange={(e) => this.setState({lastname : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s6">
                                        <label htmlFor="mobile" className="active">Mobile</label>
                                        <input 
                                            id="mobile"
                                            type="text"
                                            value={mobile}
                                            name="mobile"
                                            onChange={(e) => this.setState({mobile : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                    <div className="input-field col s6">
                                        <label htmlFor="address" className="active">Address</label>
                                        <input 
                                            id="address"
                                            type="text"
                                            value={address}
                                            name="address"
                                            onChange={(e) => this.setState({address : e.target.value})}
                                            maxLength="256" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <label htmlFor="db_url" className="active">DB URL*</label>
                                        <input 
                                            id="db_url"
                                            type="text"
                                            value={db_url}
                                            onChange={(e) => this.setState({db_url : e.target.value})}
                                            name="db_url"
                                            maxLength="256" />
                                    </div>
                                </div>

                                {!is_active && 
                                <button type="submit center" disabled={isLoginButtonDisabled} className="waves-effect waves-light btn submit-btn">Submit</button>
                                }
                                
                            </div>
                            <div className="input-field col s1"></div>
                        </div>
                    </form>
                </div >
            </div>

        )
    }
}

export default TenantApprovedForms