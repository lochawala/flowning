import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './TenantRequestPage.css';
import { TenantRequest, GetTenantRequest, AddCompany, GetPassword } from '../../actions/admin';
import { connect } from "react-redux";
import emails from '../../utils/validateEmails';
import TenantApprovedForms from './TenantApprovedForm/TenantApprovedForms';
import { getUrl } from "../../utils/helperFunctions";

class TenantRequestPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            c_email: "",
            user: "",
            country: "",
            db_user : "",
            db_connection : "",
            db_password : "",
            is_system : false,
            tenantLists: [],
            is_approved: false,
            is_user_info: false,
            userdata: null,
            is_password: null
        };
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        let { email, name ,c_email,db_connection,db_user,db_password } = this.state;
        email = this.state.email//+'@'+this.state.name+'.com'
       var filter = /^([\w-\.]+@(?!gmail.com)(?!yahoo.com)(?!hotmail.com)(?!yahoo.co.in)(?!aol.com)(?!abc.com)(?!xyz.com)(?!pqr.com)(?!rediffmail.com)(?!live.com)(?!outlook.com)(?!me.com)(?!msn.com)(?!ymail.com)([\w-]+\.)+[\w-]{2,4})?$/;
        if (!filter.test(email)) {
            window.M.toast({ html: "Please Enter Business Email Address." })
        } else {
            db_connection =  db_connection.replace("<dbuser>",db_user);
            db_connection =  db_connection.replace("<dbpassword>",db_password);

            if(email == c_email){
                const payload = {
                    name:name,
                    email: email,
                    user: this.state.user,
                    is_system : this.state.is_system,
                    db_user :  this.state.db_user,
                    db_connection :  db_connection,
                    db_password :  this.state.db_password,
                    domain: this.state.name + "." + getUrl(),
                    status: "new"
                }
                this.props.TenantRequest(payload).then(response => {
                    window.M.toast({ html: response.message });
                    this.props.GetTenantRequest({id : this.state.user});
                    if (response.status) {
                        this.setState({ is_approved: false, userdata: null });
                    }
                });
            }else{
                window.M.toast({ html: "Business Email Address.doesn't match." }) 
            }   
        }
    }

    componentDidMount() {
        const { User_data } = this.props;
        this.setState({ user: User_data._id });
        if (User_data.level == "8") {
            this.props.GetTenantRequest({id : 0});
        } else {
            this.props.GetTenantRequest({id : User_data._id});
        }
    }

    componentWillReceiveProps(props) {
        this.setState({ tenantLists: props.tenantLists })
    }

    addCompany = (e) => {
        const { User_data } = this.props;
        this.props.AddCompany(e).then(res_data => {
            if(res_data.state==false){
                window.M.toast({ html: res_data.message })
            }else{
                window.M.toast({ html: res_data.message})
                this.props.GetTenantRequest({id : 0});    
            }
            this.setState({ is_approved: false, userdata: null });
        });
    }

    GetPassword = (e) => {
        if (e.is_active == true) {
            this.props.GetPassword({ email: e.email, hostname : e.name }).then(resData => {
                if (resData.status) {
                    this.setState({ is_password: true, userdata: { email: e.email, password: resData.data } })
                } else {
                    window.M.toast({ html: "Please Enter Business Email Address." })
                }
            })

        }

    }

    render() {

        let { email, name,c_email, country,db_user, db_connection ,db_password, is_system , tenantLists } = this.state;
        const { User_data } = this.props;
        const isLoginButtonDisabled = !email || !name;
        const that = this;
        console.log(this.props.tenantLists);


        if (!this.props.tenantLists) {
            return false
        } else {

            if (tenantLists.length == 0 && User_data.level != "8") {
                return (
                    <div id="LoginForm" className="TenantRequestForm">
                        <h6 className="header-title">Request a new tenant</h6>
                        <form id="LoginFormData" onSubmit={this.onSubmit}>
                            <div className="row">
                                <div className="col s6">
                                    <div className="row t-left border-r">
                                        <div className="col s1"></div>
                                        <div className="col s10">
                                            <div className="row">
                                                <div className="col s6">
                                                    <label className="validate">Tenant Name</label>
                                                    <input className=""
                                                        id="name"
                                                        type="text"
                                                        name="name"
                                                        placeholder="Tenant Name"
                                                        value={name}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ name: e.target.value })} />
                                                </div>
                                                <div className="col s6">
                                                    <label className="validate">Domain</label>
                                                    <input className=""
                                                        id="email"
                                                        style={{ "border": "0px solid" }}
                                                        type="text"
                                                        name="email"
                                                        readOnly
                                                        placeholder="Domain"
                                                        value={getUrl()}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ email: e.target.value })} />
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col s12">
                                                    <label htmlFor="country">Country</label>
                                                    <input className=""
                                                        id="country"
                                                        type="text"
                                                        name="country"
                                                        placeholder="country"
                                                        value={country}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ country: e.target.value })} />
                                                </div>
                                            </div>
                                            <div className="row">

                                             <div className="col s12">
                                             <label htmlFor="email">Tenant Email</label>
                                                    <input className=""
                                                        id="email"
                                                        type="text"
                                                        name="email"
                                                        placeholder="Email"
                                                        value={email}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ email: e.target.value.toLowerCase() })} />
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col s12">
                                                    <label htmlFor="c_email">Confirm Tenant Business Email</label>
                                                    <input className=""
                                                        id="c_email"
                                                        type="text"
                                                        name="email"
                                                        placeholder="Email"
                                                        value={c_email}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ c_email: e.target.value.toLowerCase() })} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col s1"></div>
                                    </div>
                                </div>
                                <div className="col s6">
                                    <div className="row t-left">
                                        <div className="col s1"></div>
                                        <div className="col s10">
                                            <div className="row">
                                                <div className="col s12">
                                                    <div class="switch">
                                                        <label>
                                                            System assigned database ? {is_system}
                                                    <input type="checkbox" 
                                                    onChange={(e) => this.setState({
                                                        is_system : !is_system,
                                                        db_connection : !is_system ? "" :db_connection,
                                                        db_user : !is_system ? "" :db_user,
                                                        db_password : !is_system ? "" : db_password,
                                                        })} />
                                                            <span class="lever"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col s12">
                                                    <label htmlFor="email">DB Connection : </label>
                                                    <input className=""
                                                        id="db_connection"
                                                        type="text"
                                                        readOnly ={is_system}
                                                        name="db_connection"
                                                        placeholder="DB Connection"
                                                        value={db_connection}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ db_connection: e.target.value })} />
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col s12">
                                                    <label htmlFor="email">DB User : </label>
                                                    <input className=""
                                                        id="db_user"
                                                        type="text"
                                                        name="db_user"
                                                        readOnly ={is_system}
                                                        placeholder="db_user"
                                                        value={db_user}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ db_user: e.target.value })} />
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col s12">
                                                    <label htmlFor="email">DB Password</label>
                                                    <input className=""
                                                        id="db_password"
                                                        type="password"
                                                        readOnly ={is_system}
                                                        name="db_password"
                                                        placeholder="DB Password"
                                                        value={db_password}
                                                        maxLength="256"
                                                        onChange={e => this.setState({ db_password: e.target.value })} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col s1"></div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" className="waves-effect waves-light btn submit-btn" disabled={isLoginButtonDisabled}>Submit</button>
                        </form>
                    </div >
                )
            } else {
                return (
                    <div id="TenantRequestList" style={{ "margin": "20px 20px 20px 20px" }}>

                        {
                            this.state.is_approved &&
                            <div>
                                <div id="modal-login-form" className="modal open">
                                    <div className="close_btn" onClick={() => this.setState({ is_approved: false, userdata: null })} ><i className="material-icons">close</i></div>
                                    <TenantApprovedForms addCompany={this.addCompany} userdata={this.state.userdata}></TenantApprovedForms>
                                </div>
                                <div className="modal-overlay">
                                </div>
                            </div>

                        }

                        {
                            this.state.is_password &&
                            <div>
                                <div id="modal-login-form" className="modal open">
                                    <div className="close_btn" onClick={() => this.setState({ is_password: false, userdata: null })} ><i className="material-icons">close</i></div>
                                    <div className="row" style={{ "padding": "100px 90px 0px 90px" }}>
                                        <div className="input-field col s6">
                                            <label htmlFor="company_name" className="active">Email Address</label>
                                            <input
                                                id="company_name"
                                                type="text"
                                                value={this.state.userdata.email}
                                                name="company_name"
                                                maxLength="256" />
                                        </div>
                                        <div className="input-field col s6">
                                            <label htmlFor="company_email" className="active">Password</label>
                                            <input
                                                id="company_email"
                                                type="text"
                                                value={this.state.userdata.password}
                                                name="company_email"
                                                maxLength="256" />
                                        </div>
                                    </div>

                                </div>
                                <div className="modal-overlay">
                                </div>
                            </div>

                        }

                        {/* <div className="modal-trigger waves-effect waves-light btn" onClick={() => this.setState({ tenantLists: [] })}>Request new tenant </div> */}
                        <table className="striped mdl-data-table" id="example">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>status</th>
                                    {/* {User_data.level == "8" && <th>User Info</th>} */}
                                    {User_data.level == "8" && <th>Actions</th>}
                                    {/* <th>Get Password</th> */}
                                </tr>
                            </thead>

                            <tbody>

                                {this.props.tenantLists.map(function (item, i) {
                                    return (
                                        <tr key={i}>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>  <label>
                                                <input type="checkbox" className="filled-in" onChange={(e) => console.log(e)} checked={item.is_active ? "checked" : ""} />
                                                <span>Filled in</span>
                                            </label>
                                            </td>
                                            {/* {User_data.level == "8" && <td> <div className="modal-trigger waves-effect waves-light btn" onClick={() => that.setState({ is_user_info: true, userdata: item })}>View</div></td>} */}
                                            {User_data.level == "8" && <td> <div className="modal-trigger waves-effect waves-light btn" onClick={() => that.setState({ is_approved: true, userdata: item })}>
                                                {!item.is_active ? "Approved" : "View"}
                                            </div></td>}

                                            {/* <td> <div className="modal-trigger waves-effect waves-light btn" onClick={() => that.GetPassword(item)}>
                                                {!item.is_active ? "Pending" : "View"}
                                            </div></td> */}
                                        </tr>
                                    )
                                })
                                }

                            </tbody>
                        </table>
                    </div>

                )

            }

        }



    }
}

const mapStateToProps = (state) => {
    return {
        User_data: state.user.User_data,
        tenantLists: state.user.tenantLists
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        TenantRequest: (data) => dispatch(TenantRequest(data)),
        GetTenantRequest: (id) => dispatch(GetTenantRequest(id)),
        AddCompany: (data) => dispatch(AddCompany(data)),
        GetPassword: data => dispatch(GetPassword(data))
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(TenantRequestPage);
