import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import Payments from './Payments'
import Sidenav from '../Sidenav/Sidenav'
import Apps from './Apps'
import UserMenu from './UserMenu'
import * as ACT from '../../actions'
import * as user from '../../actions/users'

import './Topnav.css'
import User from '../Admin/Users/User';

import { checkTenatLogin } from "../../utils/helperFunctions";

class Topnav extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedItem: ''
    }
  }

  GetDropdown() {
    const { isLoggedIn, User_data } = this.props.user

    var menus = [
      <li key="1" className="non-hoverable"><Payments /></li>,
      <li key="2" className="non-hoverable" style={{ padding: '15px' }}>Credits: {isLoggedIn ? User_data.credits : 0}</li>,
    ];

    if (User_data && (User_data.level == 2)) {
      menus.push(<li key="3"><Link to="/tenantrequest">Request a tenant</Link></li>)
    }

    menus.push(
      <li key="4"><Link to="/profile">Profile</Link></li>,
      <li key="5" style={{ padding: '15px' }} onClick={this.props.logout}> Logout</li>
    )

    return menus
  }

  render() {
    const { isLoggedIn, User_data } = this.props.user

    var dropdown_menu_item = this.GetDropdown();

    return (
      <nav>
        {isLoggedIn &&
          <div>
            <Sidenav />
            <div className="nav-wrapper">
              <span className="brand-logo left">
                FLOWNGIN
              </span>
            </div>
          </div>

        }

        <div>
          <div className="nav-wrapper">
            <span className="brand-logo left">
              FLOWNGIN
          </span>
            {User_data &&

              <UserMenu
                Logout={this.Logout}
                className="right"
                item={dropdown_menu_item}
              />
            }
            {this.renderContent(User_data)}
          <Apps />
          </div>

        </div>

      </nav>
    )
  }

  renderContent(User_data) {
    const { selectedItem } = this.state
    switch (this.props.user.isLoggedIn) {
      case false:

      if(checkTenatLogin()==true) return false;
      
      return (
          <ul className="right">
            <li>
              <Link
                className={selectedItem === 'Login' ? "selected" : ""}
                to="/login"
              >
                Sign In
              </Link>
            </li>
            <li>
              <Link
                className={selectedItem === 'Sign Up' ? "selected" : ""}
                to="/registration"
              >
                Sign Up
              </Link>
            </li>
          </ul>
        )
      case true:
        return (
          <ul className="right">
            <li>
              <Link 
                to ="/"
                className={`dropdown-trigger ${selectedItem === 'apps' ? "selected" : ""}`}
                data-target="dropdown-apps"
                onClick={this.handleClickApps}>
                Apps
              </Link>
            </li>
            {/* <li>
              <Link className={selectedItem === 'dashboard' ? "selected" : ""}
                onClick={this.handleClickDashboard}
                to="/">
                Dashboard
              </Link>
            </li> */}
            {(User_data.level == 8 || User_data.level==6) &&
              <li>
                <Link className={selectedItem === 'admin' ? "selected" : ""}
                  onClick={this.handleClickAdmin}
                  to="/admin">
                  Admin
               </Link>
              </li>
            }
          </ul>
        )
      default:
        return <ul className="right"><li><a href="/auth/google">Login With Google</a></li></ul>
    }
  }

  Logout = (event) => {
    event.preventDefault();
    this.props.logout();

  }
  
   handleClickApps = () => {
    this.setState({ selectedItem: 'apps' })
  }

  handleClickDashboard = () => {
    this.setSidenavUser()
    this.setState({ selectedItem: 'dashboard' })
  }

  handleClickAdmin = () => {
    this.setSidenavAdmin()
    this.setState({ selectedItem: 'admin' })
  }

  setSidenavUser = () => {
    this.props.setApp('default')
    this.props.loadSidenavConfig('default')
  }

  setSidenavAdmin = () => {
    this.props.setApp('admin')
    this.props.loadSidenavConfig('admin')
  }
}

const mapStateToProps = ({ user }) => {
  return { user }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setApp: (appName) => dispatch(ACT.setApp(appName)),
    loadSidenavConfig: (appName) => dispatch(ACT.loadSidenavConfig(appName)),
    logout: () => dispatch(user.logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Topnav);
