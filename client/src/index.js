import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min.js';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from './containers/App';

import configureStore from './configureStore';

const store = configureStore();

ReactDOM.render(
  <Router basename="/">
    <Provider store={store}>
      <Route component={App} />
    </Provider>
  </Router>,
  document.querySelector('#root')
);
