const constant = {
    levels: [
                { name: "visitor", id: 1 },
                { name: "member", id: 2 },
                { name: "designer", id: 3 },
                { name: "programmer", id: 4 },
                { name: "reserved", id: 5 },
                { name: "site/tenant admin", id: 6 },
                { name: "Portal admin", id: 7 },
                { name: "Super admin", id: 8 },
           ]
}

export default constant ;