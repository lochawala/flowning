const express = require('express')
const mongoUtil = require('./server/services/mongoUtil')
const cookieSession = require('cookie-session')
const passport = require('passport')
const bodyParser = require('body-parser');
const keys = require('./config/keys');
var Company = require('./server/models/company');
var utility = require('.//server/services/utility');

// //Update by 23-01-2019

var db = require('./config/db');
var authenticate = require('./server/middlewares/authenticate').authenticate;
var oauth = require('./server/middlewares/authenticate').oauth;

//initialize connections
mongoUtil.connectToDB();


const app = express();
const PORT = process.env.PORT || 5000

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var Routers = require('./server/routes/index')(app);

app.use(bodyParser.json())
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);

app.use(passport.initialize())
app.use(passport.session())
require('./server/services/passport')

require('./server/routes/authRoutes')(app)
require('./server/routes/billingRoutes')(app)
require('./server/routes/formRoutes')(app)
require('./server/routes/sidenavRoutes')(app)
require('./server/routes/externalCollectionRoutes')(app)
require('./server/routes/eventApiRoutes')(app)
require('./server/routes/appsRoutes')(app)
require('./server/routes/mongodbUtilityRoutes')(app)


if (process.env.NODE_ENV === 'production') {
  // Express will serve up production assets
  // like our main.js file, or main.css file!
  app.use(express.static('client/build'))

  // Express will serve up the index.html profile
  // if it doesn't recognize the route
  const path = require('path')
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
}

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})
