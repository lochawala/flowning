var User = require('./../models/users.js');
var Company = require('./../models/company.js');
var utility = require('../services/utility');
var authenticate = require('../middlewares/authenticate');
var oauthServer = require('oauth2-server');
var Request = oauthServer.Request;
var Response = oauthServer.Response;
var sendMail = require('../services/sendMail');
const mongoUtil = require('../services/mongoUtil');

var userLogin = function (req, res, next) {
    try{
        if (req.body.isLogin == 2) {
            req.headers['content-type'] = 'application/x-www-form-urlencoded';
            req.body['grant_type'] = 'password';
            req.body['password'] = req.body.password;
            req.body['username'] = req.body.email;
            var request = new Request(req);
            var response = new Response(res);
            const User = mongoUtil.connectDB(utility.getHostName(req), 'users');
            if (User) {
                User.findOne({ email: req.body.email }).then(function (user, err) {
                    if (err) {
                        res.send({ "status": false, "message": err, "status_code": 202 });
                    } else {
                        if (user) {
                            if (user.is_verify) {
                                authenticate.outhauthenticate(request, response)
                                    .then(function (token) {
                                        var response_data = {
                                            access_token: token.accessToken,
                                            refresh_token: token.refreshToken,
                                        }
                                        token.user = JSON.parse(JSON.stringify(token.user))
                                        response_data.user = token.user
                                        res.json({ "status_code": 200, status: true, data: response_data })
                                    }).catch(function (err) {
                                        res.json({ "status_code": 201, status: false, message: err.message })
                                    });
                            } else {
                                res.json({ "status": false, "message": "Please verify your account first, Thank you.", "status_code": 202 });
                            }
                        } else {
                            res.json({ "status": false, "message": "OOPS! Your account is Doesn't exist.", "status_code": 202 });
                        }
                    }
                });
            } else {
                res.json({ "status": false, "message": "OOPS!.", "status_code": 202 });
            }
        } else {
            req.headers['content-type'] = 'application/x-www-form-urlencoded';
            req.body['grant_type'] = 'password';
            req.body['password'] = req.body.password;
            req.body['username'] = req.body.email;
            var request = new Request(req);
            var response = new Response(res);
            Company.findOne({ emailDomain: utility.getHostInfo(req).request_emaildomain }).exec((err, company_data) => {
                if (err) {
                    res.json({ "status": false, "message": "Some thing is wrong, Please try to contact support.", "status_code": 202 });
                } else {
                    if (company_data) {
                        if (company_data.hostname != utility.getHostInfo(req).hostname) {
                            var redirect_url = (utility.IsProduction(req.hostname)) ? ".flowngin.com" : ".localhost:3000"
                            var temp_data = {
                                user: {
                                    level: "6",
                                    domain: company_data.hostname + redirect_url,
                                    isView: "login",
                                    email: req.body.email,
                                    password: req.body.password
                                }
                            }
                        }
                        res.json({ "status_code": 200, status: true, data: temp_data })
                    } else {
                        User.findOne({ email: req.body.email }).exec(function (err, user) {
                            if (err) {
                                res.send({ "status": false, "message": err.message, "status_code": 202 });
                            } else {
                                if (user) {
                                    if (user.is_verify) {
                                        authenticate.oauth.token(request, response)
                                            .then(function (token) {
                                                var response_data = {
                                                    access_token: token.accessToken,
                                                    refresh_token: token.refreshToken,
                                                }
                                                token.user = JSON.parse(JSON.stringify(token.user))
                                                response_data.user = token.user
                                                res.json({ "status_code": 200, status: true, data: response_data })
                                            }).catch(function (err) {
                                                res.json({ "status_code": 2012, status: false, message: err.message })
                                            });
                                    } else {
                                        res.json({ "status": false, "message": "Please verify your account first, Thank you.", "status_code": 202 });
                                    }
    
                                } else {
                                    res.json({ "status": false, "message": "OOPS! Your account is Doesn't exist.", "status_code": 202 });
                                }
                            }
                        });
                    }
                }
            })
        }
    }catch(err){
        res.json({ "status": false, "message": err.message, "status_code": 201 });
    }


}

var userLogout = function (req, res, next) {
}

var userRefreshToken = function (req, res, next) {
    req.headers['content-type'] = 'application/x-www-form-urlencoded';
    req.body['grant_type'] = 'refresh_token';
    var request = new Request(req);
    var response = new Response(res);
    authenticate.oauth.token(request, response)
        .then(function (token) {
            var response_data = {
                access_token: token.accessToken,
                refresh_token: token.refreshToken,
            }
            response_data.user = token.user
            res.json({ "status_code": 200, status: true, data: response_data })
        }).catch(function (err) {
            res.status(500).json(res.json({ "status_code": 400, status: false, message: "user credentials are invalid" }))
        });

}

var userRegistration = function (req, res, next) {

    var fname = req.body.fname;
    var lname = req.body.lname;
    var email = req.body.email;
    var password = req.body.password;

    var userarr = {
        firstname: fname,
        lastname: lname,
        email: email,
        password: utility.encrypt(password),
        verify_code: Math.floor((Math.random() * 999999) + 111111),
        login_type: "1",
        level: "2", // 2 User,
        credits: 0
    }

    User.findOne({ email: email }, function (err, user) {
        if (err) {
            res.send({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
        } else {
            if (user) {
                res.send({ "status": false, "message": 'This email address already exist', "status_code": 200 });
            } else {
                var user = new User(userarr);
                user.save(function (err) {
                    if (err) {
                        res.send({ "status": "false", err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                    } else {
                        sendMail.sendVerifyEmail({ emailTo: req.body.email, name: fname + " " + lname, verify_code: userarr.verify_code });
                        msg = "Profile created successfully, Please check your mail."
                        res.send({ "status": true, message: msg, "status_code": 200 });
                    }
                });
            }
        }
    })

}

var tokenVerification = function (req, res, next) {
    if (utility.IsTenant(req.hostname)) {
        const User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            var token = parseInt(req.body.token);
            User.findOne({ verify_code: token }).then((user_data, err) => {
                if (err) {
                    res.send({ "status": false, "message": err.message, "status_code": 202 });
                } else {
                    if (user_data) {
                        user_data.verify_code = null
                        user_data.is_verify = true
                        User.update({ _id: user_data._id }, user_data).then((done, err) => {
                            if (err) {
                                res.send({ "status": false, err, "message": err.message, "status_codeuser_data": 202 });
                            } else {
                                res.send({ "status": true, "message": "Suceessfully Verification", "status_code": 200, user_id: user_data._id });
                            }
                        }).catch(e => {
                            res.send({ "status": false, "message": e.message, "status_code": 202 });
                        });
                    } else {
                        res.send({ "status": false, "message": "Not Found User.", "status_code": 201 });
                    }

                }
            }).catch(e => {
                res.send({ "status": false, "message": e.message, "status_code": 202 });
            });
        } else {
            res.send({ "status": false, "message": "Verification Failed.", "status_code": 202 });
        }
    } else {
        User.findOne({ verify_code: req.body.token }, function (err, user) {
            if (err) {
                res.send({ "status": false, "message": err.message, "status_code": 202 });
            } else {
                if (user) {
                    user.verify_code = null;
                    user.is_verify = true
                    user.save(function (err) {
                        if (err) {
                            res.send({ "status": false, err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                        } else {
                            res.send({ "status": true, "message": "Suceessfully Verification", "status_code": 200, user_id: user._id });
                        }
                    });

                } else {
                    res.send({ "status": false, "message": "Verification Failed", "status_code": 202 });
                }

            }
        })
    }

}

var forgetUserPassword = function (req, res, next) {
    if (utility.IsTenant(req.hostname)) {
        User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            User.findOne({ email: req.body.email }).then((user_data) => {
                if (user_data) {
                    user_data.verify_code = Math.floor((Math.random() * 999999) + 111111);
                    User.update({ _id: mongoUtil.ObjectId(user_data._id) }, user_data).then((done, err) => {
                        if (err) {
                            res.send({ "status": false, err, "message": err.message, "status_codeuser_data": 202 });
                        } else {
                            var host = (utility.IsProduction(req.hostname)) ? utility.getHostName(req) + ".flowngin.com" : utility.getHostName(req) + ".localhost:3000"
                            var payload = {
                                name: user_data.firstname + " " + user_data.lastname,
                                emailTo: user_data.email,
                                url: "http://" + host + "/login?token=" + user_data.verify_code,
                                host: req.hostname
                            }
                            sendMail.forgetUserPassword(payload);
                            res.send({ "status": true, "message": "Please check your mail", "status_code": 200, data: payload });
                        }
                    }).catch(e => {
                        res.send({ "status": false, "message": e.message, "status_code": 202 });
                    });
                } else {
                    res.send({ "status": false, "message": "Not Found User.", "status_code": 201 });
                }
            }).catch(e => {
                res.send({ "status": false, "message": e.message, "status_code": 202 });
            });
        } else {
            res.send({ "status": false, "message": "Process Failed.", "status_code": 202 });
        }
    } else {
        User.findOne({ email: req.body.email }, function (err, user) {
            if (err) {
                res.send({ "status": false, "message": err.message, "status_code": 202 });
            } else {
                if (user) {
                    user.verify_code = Math.floor((Math.random() * 999999) + 111111);
                    user.save(function (err) {
                        if (err) {
                            res.send({ "status": false, err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                        } else {
                            var url = "http://localhost:3000/login?token=" + user.verify_code
                            if (utility.IsProduction(req.hostname)) {
                                url = "http://portal.flowngin.com/login?token=" + user.verify_code
                            }

                            sendMail.forgetUserPassword({
                                emailTo: user.email,
                                name: user.firstname + " " + user.lastname,
                                verify_code: user.verify_code,
                                url : url
                            });
                            res.send({ "status": true, "message": "Please check your mail", "status_code": 200 });
                        }
                    });
                } else {
                    res.send({ "status": false, "message": "This Email address doesn't exist", "status_code": 202 });
                }
            }
        });
    }
}

var changepassword = function (req, res, next) {

    if (utility.IsTenant(req.hostname)) {
        const User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            var user_id = parseInt(req.body.user_id);
            User.findOne({ _id: mongoUtil.ObjectId(req.body.user_id) }).then((user_data, err) => {
                if (err) {
                    res.send({ "status": false, "message": err.message, "status_code": 202 });
                } else {
                    if (user_data) {
                        user_data.password = utility.encrypt(req.body.password),
                            User.update({ _id: user_data._id }, user_data).then((done, err) => {
                                if (err) {
                                    res.send({ "status": false, err, "message": err.message, "status_codeuser_data": 202 });
                                } else {
                                    res.send({ "status": true, "message": "Process successfull complated.", "status_code": 200 });
                                }
                            }).catch(e => {
                                res.send({ "status": false, "message": e.message, "status_code": 202 });
                            });
                    } else {
                        res.send({ "status": false, "message": "Not Found User.", "status_code": 201 });
                    }

                }
            }).catch(e => {
                res.send({ "status": false, "message": e.message, "status_code": 202 });
            });
        } else {
            res.send({ "status": false, "message": "Verification Failed.", "status_code": 202 });
        }
    } else {
        User.findOne({ _id: req.body.user_id }, function (err, user) {
            if (err) {
                res.send({ "status": false, "message": err.message, "status_code": 202 });
            } else {
                if (user) {
                    user.password = utility.encrypt(req.body.password),
                        user.save(function (err) {
                            if (err) {
                                res.send({ "status": false, err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                            } else {
                                res.send({ "status": true, "message": "Process successfull complated.", "status_code": 200 });
                            }
                        });
                } else {
                    res.send({ "status": false, "message": "Update Failed.", "status_code": 202 });
                }
            }
        });
    }


}



exports.userLogin = userLogin;
exports.userLogout = userLogout;
exports.userRegistration = userRegistration;
exports.userRefreshToken = userRefreshToken;
exports.tokenVerification = tokenVerification;
exports.forgetUserPassword = forgetUserPassword;
exports.changepassword = changepassword;