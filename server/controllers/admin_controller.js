var User = require('./../models/users.js');
var Company = require('./../models/company.js');
var Tenant = require('./../models/tenant.js');
var config = require('../../config/index');
var utility = require('../services/utility');
var mongoUtil = require('../services/mongoUtil');
var sendMail = require('../services/sendMail');
var GetAdminConfig = (req, done) => {
    if (utility.IsTenant(req.hostname)) {
        Company.findOne({ hostname: utility.getHostName(req) }).exec(function (err, userlist) {
            if (!err) {
                User = mongoUtil.connectDB(utility.getHostName(req), 'users');
                if (User) {
                    User.findOne({ _id: mongoUtil.ObjectId(req.body.user_id) }).then(function (res_data) {
                        if (!res_data) {
                            done({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
                        } else {
                            var data = {
                                ...JSON.parse(JSON.stringify(userlist)),
                                user: res_data
                            };
                            done({ "status": true, data: data, status_code: 200 })
                        }
                    });
                } else {
                    res.json({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
                }
            } else {
                res.json({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
            }
        });

    } else {
        Company.findOne().populate('user').exec(function (err, userlist) {
            if (!err) {
                config.smtp = {
                    smtp_server: userlist.smtp_server,
                    smtp_port: userlist.smtp_port,
                    smtp_auth: userlist.smtp_auth,
                    smtp_fromMail: userlist.smtp_fromMail
                }
                done({ "status_code": 200, status: true, data: userlist })
            } else {
                res.json({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
            }
        });
    }
}

var EditSetting = (req, done) => {
    var data = req.body;
    if (utility.IsTenant(req.hostname)) {
        Company.findOne({ hostname: utility.getHostName(req) }).exec(function (err, companyData) {
            if (!err) {
                Company.update({_id : companyData._id},data, function (err) {
                    if (!err) {
                        done({ "status_code": data, status: true })
                    } else {
                        done({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
                    }
                });
            } else {
                res.json({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
            }
        });

    }else{
        Company.update({}, data, function (err) {
            if (!err) {
                done({ "status_code": 200, status: true })
            } else {
                done({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
            }
        });
    }
    
}

var EditCompanyAdminUser = (req, done) => {
    var data = req.body;
    if (utility.IsTenant(req.hostname)) {
        const User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            User.update(
                { _id: mongoUtil.ObjectId(data.id) },
                { $set: { firstname: data.firstname, lastname: data.lastname } }
            ).then(function (user) {
                done({ "status_code": 200, status: true })
            });
        } else {
            done({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
        }
    } else {
        User.update({ _id: data.id }, { firstname: data.firstname, lastname: data.lastname }, function (err) {
            if (!err) {
                done({ "status_code": 200, status: true })
            } else {
                done({ "status": false, "message": "OOPS! Data Doesn't exist.", "status_code": 202 });
            }
        });
    }

}

var TenantRequest = (data, done) => {
    Tenant.find({
        $and: [
            { $or: [{ name: data.name }, { email: data.email }, { db_connection: data.db_connection }] },
            { db_connection: { $ne: "" } }
        ]
    }).exec(function (err, result) {
        if (err) {
            done({ "status": false, "message": err.message, "status_code": 202 });
        } else {
            if (result.length == 0) {
                const payload = {
                    name: data.name,
                    email: data.email,
                    user: data.user,
                    is_system: data.is_system,
                    db_user: data.db_user,
                    db_connection: data.db_connection,
                    db_password: utility.encrypt(data.db_password),
                    domain: data.email.split("@")[1],
                    status: "new"
                }
                if (data.is_system == false) {
                    mongoUtil.CheckValidateConnectionURL(data.db_connection, (res_data) => {
                        if (res_data.res == "0") {
                            var tenant = new Tenant(payload);
                            tenant.save(function (err) {
                                if (err) {
                                    done({ "status": false, err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                                } else {
                                    done({ "status": true, "message": "Process successfull complated.", "status_code": 200 });
                                }
                            });
                        } else {
                            done({ "status": false, "message": res_data.message, "status_code": 202, data: result });
                        }

                    });
                } else {
                    var tenant = new Tenant(payload);
                    tenant.save(function (err) {
                        if (err) {
                            done({ "status": false, err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                        } else {
                            done({ "status": true, "message": "Process successfull complated.", "status_code": 200 });
                        }
                    });
                }

            } else {
                done({ "status": false, "message": "already exist.", "status_code": 202, data: result });
            }
        }
    })
}

var GetTenantRequest = (data, done) => {
    if (data.id == 0) {
        Tenant.find().populate('company').populate('user').exec(function (err, result) {
            if (err) {
                done({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
            } else {
                done({ "status": true, "message": "Process successfull complated.", "status_code": 200, data: result });
            }
        })
    } else {
        Tenant.find({ user: data.id }).exec(function (err, result) {
            if (err) {
                done({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
            } else {
                done({ "status": true, "message": "Process successfull complated.", "status_code": 200, data: result });
            }
        })
    }
}

var AddCompany = (reqhost, data, done) => {
    Company.findOne({ database_url: data.db_url }).exec(function (err, result) {
        if (err) {
            done({ "status": false, "message": "Failed.", "status_code": 201 });
        } else {
            if (result) {
                done({ "status": false, "message": "DB URL already exist.", "status_code": 201 });
            } else {
                mongoUtil.CheckValidateConnectionURL(data.db_url, (res_data) => {
                    if (res_data.res == "0") {
                        mongoUtil.newconnectToDB(data.hostname, data.db_url, (new_res_data) => {
                            if (new_res_data) {
                                var User = mongoUtil.connectDB(data.hostname, 'users');
                                var oauthclients = mongoUtil.connectDB(data.hostname, 'oauthclients');
                                var oauthscopes = mongoUtil.connectDB(data.hostname, 'oauthscopes');
                                var sidenav = mongoUtil.connectDB(data.hostname, 'sidenav');
                                if (User && oauthclients && oauthscopes) {
                                    var companyData = {
                                        company_name: data.company_name,
                                        logo: "",
                                        address: data.address,
                                        country: data.country,
                                        hostname: data.hostname,
                                        emailDomain: data.emailDomain,
                                        smtp_server: "",
                                        smtp_port: "",
                                        smtp_auth: "",
                                        smtp_fromMail: "",
                                        database_url: data.db_url,
                                        user: data.user_id
                                    }
                                    var companyData = new Company(companyData)

                                    companyData.save((id) => {
                                        Tenant.update({ _id: data._id }, {
                                            is_active: true,
                                            company: companyData._id
                                        }).exec((err, user_data) => {

                                            var oauthclientsPayload = {
                                                "client_id": "democlient",
                                                "client_secret": "democlientsecret",
                                                "redirect_uri": "http://localhost/3000",
                                            }

                                            var oauthscopesPayload = [
                                                {
                                                    "scope": "profile",
                                                    "is_default": false,
                                                },
                                                {
                                                    "scope": "defaultscope",
                                                    "is_default": true,
                                                }
                                            ];
                                            var sidenavPayload = [
                                                {
                                                    "appName": "admin",
                                                    "groupLinks": [
                                                        {
                                                            "header": "Admin",
                                                            "dividerBottom": false,
                                                            "links": [
                                                                {
                                                                    "name": "user",
                                                                    "route": "/user",
                                                                    "icon": "account_circle",
                                                                    "text": "User management"
                                                                },
                                                                {
                                                                    "name": "Task palette",
                                                                    "route": "/tenantrequest",
                                                                    "icon": "account_circle",
                                                                    "text": "Task Palette"
                                                                },
                                                                {
                                                                    "name": "settings",
                                                                    "route": "/settings",
                                                                    "icon": "settings",
                                                                    "text": "Settings"
                                                                },
                                                                {
                                                                    "name": "Templates",
                                                                    "route": "/Templates",
                                                                    "icon": "account_circle",
                                                                    "text": "Templates"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "appName": "default",
                                                    "groupLinks": [
                                                        {
                                                            "header": "Setup",
                                                            "dividerBottom": false,
                                                            "links": [
                                                                {
                                                                    "name": "collection-list",
                                                                    "route": "/collection-list",
                                                                    "icon": "apps",
                                                                    "text": "Collections"
                                                                },
                                                                {
                                                                    "name": "sidenav-setup",
                                                                    "route": "/sidenav-setup",
                                                                    "icon": "settings",
                                                                    "text": "Sidenav"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }

                                            ]
                                            var password = Math.random().toString(36).substring(2);
                                            var UserPayload = {
                                                firstname: data.firstname,
                                                lastname: data.lastname,
                                                email: data.company_email,
                                                password: utility.encrypt("123456"),
                                                mobile: data.mobile,
                                                address: data.address,
                                                level: 6,
                                                verify_code: Math.floor((Math.random() * 999999) + 111111),
                                                login_type: 1,
                                                credits: 0,
                                                is_verify: true,
                                                is_deleted: false,
                                                is_active: true,
                                            }
                                            if (oauthclients && oauthscopes) {
                                                oauthclients.insertOne(oauthclientsPayload).then((data, err) => {
                                                    oauthscopes.insertMany(oauthscopesPayload).then((data, err) => {
                                                        sidenav.insertMany(sidenavPayload).then((data, err) => {
                                                            User.insertOne(UserPayload).then((data, err) => {
                                                                var host = utility.IsProduction(reqhost) ? ".flowngin.com" : ".localhost:3000" 
                                                                var payload = {
                                                                    name: UserPayload.firstname + " " + UserPayload.lastname,
                                                                    emailTo : UserPayload.email,
                                                                    url: "http://" + companyData.hostname + host + "/login?token=" + UserPayload.verify_code
                                                                }
                                                                sendMail.resetTenantUserPassword(payload)
                                                                done({ "status": true, "message": "Tenant request approved." , "status_code": 200, data: payload });
                                                            });
                                                        })
                                                    });
                                                });
                                            } else {
                                                done({ "status": false, "message": "Approved Failed", "status_code": 201 });
                                            }
                                        });
                                    });
                                } else {
                                    mongoUtil.DisconnectionMongoURL(data.hostname);
                                    done({ "status": false, "message": "Some thing worng. Please try again leter.", "status_code": 201 });
                                }
                            } else {
                                mongoUtil.DisconnectionMongoURL(data.hostname);
                                done({ "status": false, "message": new_res_data.message, "status_code": 201 });
                            }
                        });
                    } else {
                        done({ "status": false, "message": res_data.message, "status_code": 201 });
                    }
                });
            }
        }
    });

}

var GetPassword = (data, done) => {
    var User = mongoUtil.connectDB(data.hostname, 'users');
    User.findOne({ email: data.email }).then(function (userlist, err) {
        if (err) {
            done({ "status": false, "message": "Some thing is worng", "status_code": 202 });
        } else {
            done({ "status": true, "data": utility.decrypt(userlist.password) });
        }
    })
}

exports.GetAdminConfig = GetAdminConfig
exports.EditSetting = EditSetting
exports.EditCompanyAdminUser = EditCompanyAdminUser
exports.TenantRequest = TenantRequest
exports.GetTenantRequest = GetTenantRequest
exports.AddCompany = AddCompany
exports.GetPassword = GetPassword