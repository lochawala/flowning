const mongoUtil = require('../services/mongoUtil');
const mongodb = require('mongodb')

var AddSurvey = (req, res, next) => {
    const Survey = mongoUtil.getDB().collection('survey')
    if (req.body._id == "" || !req.body._id) {
        delete req.body._id
        Survey.insertOne(req.body).then((data, err) => {
            res.json({ "status": true, "message": "Success." });
        });
    } else {
        Survey.update({ "_id": mongodb.ObjectID(req.body._id) }, req.body).then((data, err) => {
            res.json({ "status": true, "message": "Success." });
        });
    }
}

var SurveyList = (req, res, next) => {
    const Survey = mongoUtil.getDB().collection('survey')
    Survey.find({}).toArray((err, data) => {
        res.json({ "status": true, "data": data });
    })
}

var GetQuestionsData = (req, res, next) => {
    const Survey = mongoUtil.getDB().collection('survey');
    var id = { external_link: req.body.id };
    if(req.body.key){
        id = { _id: mongodb.ObjectID(req.body.id) };
    } 
    Survey.findOne(id).then((data, err) => {
        res.json({ "status": true, "data": data });
    })
}

var ServeyDelete = (req, res, next) => {
    const Survey = mongoUtil.getDB().collection('survey')
    Survey.remove({ _id: mongodb.ObjectID(req.body.id) }).then((data, err) => {
        res.json({ "status": true, "data": data });
    })
}

var AddSurveyResponse = (req, res, next) => {
    const surveyresponse = mongoUtil.getDB().collection('surveyresponse')
    surveyresponse.insertOne(req.body).then((data, err) => {
        res.json({ "status": true, "message": "Success." });
    });
}

var AddSurveyResponse = (req, res, next) => {
    const surveyresponse = mongoUtil.getDB().collection('surveyresponse')
    surveyresponse.insertOne(req.body).then((data, err) => {
        res.json({ "status": true, "message": "Success." });
    });
}

var GetSurveyResponse = (req, res, next) => {
    const surveyresponse = mongoUtil.getDB().collection('surveyresponse')
    surveyresponse.find({survey_id:req.body.id}).toArray((err, data) => {
        res.json({ "status": true, "message": "Success." , data : data });
    });
}
var GetSurveyResponseanswer = (req, res, next) => {
    const surveyresponse = mongoUtil.getDB().collection('surveyresponse')
    surveyresponse.find({ _id : mongodb.ObjectID(req.body.id)}).toArray((err, data) => {
        res.json({ "status": true, "message": "Success." , data : data });
    });
}




exports.AddSurvey = AddSurvey
exports.SurveyList = SurveyList
exports.GetQuestionsData = GetQuestionsData
exports.ServeyDelete = ServeyDelete
exports.AddSurveyResponse = AddSurveyResponse
exports.GetSurveyResponse = GetSurveyResponse
exports.GetSurveyResponseanswer = GetSurveyResponseanswer
