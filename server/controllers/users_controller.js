var User = require('./../models/users.js');
var Company = require('./../models/company.js');
var utility = require('../services/utility');
var sendMail = require('../services/sendMail');
var config = require('../../config/index');
const mongoUtil = require('../services/mongoUtil');

var AddUsers = (req, res, next) => {
    var fname = req.body.firstname;
    var lname = req.body.lastname;
    var email = req.body.email;
    var selected_level = req.body.level

    var userarr = {
        firstname: fname,
        lastname: lname,
        email: email,
        password: utility.encrypt("123456"),
        verify_code: Math.floor((Math.random() * 999999) + 111111),
        login_type: "1",
        level: selected_level, // 2 User,
        credits: 0,
        is_verify: false
    }

    if (utility.IsTenant(req.hostname)) {
        User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            if (req.body._id) {
                res.json({ "status_code": 200, status: false, data: [] })
            } else {
                User.findOne({ email: email }, function (err, user) {
                    if (err) {
                        res.send({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                    } else {
                        if (user) {
                            res.send({ "status": false, "message": 'This email address already exist', "status_code": 200 });
                        } else {
                            User.insertOne(userarr).then((data, err) => {
                                if (err) {
                                    res.send({ "status": "false", err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                                } else {
                                    var host = (utility.IsProduction(req.hostname)) ? utility.getHostName(req) + ".flowngin.com" : utility.getHostName(req) + ".localhost:3000"
                                    var payload = {
                                        name: userarr.firstname + " " + userarr.lastname,
                                        emailTo: userarr.email,
                                        url: "http://" + host + "/login?token=" + userarr.verify_code,
                                        host : req.hostname
                                    }
                                    sendMail.resetTenantUserPassword(payload)
                                    msg = "Profile created successfully"
                                    res.send({ "status": true, message: msg, "status_code": 200 });
                                }
                            });
                        }
                    }
                })


            }
        } else {
            res.json({ "status_code": 200, status: true, data: [] })
        }
    } else {
        if (req.body._id) {
            User.update({ _id: req.body._id }, { firstname: fname, lastname: lname, email: email, level: selected_level }, function (err, user) {
                if (err) {
                    res.send({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                } else {
                    res.send({ "status": true, "message": 'Update sccessfull!', "status_code": 200 });
                }
            })
        } else {
            User.findOne({ email: email }, function (err, user) {
                if (err) {
                    res.send({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                } else {
                    if (user) {
                        res.send({ "status": false, "message": 'This email address already exist', "status_code": 200 });
                    } else {
                        var user = new User(userarr);
                        user.save(function (err) {
                            if (err) {
                                res.send({ "status": "false", err, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
                            } else {
                                var url = "http://localhost:3000/login?token=" + userarr.verify_code
                                if(utility.IsProduction(req.hostname)){
                                    url = "http://portal.flowngin.com/login?token=" + userarr.verify_code
                                } 
                                sendMail.resetNewUserPassword({ 
                                    emailTo: userarr.email, 
                                    name: userarr.firstname + " " + userarr.lastname,
                                    url : url,
                                    verify_code: userarr.verify_code });
                                msg = "Profile created successfully"
                                res.send({ "status": true, message: msg, "status_code": 200 });
                            }
                        });
                    }
                }
            })
        }
    }
}

var GetUserList = (req, res, next) => {
    if (utility.IsTenant(req.hostname)) {
        User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            User.aggregate([{
                $match: { level: { $ne: 6 } }
            }
            ]).toArray((err, result) => {
                if (err) {
                    res.json({ "status": false, "message": err.message, "status_code": 202 });
                } else {
                    res.json({ "status": true, status: true, "data": result, "status_code": 200, });
                }
            });
        } else {
            res.json({ "status_code": 200, status: true, data: [] })
        }
    } else {
        User.find({ level: { $ne: 8 } })
            .exec(function (err, result) {
                var output = result.filter((item) => item.level != null)
                if (err) res.json({ "status": false, "message": err.message, "status_code": 202 });
                else res.json({ "status_code": 200, status: true, data: output })
            })
    }
}

var updateProfile = (req, res, next) => {
    if (utility.IsTenant(req.hostname)) {
        User = mongoUtil.connectDB(utility.getHostName(req), 'users');
        if (User) {
            User.update({ _id:mongoUtil.ObjectId(req.body.id) }, 
                { $set:  req.body}).then((result) => {
                res.json({ "status_code": 200, "message": "Update Successfully.", status: true  })
            }).catch(err=>{
                res.json({ "status": false, "message": "OOPS! Your account is Doesn't exist.", "status_code": 202 });
            });
        } else {
            res.json({ "status": false, "message": "OOPS! Your account is Doesn't exist.", "status_code": 2012 });
        }
    } else {
        User.update({ _id: req.body.id }, req.body, (err, result) => {
            if (err) res.json({ "status": false, "message": "OOPS! Your account is Doesn't exist.", "status_code": 202 });
            else res.json({ "status_code": 200, "message": "Update Successfully.", status: true  })
        });
    }
}

var DeleteUsers = (req, res, next) => {
    User.find({ level: 2 }, (err, result) => {
        if (err) res.json({ "status": false, "message": "OOPS! Your account is Doesn't exist.", "status_code": 202 });
        else res.json({ "status_code": 200, status: true, data: result })
    })
}

exports.AddUsers = AddUsers
exports.GetUserList = GetUserList
exports.updateProfile = updateProfile
exports.DeleteUsers = DeleteUsers
exports.DeleteUsers = DeleteUsers
