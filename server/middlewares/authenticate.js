const oauthServer = require("oauth2-server");
var Company = require('./../models/company.js');
const mongoUtil = require('../services/mongoUtil');
var utility = require('../services/utility');
var oauthTenant = require('./oauthTenant');
var Request = oauthServer.Request;
var Response = oauthServer.Response;

var oauth = new oauthServer({
  debug: true,
  model: require('./oauth.js')
});

var authenticate = function (options) {
  var options = options || {};
  return function (req, res, next) {
    var request = new Request({
      headers: { authorization: req.headers.authorization },
      method: req.method,
      query: req.query,
      body: req.body
    });
    var response = new Response(res);
    if (utility.IsTenant(req.hostname)) {
      IsTenantConnection(req, function (data) {
        if (data.status) {
          TenantConfig(req, (oauthT) => {
            oauthT.authenticate(request, response, options)
              .then(function (token) {
                req.user = token
                next()
              })
              .catch(function (err) {
                // Request is not authorized.
                var message = err.message.split(":")[1] ? err.message.split(":")[1] : "";
                if(message==" no authentication given"){
                  message = "Authentication Failed."
                }else if(message==" access token has expired"){
                  message = "Authentication token has expired."
                }else{
                  message = "Authentication Failed."
                }
                res.json({status : false , message : message})
              });
          });
        } else {
          res.json(data);
        }
      });
    } else {
      oauth.authenticate(request, response, options)
        .then(function (token) {
          req.user = token
          next()
        })
        .catch(function (err) {
          // Request is not authorized.
          var message = err.message.split(":")[1] ? err.message.split(":")[1] : "";
          if(message==" no authentication given"){
            message = "Authentication Failed."
          }else if(message==" access token has expired"){
            message = "Authentication token has expired."
          }else{
            message = "Authentication Failed."
          }
          res.json({status : false , message : message})
        });
    }
  }
}

var TenantConnection = () => {
  return function (req, res, next) {
    IsTenantConnection(req, function (data) {
      if (data.status) {
        next();
      } else {
        res.json(data);
      }
    })
  }
}

var IsTenantConnection = (req, done) => {
  if (utility.IsTenant(req.hostname)) {
    var Users = mongoUtil.connectDB(utility.getHostName(req), 'users');
    if (Users) {
      done({ "status": true })
    } else {
      Company.findOne({ hostname: utility.getHostInfo(req).hostName }).exec((err, company_data) => {
        if (err) {
          done({ "status": false, "message": "Some thing is wrong, Please try to contact support.", "status_code": 202 });
        } else {
          if (company_data) {
            var hostname = utility.getHostInfo(req).hostName;
            mongoUtil.newconnectToDB(hostname, company_data.database_url, () => {
              done({ "status": true })
            });
          }

        }
      })
    }
  } else {
    done({ "status": true });
  }
}

var outhauthenticate = (request, response) => {
  return new Promise((resolve, reject) => {
    TenantConfig(request.headers, (oauthT) => {
      var res = oauthT.token(request, response).then(function (token) {
        return token
      });
      resolve(res);
    })
  })
}

var TenantConfig = (req, done) => {
   oauthTenant.SetCollection({
    User: mongoUtil.connectDB(utility.getHostName({hostname :  req.host}), 'users'),
    OAuthClient: mongoUtil.connectDB(utility.getHostName({hostname :  req.host}), 'oauthclients'),
    OAuthAccessToken: mongoUtil.connectDB(utility.getHostName({hostname :  req.host}), 'oauthaccesstokens'),
    OAuthAuthorizationCode: mongoUtil.connectDB(utility.getHostName({hostname :  req.host}), 'OAuthAuthorizationCode'),
    OAuthRefreshToken: mongoUtil.connectDB(utility.getHostName({hostname :  req.host}), 'refreshtokens')
  });

  var oauthT = new oauthServer({
    debug: true,
    model: {
      getAccessToken: oauthTenant.getAccessToken,
      getAuthorizationCode: oauthTenant.getAuthorizationCode, //getOAuthAuthorizationCode renamed to,
      getClient: oauthTenant.getClient,
      getRefreshToken: oauthTenant.getRefreshToken,
      getUser: oauthTenant.getUser,
      getUserFromClient: oauthTenant.getUserFromClient,
      revokeAuthorizationCode: oauthTenant.revokeAuthorizationCode,
      revokeToken: oauthTenant.revokeToken,
      saveToken: oauthTenant.saveToken,//saveOAuthAccessToken, renamed to
      saveAuthorizationCode: oauthTenant.saveAuthorizationCode, //renamed saveOAuthAuthorizationCode,
      verifyScope: oauthTenant.verifyScope,
    }
  });

  done(oauthT)
}



module.exports.oauth = oauth;
module.exports.authenticate = authenticate;
module.exports.TenantConnection = TenantConnection;
module.exports.outhauthenticate = outhauthenticate;