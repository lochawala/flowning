const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// create a schema
const CompanySchema = new Schema({
    // Simple declaration of datatype that will be used:
    company_name: {
        type: String,
        required: true
    },
    user:  { type: Schema.Types.ObjectId, ref: 'User' },
    logo: {
        type: String,
    },
    address: {
        type: String,
    },
    country: {
        type: String,
    },
    hostname : {
        type: String,
    },
    emailDomain : {
        type: String,
    },
    smtp_server: {
        type: String,
    },
    smtp_port: {
        type: Number,
    },
    smtp_auth: {
        type: Object,
    },
    smtp_fromMail : {
        type: String,
    },
    database_url: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date
    }
}, {
        timestamps: true,
        usePushEach: true,
    });

module.exports = mongoose.model('company', CompanySchema);
