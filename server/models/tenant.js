const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// create a schema
const TenantSchema = new Schema({
    // Simple declaration of datatype that will be used:
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    is_system : {
        type: Boolean,
        default: false
    },
    db_user :  {
        type: String
    },
    db_connection :  {
        type: String
    },
    db_password :  {
        type: String
    },
    country : {
        type: String
    },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    company: { type: Schema.Types.ObjectId, ref: 'company' },
    domain : {
        type: String,
        required: true
    },
    is_active: {
        type: Boolean,
        default: false
    },
    status: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    },

}, {
        timestamps: true,
        usePushEach: true,
    });

module.exports = mongoose.model('tenant', TenantSchema, 'tenants');
