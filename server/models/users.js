const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// create a schema
const userSchema = new Schema({
  // Simple declaration of datatype that will be used:

  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  credits: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  verify_code: {
    type: String,
  },
  is_verify: {
    type: Boolean,
    default: false
  },
  googleId: {
    type: String,
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  is_active: {
    type: Boolean,
    default: true
  },
  level: {
    type: String,
    required: true
  },
  login_type: {
    type: String, // 1 creadential, 2 social
    required: true
  },
  mobile: {
    type: String
  },
  address: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  }
}, {
    timestamps: true,
    usePushEach: true,
  });

module.exports = mongoose.model('User', userSchema, 'users');
