var express = require('express');
var router = express.Router();

var accessController = require('../controllers/access_controller');
var authenticated = require('../middlewares/authenticate');
var oauthServer = require('oauth2-server');
var Request = oauthServer.Request;
var Response = oauthServer.Response;
var authenticate = authenticated.authenticate;
var TenantConnection = authenticated.TenantConnection;
var oauth = authenticated.oauth;
var sendMail = require('../services/sendMail');
const cors = require('cors')


module.exports = (app) => {
    router.post('/login', TenantConnection(), (req, res, next) => {
        accessController.userLogin(req, res, next);
    });

    router.post('/signup', (req, res, next) => {
        accessController.userRegistration(req, res, next);
    });

    router.post('/tokenVerify', TenantConnection(), (req, res, next) => {
        accessController.tokenVerification(req, res, next);
    });

    router.post('/changepassword/', TenantConnection(), (req, res, next) => {
        accessController.changepassword(req, res, next);
    });
    
    router.post('/refreshtoken', (req, res, next) => {
        accessController.userRefreshToken(req, res, next);
    });

    router.post('/forgetpassword',TenantConnection(),(req, res, next) => {
        accessController.forgetUserPassword(req, res, next);
    });

    router.get('/secure', authenticate(), function (req, res) {
        res.json({ message: 'Secure data' })
    });

    router.get('/send_mail', (req, res) => {
        sendMail.TestSend_Email((response)=>{
            res.json(response);
        });
      })

    app.use('/api', router);
};
