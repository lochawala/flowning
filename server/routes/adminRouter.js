var express = require('express');
var router = express.Router();
var authenticated = require('../middlewares/authenticate');
var adminController = require('../controllers/admin_controller');
var authenticate = authenticated.authenticate;

module.exports = (app) => {

    router.post('/GetCompanySetting', authenticate(), (req, res, next) => {
        adminController.GetAdminConfig(req,function(data){
            res.json(data);
        });
    });

    
    router.post('/EditSetting',  authenticate(), (req, res, next) => {
        adminController.EditSetting(req,function(data){
            res.json(data);
        });
    });
    
    router.post('/EditCompanyAdminUser',  authenticate(), (req, res, next) => {
        adminController.EditCompanyAdminUser(req,function(data){
            res.json(data);
        });
    });    

    router.post('/TenantRequest', authenticate(),  (req, res, next) => {
        adminController.TenantRequest(req.body,function(data){
            res.json(data);
        });
    });
    
    router.post('/GetTenantRequest', authenticate(), (req, res, next) => {
        adminController.GetTenantRequest(req.body,function(data){
            res.json(data);
        });
    });

    router.post('/AddCompany', authenticate(), (req, res, next) => {
        adminController.AddCompany(req.hostname,req.body,(data) =>{
            res.json(data);
        });
    });

    router.post('/GetPassword', authenticate(), (req, res, next) => {
        adminController.GetPassword(req.body,(data) =>{
            res.json(data);
        });
    });
    
    
    
    app.use('/api', router);
};

