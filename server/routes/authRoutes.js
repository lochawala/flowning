const passport = require('passport');
const mongoUtil = require('../services/mongoUtil');
const keys = require('../../config/keys');
var utility = require('../services/utility');
var authenticate = require('../middlewares/authenticate');
var oauthServer = require('oauth2-server');
var User = require('./../models/users.js');
var Request = oauthServer.Request;
var Response = oauthServer.Response;

module.exports = (app) => {
  app.get(
    '/auth/google',
    passport.authenticate('google', {
      scope: ['profile', 'email']
    })
  );

  app.get('/api', (req, res) => {
    res.json("test");
  });

  app.get(
    '/auth/google/callback',
    passport.authenticate('google'),
    (req, res) => {
      res.redirect('/login/' + req.user.googleId);
    }
  );

  app.get('/api/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });

  app.get('/api/current_user', (req, res) => {
    res.send(req.user);
  })

  app.post('/api/googleLogin', (req, res) => {
    if(req.user){
      if (req.user.googleId == req.body.id) {
        User.findOne({ email: req.user.email, googleId: req.user.googleId }, function (err, user) {
          if (err) {
            res.send({ "status": false, "message": 'OOPS! There is some error in processing your request', "status_code": 202 });
          } else {
            if (user) {
              req.headers['content-type'] = 'application/x-www-form-urlencoded';
              req.headers['Authorization'] = 'Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0';
              req.body['grant_type'] = 'password';
              req.body['password'] = utility.decrypt(user.password);
              req.body['username'] = user.email;
              
              var request = new Request(req);
              var response = new Response(res);
              authenticate.oauth.token(request, response)
                .then(function (token) {
                  var response_data = {
                    access_token: token.accessToken,
                    refresh_token: token.refreshToken,
                  }
                  response_data.user = token.user
                  res.send({ "status_code": 200, status: true, data: response_data })
                }).catch(function (err) {
                  res.send({ "status_code": 201, status: err, message: req.body  , data :user})
                });
            }else{
              res.send({ "status_code": 201, status: false, message: "Some thign is worng, Please try again" })
            }
          }
        });
      } else {
        res.send();
      }
    }else{
      res.send();
    }
  

  });
}
