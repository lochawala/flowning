const keys = require('../../config/keys')
const stripe = require('stripe')(keys.stripeSecretKey)
const requireLogin = require('../middlewares/requireLogin')
//const db = require('../services/mongoUtil').getDB()
//const usersCollection = db.collection('users')
var authenticated = require('../middlewares/authenticate');
var authenticate = authenticated.authenticate;
var User = require('./../models/users.js');

module.exports = app => {
  app.post('/api/stripe', authenticate(), requireLogin, async (req, res) => {
    const charge = await stripe.charges.create({
      amount: 500,
      currency: 'usd',
      description: '$5 for 5 credits',
      source: req.body.id
    })

    req.user.credits += 5;

    User.updateOne(
      {googleId: req.user.googleId},
      {$set: {credits: req.user.credits}},
      (err, obj) => {
      if (err) console.error(err)
    })

    res.send(req.user)
  })
}