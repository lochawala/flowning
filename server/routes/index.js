var accessController = require('./accessRoutes');
var usersRouter = require('./usersRouter');
var adminRouter = require('./adminRouter');
var surveyRouter = require('./surveryRoutes');
const cors = require('cors')

var Routers = function(app){
    app.use(cors());
    accessController(app);
    usersRouter(app);
    adminRouter(app);
    surveyRouter(app);
}

module.exports = Routers;