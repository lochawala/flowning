const cors = require('cors')
const URL = require('url')
const mongodb = require('mongodb')

const mongoUtil = require( '../services/mongoUtil' );
var authenticated = require('../middlewares/authenticate');
var authenticate = authenticated.authenticate;
var utility = require('../services/utility');


module.exports = (app) => {	  
  app.use(cors());

  // delete a document in a collection
  app.delete('/api/delete-document',authenticate(), (req, res) => {
    const url = URL.parse(req.url, true)
    const { value, table, field } = url.query
    //const collection = db.collection(table)

    const db = mongoUtil.getDB();
    var collection = db.collection(table);
    if(utility.IsTenant(req.hostname)){
      collection = mongoUtil.connectDB(utility.getHostName(req), table);
    }

    const data = { [field] : value }

    collection.deleteOne(data, (err, obj) => {
      if (err) console.error(err)
      if (obj.result.n > 0) {
        res.send({ message: `success delete document with ${field} = ${value} from ${table} collection` })
      } else {
        res.send({ message: `no document with ${field} = ${value} in ${table} collection` })
      }
    })
  })

  // clear collection
  app.get('/api/delete-all-documents', authenticate(), (req, res) => {
    const url = URL.parse(req.url, true)
    const { form } = url.query
    //const formCollection = db.collection(form)
    
    const db = mongoUtil.getDB();
    var formCollection = db.collection(form);
    if(utility.IsTenant(req.hostname)){
      formCollection = mongoUtil.connectDB(utility.getHostName(req), form);
    }


  	formCollection.deleteMany({}, (err, result) => {
  		if (err) console.error(err)
  		res.send({ message: `success delete forms on DB` })
  	})
  })

  // delete collection
  app.delete('/api/delete-collection',authenticate(), (req, res) => {
  	const url = URL.parse(req.url, true)
  	const name = url.query.name
  //  const collection = db.collection(name);
    
    const db = mongoUtil.getDB();
    var collection = db.collection(form);
    if(utility.IsTenant(req.hostname)){
      collection = mongoUtil.connectDB(utility.getHostName(req), form);
    }

  	collection.drop((err, result) => {
  		if (err) console.error(err)
  		res.send({ result })
  	})
  })

  // get the all collections
  app.get('/api/all-collection', authenticate(), (req, res) => {
    const db = mongoUtil.getDB();
    if(utility.IsTenant(req.hostname)){
      db = mongoUtil.getDBs(utility.getHostName(req));
    }
  	db.listCollections().toArray((err, result) => {
  		res.send({ result })
  	})
  })

  // find collection by name
  app.get('/api/collection',authenticate(), (req, res) => {
  	const url = URL.parse(req.url, true)
  	const name = url.query.name
    //const collection = db.collection(name)

    const db = mongoUtil.getDB();
    var collection = db.collection(name);
    if(utility.IsTenant(req.hostname)){
      collection = mongoUtil.connectDB(utility.getHostName(req), name);
    }

  	collection.find({}).toArray((err, result) => {
  		res.send({ result })
  	})
  })

  // example for inline (non-nested) mongodb query call
  app.get('/api/test-collection',authenticate(), async (req, res) => {
    const url = URL.parse(req.url, true)
    const name = url.query.name
    //const collection = db.collection(name)

    const db = mongoUtil.getDB();
    var collection = db.collection(name);
    if(utility.IsTenant(req.hostname)){
      collection = mongoUtil.connectDB(utility.getHostName(req), name);
    }

    const promise = new Promise((resolve, reject) => {
      collection.find({}).toArray((err, result) => {
        resolve(result)
      })
    })

    const result = await promise.then(res => res)
    res.send(result)
  })

  // api for testing
  app.patch('/api/test-change-user-role-id', authenticate(), async (req, res) => {
    const url = URL.parse(req.url, true)
    const { id, role_id } = url.query
   // const collection = db.collection('users')

    const db = mongoUtil.getDB();
    var collection = db.collection('users');
    if(utility.IsTenant(req.hostname)){
      collection = mongoUtil.connectDB(utility.getHostName(req), 'users');
    }

    const promise = new Promise((resolve, reject) => {
      collection.updateOne({_id: mongodb.ObjectID(id)}, {$set: {role_id: Number(role_id)}}, (err, result) => {
        resolve(result)
      })
    })

    const result = await promise.then(res => res)
    res.send(result)
  })
}