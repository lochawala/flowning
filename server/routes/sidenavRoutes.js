const cors = require('cors')
const URL = require('url')
var utility = require('../services/utility');
const mongoUtil = require('../services/mongoUtil');
var authenticated = require('../middlewares/authenticate');
var TenantConnection = authenticated.TenantConnection;
var authenticate = authenticated.authenticate;
module.exports = (app) => {
  app.use(cors());

  app.get('/api/sidenav-links',  authenticate(), (req, res) => {
    const formCollection = mongoUtil.getDB().collection('form')
    if (utility.IsTenant(req.hostname)) {
      formCollection = mongoUtil.connectDB(utility.getHostName(req), 'form');
    }

    formCollection.find({}).toArray((err, result) => {
      const data = result.map(r => {
        // send Pascalcase collection name
        let { collectionName } = r
        collectionName = collectionName.charAt(0).toUpperCase() + collectionName.slice(1)

        return {
          name: collectionName,
            route: `/collection?id=${r._id}`,
          icon: r.icon,
          text: collectionName
        }
      }
      )
      res.send({ data })
    })
  })

  // store sidenav links configuration to database
  app.post('/api/sidenav-config', authenticate(), (req, res) => {
    const { appName, groupLinks } = req.body
    var sidenavCollection = mongoUtil.getDB().collection('sidenav');
    if (utility.IsTenant(req.hostname)) {
      sidenavCollection = mongoUtil.connectDB(utility.getHostName(req), 'sidenav');
    }
    
    sidenavCollection.findOne({ appName }, (err, config) => {
      if (err) console.error(err)
      if (config != null) {
        sidenavCollection.updateOne({ appName }, { $set: { groupLinks } })
        res.send({
          message: 'updated existing sidenav config'
        })
      } else {
        sidenavCollection.insertOne(req.body, (err, result) => {
          if (err) console.error(err)
          res.send({
            message: 'success add sidenav config'
          })
        })
      }
    });
  });

  // get sidenav config based on app name from DB
  app.get('/api/sidenav-config',authenticate(), (req, res) => {
    const url = URL.parse(req.url, true)
    const appName = url.query.app_name
    let sidenavCollection = mongoUtil.getDB().collection('sidenav');
      
    if (utility.IsTenant(req.hostname)) {
      sidenavCollection = mongoUtil.connectDB(utility.getHostName(req), 'sidenav');
    }
    if (appName) {
      if (sidenavCollection) {
        sidenavCollection.findOne({ appName }).then((sidenav,err) => {
          if (err) console.error(err)
          if (sidenav != null) {
            res.send({ data: sidenav })
          } else {
            res.send({ data: {}, message: `config for ${appName} is not found in database` })
          }
        }).catch(err => {
          res.json({ "status": false, "message": err.message, "status_code": 202 });
        })
      }else{
        res.json({ "status": false, "status_code": 202 });
      }

    } else {
      sidenavCollection.find({}).toArray((err, result) => {
        if (err) console.error(err)

        if (result.length === 0) {
          res.send({ message: 'no sidenav config record in database' })
        } else if (result.length > 0) {
          res.send({ data: result })
        }
      })
    }
  })

}