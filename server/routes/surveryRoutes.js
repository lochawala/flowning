var express = require('express');
var router = express.Router();
var authenticated = require('../middlewares/authenticate');
var surveryController = require('../controllers/survery_controller');
var authenticate = authenticated.authenticate;

module.exports = (app) => {

    router.put('/addservey',  authenticate(), (req, res, next) => surveryController.AddSurvey(req, res, next));
    router.get('/serveyList',  authenticate(), (req, res, next) => surveryController.SurveyList(req, res, next));
    router.post('/serveyDelete',  authenticate(), (req, res, next) => surveryController.ServeyDelete(req, res, next));
    router.post('/getQuestionsData',  (req, res, next) => surveryController.GetQuestionsData(req, res, next));
    router.post('/addSurveyResponse',  (req, res, next) => surveryController.AddSurveyResponse(req, res, next));
    router.post('/getSurveyResponse',  authenticate(), (req, res, next) => surveryController.GetSurveyResponse(req, res, next));
    router.post('/getSurveyResponseanswer',  authenticate(), (req, res, next) => surveryController.GetSurveyResponseanswer(req, res, next));
    app.use('/api', router);
};
