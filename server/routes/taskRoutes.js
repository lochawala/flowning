const _ = require('lodash');
const Path = require('path-parser').default;
const { URL } = require('url');
const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');
var authenticated = require('../middlewares/authenticate');
var authenticate = authenticated.authenticate;

const Task = mongoose.model('tasks');

module.exports = app => {
  app.post('/api/tasks',authenticate(), requireLogin, async (req, res) => {
    const { title, description } = req.body;

    const task = new Task({
      title,
      description,
      _user: req.user.id
    });

    try {
      await task.save();

      res.send(task);
    } catch (err) {
      res.status(422).send(err);
    }
  });

  app.get('/api/tasks', requireLogin, async (req, res) => {
    const tasks = await Task.find({ _user: req.user.id });

    res.send(tasks);
  });
};
