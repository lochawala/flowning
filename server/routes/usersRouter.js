var express = require('express');
var router = express.Router();
var authenticated = require('../middlewares/authenticate');
var userController = require('../controllers/users_controller');
var authenticate = authenticated.authenticate;

module.exports = (app) => {
    router.post('/AddUser',  authenticate(), (req, res, next) => {
        userController.AddUsers(req, res, next);
    });

    router.post('/GetUserList',  authenticate(), (req, res, next) => {
        userController.GetUserList(req, res, next);
    });

    router.post('/updateProfile',  authenticate(), (req, res, next) => {
        userController.updateProfile(req, res, next);
    });

    router.delete('/DeltedUser',  authenticate(), (req, res, next) => {
        res.json("connect");
    });
    
    app.use('/api', router);
};
